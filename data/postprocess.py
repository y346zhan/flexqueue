#!/usr/bin/env python3
import os
import glob
import sys
import statistics
# get list of raw files

# each line is either
# 1. begins with '#', is a comment: print verbatim
# 2. begins with '"', is a block header: print verbatim
# 3. is empty, signals end of block: print verbatim
# 4. is a dataline, belongs with the next 19 lines, column 1 is x, column 2 is y

hosts = ['amd', 'kosi32']
prefix = os.path.expanduser("~/thesis/data/")
sample_size = 20

for host in hosts:
    for filename in glob.glob(prefix + host + "/raw/*.dat"):
        if "ns3" in filename:
            sample_size = 10
        else:
            sample_size = 20
        with open(filename, 'r') as fsrc:
            print("processing " + fsrc.name + "...")
            with open(prefix + host + "/" + os.path.basename(filename), "w") as fdst:
                y = []
                for line in fsrc:
                    if not line.strip():
                        fdst.write("\n")
                    elif line[0] == '"':
                        fdst.write(line.strip() + "\n")
                    elif line[0] == '#':
                        continue
                    else:
                        tokens = line.split()
                        x = float(tokens[0])
                        y.append(int(tokens[1]))
                        if len(y) == sample_size:
                            fdst.write("{:.1f}\t{:.2f}\t{:.2f}\n".format(x, statistics.mean(y), statistics.stdev(y)))
                            y = []
        print("...done")


