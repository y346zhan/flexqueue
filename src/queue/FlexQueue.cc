#include "FlexQueue.h"


Horizon::Horizon()
  : tpb(TP::flexQueue::tpbInit),
    B(TP::flexQueue::bitCount)
{
  buf = new buf_t[memsize(B)]();
  init(B, buf);

  d = tpb * B;
  l = 0;
  r = d;

  INFO("Horizon memory usage=" << memsize(B) << " bytes");
}

Horizon::~Horizon()
{
  delete buf;
}

FlexQueue::FlexQueue () :
    MAX_EVENT(limit<uint64_t>(), limit<uint32_t>())
#ifndef NO_RESIZE
    ,S({0,0,TP::flexQueue::k})
#endif
{
  ts0 = 0;
  c = 0;
  n_a = 0;

  DEBUG("FlexQueue ctor"
      << ", tpb=" << V[c].tpb
      << ", d=" << V[c].d
#ifndef NO_RESIZE
      << ", u=" << S.u
      << ", sigma=" << S.sigma
#endif
      );
}

inline FlexQueue::~FlexQueue ()
{
  DEBUG("FlexQueue dtor"
      << ", current tpb=" << V[c].tpb
      << ", A.size=" << n_a
      << ", H.size=" << H.size()
      );
}

/** 
 * Within horizon: insert into bucket and set corresponding bit
 * Outside horizon: insert into STL map
 */
void FlexQueue::Insert (Event &ev)
{
  using namespace TP::flexQueue;

  //assert(ev.key.m_ts >= GetCurrentTime());
  //assert(ts0 <= GetCurrentTime());

  uint64_t offset = ev.key.m_ts - GetCurrentTime();

  // update estimates for u and sigma
#ifndef NO_RESIZE
  S.u = (S.u * alphaN >> alphaDe)
      + (offset * (alphaD - alphaN) >> alphaDe);
  S.sigma = (S.sigma * betaN >> betaDe)
      + ((offset > S.u ? (offset-S.u) : (S.u-offset)) * (betaD - betaN) >> betaDe);

  TRACE("u'=" << S.u << ", sigma'=" << S.sigma);
#endif

  if (offset < V[c].r && offset >= V[c].l) {
    uint64_t bitPos = ( ev.key.m_ts - ts0 - V[c].l ) / V[c].tpb;
    if (bitPos >= V[c].B) {
      // since lower bound is potentially increased, there is a new possibility
      // that what belongs in A according to V[c], actually belongs in H,
      // according to V[1-c].

      uint64_t bitPosNext = (ev.key.m_ts - ts0 - V[c].d - V[c].l - V[1^c].l) / V[1^c].tpb;

      if (bitPosNext >= V[1^c].B) {
        H.insert(ev);
      } else {
        V[1 ^ c].set(bitPosNext);
        A[1 ^ c][bitPosNext].insert(ev); // operator[] creates the set if bitPosNext doesn't exist
        n_a++;

        TRACE(" Insert {id=" << ev.key.m_uid << ", ts=" << ev.key.m_ts << "} into A[" << (1^c) << "][" << bitPosNext << "]");
      }
    } else {
      //assert(bitPos < V[c].B);

      V[c].set(bitPos);
      A[c][bitPos].insert(ev);
      n_a++;

      TRACE(" Insert {id=" << ev.key.m_uid << ", ts=" << ev.key.m_ts << "} into A[" << c << "][" << bitPos << "]");
    }
  } else {
    H.insert(ev);

    TRACE(" Insert t=" << ev.key.m_ts << " into H");
  }
}

/**
 * Test empty
 */
bool FlexQueue::IsEmpty (void) const
{
  return Size() == 0;
}

/**
 * Remove next
 */
Event FlexQueue::RemoveNext(void)
{
  //assert(!IsEmpty());

  int i = c;
  Bucket::iterator nearIt;
  OverflowList::iterator farIt;
  Event nearEvt = MAX_EVENT, farEvt = MAX_EVENT;

  uint64_t pos = 0;
  if (!V[0].empty() || !V[1].empty()) {
    pos = V[c].find();
    if (pos == limit<uint64_t>()) {
      pos = V[1 ^ c].find();
      i = 1 ^ c;
    }
    nearIt = A[i].at(pos).begin();
    nearEvt = *nearIt;
  }

  if (!H.empty()) {
    farIt = H.begin();
    farEvt = *farIt;
  }

  if (nearEvt < farEvt) {
    TRACE(" remove ts=" << nearEvt.key.m_ts << " from A[" << i << "][" << pos << "], c=" << c);

    A[i].at(pos).erase(nearIt);
    if (A[i].at(pos).empty()) {
      V[i].clr(pos);
    }
    n_a--;

    if (c != i) { // next event belongs to a different horizon, ie current horizon is empty
      // add to ts0 the largest multiple of d that is
      // smaller than the current gap
      uint64_t gap = nearEvt.key.m_ts - ts0;
      uint64_t multiple = gap - gap % (V[c].d + V[c].l);
      ts0 += multiple;

      TRACE(" (bucket) ts0 += " << multiple << ", to " << ts0);

      triggerResize();
      c = i;
    }

    return nearEvt;
  }

  uint64_t gap = farEvt.key.m_ts - ts0;
  uint64_t multiple = gap - gap % (V[c].d + V[c].l);
  ts0 += multiple;
  //S.remove.miss++;

  if (multiple > 0) {
    triggerResize();
    c = 1 ^ c;
  }

  TRACE("remove ts=" << farEvt.key.m_ts << " from overflow");
  TRACE("(overflow) ts0 += " << multiple << ", to " << ts0);

  H.erase(farIt);
  return farEvt;
}

/**
 * At the cost of small overhead in HierarchicalBitmap::set
 */
size_t FlexQueue::Size () const
{
  return n_a + H.size();
}

Event FlexQueue::Peek () const {
  assert(!IsEmpty());

  Event nearEvt = MAX_EVENT, farEvt = MAX_EVENT;
  if (!V[0].empty() || !V[1].empty()) {
    int i = c;
    uint64_t pos = V[c].find();
    if (pos == limit<uint64_t>()) {
      pos = V[1 ^ c].find();
      i = 1 ^ c;
    }
    nearEvt = *A[i].at(pos).begin();
  }

  if (!H.empty()) {
    farEvt = *H.begin();
  }
  return nearEvt < farEvt ? nearEvt : farEvt;
}

void FlexQueue::triggerResize() {
  //assert(V[c].empty());

#ifdef NO_RESIZE
  return;
#endif
  // If no resize is necessary, at minimum the current empty horizon should
  // match the other in-use horizon
  DEBUG("Updating horizon " << c << " (" << V[c].d
      << ") to be the same as horizon " << (1^c) << " (" << V[1^c].d << ")");

  V[c].tpb = V[1^c].tpb;
  V[c].d = V[1^c].d;

  StddevStrategy();

  //assert(V[0].d == V[0].tpb * V[0].B
  //    && V[1].d == V[1].tpb * V[1].B
  //    );
}

/*
 * This strategy tries to capture a predetermined stddev (e.g. +/- 1 stddev)
 *
 * Stat needed:
 *  S.u: estimated avg distance to next event
 *  S.sigma: estimated deviation of S.u
 */
#ifndef NO_RESIZE
void FlexQueue::StddevStrategy() {
  DEBUG("Executing stddev strategy...");
  DEBUG("u=" << S.u << ", sigma=" << S.sigma);
  DEBUG("l=" << V[c].l << ", r=" << V[c].r);

  uint64_t interval = S.k * S.sigma;
  V[c].l = (S.u <= interval) ? 0 : S.u - interval;

  V[c].tpb = (S.u + interval - V[c].l) / V[c].B;
  if (V[c].tpb == 0) {
    V[c].tpb = 1;     // there is a limit to how far we can zoom in
  }
  V[c].d = V[c].tpb * V[c].B;
  V[c].r = V[c].l + V[c].d;

  DEBUG("V[" << c << "].l=" << V[c].l << ", r=" << V[c].r);
  DEBUG("...done");
}
#endif
