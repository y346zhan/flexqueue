#ifndef FIBQ_H
#define FIBQ_H

#include "benchmark-common.h"
#include <stdint.h>
#include <utility>
#include <boost/heap/fibonacci_heap.hpp>
#include <boost/pool/pool_alloc.hpp>

class FibQueue : public QueueI
{
  public:

    inline FibQueue () {}
    inline virtual ~FibQueue () {}

    // Inherited
    inline virtual void Insert (Event &ev) {
      m_list.push (ev);
    }

    inline virtual bool IsEmpty (void) const {
      return m_list.empty ();
    }

    inline virtual Event RemoveNext (void) {
      Event ev = m_list.top();
      m_list.pop();
      return ev;
    }

    inline virtual Event Peek () const {
      return m_list.top();
    }

    inline virtual size_t Size () const {
      return m_list.size();
    }

  private:
    typedef boost::heap::fibonacci_heap< Event, boost::heap::stable<false>, boost::heap::compare<std::greater<Event>>, boost::fast_pool_allocator<Event>> EventMap;

    /** The event list for all events. */
    EventMap m_list;
};


#endif /* FIBQ_H */
