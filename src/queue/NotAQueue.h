#ifndef NONQ_H
#define NONQ_H

#include "benchmark-common.h"
#include <stdint.h>
#include <map>
#include <utility>

class NonQueue : public QueueI
{
  size_t _count = 0;
public:

  inline NonQueue () {
    INFO("NonQueue ctor");
  }
  inline virtual ~NonQueue () {
    INFO("NonQueue dtor");
  }

  // Inherited
  inline virtual void Insert (Event &ev) {
    _count++;
  }

  inline virtual bool IsEmpty (void) const {
    return Size() == 0;
  }

  inline virtual Event RemoveNext (void) {
    _count--;
    return Event{};
  }

  inline virtual Event Peek () const {
    return Event{};
  }

  inline virtual size_t Size () const {
    return _count;
  }
};


#endif /* NONQ_H */
