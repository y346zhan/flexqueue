#ifndef DUMBQ_H
#define DUMBQ_H

#include "benchmark-common.h"
#include <stdint.h>
#include <map>
#include <utility>

class DumbQueue : public QueueI
{
public:
  inline DumbQueue () {
    INFO("DumbQueue ctor");
  }

  inline virtual ~DumbQueue () {
    INFO("DumbQueue dtor, hit=" << _stat.insert.hit
        << ", miss=" << _stat.insert.miss
        << ", remove hit=" << _stat.remove.hit
        << ", remove miss=" << _stat.remove.miss
        << ", num_bkt_event=" << _bucket.size()
        );
  }

  // Inherited
  inline virtual void Insert (Event &ev) {
    if (ev.key.m_ts - GetCurrentTime() < _h) {
      _stat.insert.hit++;
      _bucket.insert(ev);
    } else {
      _stat.insert.miss++;
      _overflow.insert(ev);
    }
  }

  inline virtual bool IsEmpty (void) const {
    return _bucket.empty () && _overflow.empty();
  }

  inline virtual Event RemoveNext (void) {
    Event nearEvt = MAX_EVENT, farEvt = MAX_EVENT;

    if (!_bucket.empty()) {
      nearEvt = *_bucket.begin();
    }

    if (!_overflow.empty()) {
      farEvt = *_overflow.begin();
    }

    if (nearEvt < farEvt) {
      _bucket.erase(_bucket.begin());
      _stat.remove.hit++;
      return nearEvt;
    }

    _overflow.erase(_overflow.begin());
    _stat.remove.miss++;
    return farEvt;
  }

  inline virtual Event Peek () const {
    Event nearEvt = MAX_EVENT, farEvt = MAX_EVENT;

    if (!_bucket.empty()) {
      nearEvt = *_bucket.begin();
    }

    if (!_overflow.empty()) {
      farEvt = *_overflow.begin();
    }

    if (nearEvt < farEvt) {
      return nearEvt;
    }

    return farEvt;
  }

  inline virtual size_t Size () const {
    return _bucket.size() + _overflow.size();
  }

  inline size_t BucketSize () const {
    return _bucket.size();
  }

private:
  typedef std::set<Event, std::less<Event>, boost::fast_pool_allocator<Event>> EventMap;

  constexpr static size_t _h = (TP::piecewise::horizon);
  constexpr static Event MAX_EVENT = {nullptr, {limit<size_t>(), limit<uint32_t>()}};

  /** The event list for all events. */
  EventMap _bucket;
  EventMap _overflow;
  struct CtrlStat {
    struct Counter {
      size_t hit;
      size_t miss;
    };
    Counter insert;
    Counter remove;
  } _stat = {};
};


#endif /* DUMBQ_H */
