#ifndef FLEX_Q_H
#define FLEX_Q_H

#include "benchmark-common.h"
#include <unordered_map>
#include <set>
#include <array>
#include <boost/pool/pool_alloc.hpp>

struct Horizon : public HierarchicalBitmap<TP::baseWidth, ceilinglog2(TP::flexQueue::bitCount)> {
  uint64_t tpb;       // duration represented by each bit
  uint64_t d;         // total duration covered by the bitvector
  const uint64_t B;   // number of bits in this bitvector
  uint64_t l, r;      // [l, r) is the range of time covered by A
  bufptr_t buf;     // storage for the bitvector

  Horizon();
  ~Horizon();
  bool setUnitDuration(uint64_t newUnitDuration);
};

class FlexQueue : public QueueI {
  public:
    FlexQueue ();
    virtual ~FlexQueue ();

    virtual void    Insert      (Event &ev);
    virtual bool    IsEmpty     (void) const;
    virtual Event   RemoveNext  (void);
    virtual size_t  Size () const;
    virtual Event   Peek () const;

  private:
    const Event MAX_EVENT;

    //typedef std::set<Event, std::less<Event>, boost::fast_pool_allocator<Event>> OverflowList;
    //typedef std::set<Event, std::less<Event>, boost::fast_pool_allocator<Event>> Bucket;
    //typedef std::unordered_map<size_t, Bucket> BucketArray;

    typedef std::set<Event> OverflowList;
    typedef std::set<Event> Bucket;
    typedef std::array<Bucket, TP::flexQueue::bitCount> BucketArray;

    //typedef boost::intrusive::set<Event> OverflowList;
    //typedef boost::intrusive::set<Event> Bucket;
    //typedef std::unordered_map<size_t, Bucket> BucketArray;

    //typedef Bucket BucketArray[TP::flexQueue::bitCount];

    Horizon       V[2]; // horizon 
    BucketArray   A[2];
    OverflowList  H;

    void triggerResize();

    void executeStrategies();
    void StddevStrategy();

#ifndef NO_RESIZE
    struct {
      uint64_t u;     // mean
      uint64_t sigma; // standard deviation
      const double k;
    } S;
#endif

    uint64_t  n_a;    // total number of events in bucket array

    uint64_t  ts0;    // timestamp represented by the first bit in V[c]
    int     c;      // which horizon V[c] is currently in use: 0, 1
};

#endif
