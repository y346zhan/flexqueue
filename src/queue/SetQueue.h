#ifndef SETQ_H
#define SETQ_H

#include "benchmark-common.h"
#include <stdint.h>
#include <map>
#include <utility>
#include <boost/pool/pool_alloc.hpp>

class SetQueue : public QueueI
{
  public:

    inline SetQueue () {}
    inline virtual ~SetQueue () {}

    // Inherited
    inline virtual void Insert (Event &ev) {
      m_list.insert (ev);
    }

    inline virtual bool IsEmpty (void) const {
      return m_list.empty ();
    }

    inline virtual Event RemoveNext (void) {
      EventMapI i = m_list.begin ();
      Event ev = *i;
      m_list.erase (i);
      return ev;
    }

    inline virtual Event Peek () const {
      return *m_list.begin();
    }

    inline virtual size_t Size () const {
      return m_list.size();
    }

  private:
    //typedef std::set<Event, std::less<Event>, boost::fast_pool_allocator<Event>> EventMap;
    //typedef std::set<Event, std::less<Event>, boost::fast_pool_allocator<Event>>::iterator EventMapI;
    //typedef std::set<Event, std::less<Event>, boost::fast_pool_allocator<Event>>::const_iterator EventMapCI;
    typedef std::set<Event> EventMap;
    typedef std::set<Event>::iterator EventMapI;
    typedef std::set<Event>::const_iterator EventMapCI;

    /** The event list for all events. */
    EventMap m_list;
};


#endif /* SETQ_H */
