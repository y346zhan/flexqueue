#ifndef BITMAP_Q_H
#define BITMAP_Q_H

#include "benchmark-common.h"
#include <map>
#include <list>
#include <algorithm>
#include <exception>
#include <cstring>
#include <iostream>

/*
#ifndef BASE_WIDTH
#define BASE_WIDTH 64ul
#endif

#ifndef HORIZON
#define HORIZON 30
#endif
*/

class BitmapQueue : public QueueI {

  public:

    inline BitmapQueue () {
      size_t size = _bitmap.memsize(NUM_BKTS);
      if ( posix_memalign(&_storage, 64, size) ) {
        INFO("failed to allocate " << size << " bytes of aligned memory");
        exit(1);
      }
      _bitmap.init(NUM_BKTS, (bufptr_t)_storage);

      INFO("Bitmap memory usage=" << size << " bytes"
          );
    }

    inline virtual ~BitmapQueue () {
      free(_storage);
      _storage = nullptr;
    }

    /**
     * Insert an event
     */
    inline virtual void Insert (Event &ev) {
      size_t pos = ( (GetCurrentTime() % NUM_BKTS) + (ev.key.m_ts - GetCurrentTime()) );
      if (_bitmap.set(pos % NUM_BKTS)) _count++;
    }

    /**
     * Test for empty
     */
    inline virtual bool IsEmpty (void) const {
      return _bitmap.empty();
    }

    /**
     * Remove next
     */
    inline virtual Event RemoveNext(void) {
      if (IsEmpty()) {
        INFO("RemoveNext when empty");
        exit(1);
      }

      Event ev;
      size_t pos = _bitmap.findnext(GetCurrentTime() % NUM_BKTS);
      if (pos == limit<size_t>()) {
        pos = _bitmap.find();
      }

      _bitmap.clr(pos);
      ev.key.m_ts = pos;

      _count--;
      return ev;
    }

    /**
     * At the cost of small overhead in HierarchicalBitmap::set
     */
    inline virtual size_t Size () const {
      return _count;
    }

    /**
     * Returns next event without removing it
     */
    inline virtual Event Peek () const {
      Event ev;
      size_t pos = _bitmap.findnext(GetCurrentTime() % NUM_BKTS);
      if (pos == limit<size_t>()) {
        pos = _bitmap.find();
      }

      ev.key.m_ts = pos;
      return ev;
    }

    // log2 of bitmap width
    //static const uint64_t W = HORIZON;
    //static const uint64_t BW = BASE_WIDTH;

    // timers from 0 to 2^W time units are stored in buckets

  private:
    typedef HierarchicalBitmap<TP::baseWidth, ceilinglog2<size_t>(TP::bitmapQueue::bitCount)> bitmap_type;

    static const size_t NUM_BKTS = (TP::bitmapQueue::bitCount);
    //static const size_t BKT_MASK = bitmask<size_t>(TP::uniform::horizon);

    bitmap_type _bitmap;
    void*       _storage;
    size_t      _count = 0;
};


#endif
