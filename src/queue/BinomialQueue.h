#ifndef BINOMIALQ_H
#define BINOMIALQ_H

#include "benchmark-common.h"
#include <stdint.h>
#include <utility>
#include <boost/heap/binomial_heap.hpp>
#include <boost/pool/pool_alloc.hpp>

class BinomialQueue : public QueueI
{
  public:

    inline BinomialQueue () {}
    inline virtual ~BinomialQueue () {}

    // Inherited
    inline virtual void Insert (Event &ev) {
      m_list.push (ev);
    }

    inline virtual bool IsEmpty (void) const {
      return m_list.empty ();
    }

    inline virtual Event RemoveNext (void) {
      Event ev = m_list.top();
      m_list.pop();
      return ev;
    }

    inline virtual Event Peek () const {
      return m_list.top();
    }

    inline virtual size_t Size () const {
      return m_list.size();
    }

  private:
    typedef boost::heap::binomial_heap< Event, boost::heap::stable<false>, boost::heap::compare<std::greater<Event>>, boost::fast_pool_allocator<Event>> EventMap;

    /** The event list for all events. */
    EventMap m_list;
};


#endif /* BINOMIALQ_H */
