/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#ifndef CALENDAR_SCHEDULER_H
#define CALENDAR_SCHEDULER_H

#include <stdint.h>
#include <list>
#include <utility>
#include <string>
#include "benchmark-common.h"

/**
 * \file
 * \ingroup scheduler
 * Declaration of ns3::NS3CalendarQueue class.
 */

/**
 * This is NS3's implementation of a calendar queue variant.
 * Verified that it perform poorly compared to both STL map, and HybridQueue.
 */

struct EventImpl;

/**
 * \ingroup scheduler
 * \brief a calendar queue event scheduler
 *
 * This event scheduler is a direct implementation of the algorithm
 * known as a calendar queue, first published in 1988 in
 * "Calendar Queues: A Fast O(1) Priority Queue Implementation for
 * the Simulation Event Set Problem" by Randy Brown. There are many
 * refinements published later but this class implements
 * the original algorithm (to the best of my knowledge).
 *
 * \note
 * This queue is much slower than I expected (much slower than the
 * std::map queue) and this seems to be because the original resizing policy
 * is horribly bad.  This is most likely the reason why there have been
 * so many variations published which all slightly tweak the resizing
 * heuristics to obtain a better distribution of events across buckets.
 */
class NS3CalendarQueue : public QueueI
{
  public:

    NS3CalendarQueue ()
    {
      Init (2, 1, 0);
      m_qSize = 0;
    }
    virtual ~NS3CalendarQueue ()
    {
      delete [] m_buckets;
      m_buckets = 0;
    }

    // Inherited
    virtual void Insert (Event &ev)
    {
      DoInsert (ev);
      m_qSize++;
      ResizeUp ();
    }
    virtual bool IsEmpty (void) const
    {
      return m_qSize == 0;
    }
    virtual Event Peek (void) const
    {
      uint32_t i = m_lastBucket;
      uint64_t bucketTop = m_bucketTop;
      Event minEvent;
      minEvent.key.m_ts = ~0;
      minEvent.key.m_uid = ~0;
      //minEvent.key.m_context = 0;
      do
      {
        if (!m_buckets[i].empty ())
        {
          Event next = m_buckets[i].front ();
          if (next.key.m_ts < bucketTop)
          {
            return next;
          }
          if (next.key < minEvent.key)
          {
            minEvent = next;
          }
        }
        i++;
        i %= m_nBuckets;
        bucketTop += m_width;
      }
      while (i != m_lastBucket);

      return minEvent;
    }
    virtual Event RemoveNext (void)
    {

      Event ev = DoRemoveNext ();
      m_qSize--;
      ResizeDown ();
      return ev;
    }
    virtual size_t Size (void) const
    {
      return m_qSize;
    }

    /*
       virtual void Remove (const Event &ev)
       {
    // bucket index of event
    uint32_t bucket = Hash (ev.key.m_ts);

    Bucket::iterator end = m_buckets[bucket].end ();
    for (Bucket::iterator i = m_buckets[bucket].begin (); i != end; ++i)
    {
    if (i->key.m_uid == ev.key.m_uid)
    {
    m_buckets[bucket].erase (i);

    m_qSize--;
    ResizeDown ();
    return;
    }
    }
    }
    */

  private:
    /** Double the number of buckets if necessary. */
    void ResizeUp (void)
    {

      if (m_qSize > m_nBuckets * 2
          && m_nBuckets < 32768)
      {
        Resize (m_nBuckets * 2);
      }
    }
    /** Halve the number of buckets if necessary. */
    void ResizeDown (void)
    {

      if (m_qSize < m_nBuckets / 2)
      {
        Resize (m_nBuckets / 2);
      }
    }
    /**
     * Resize to a new number of buckets, with automatically computed width.
     *
     * \param [in] newSize The new number of buckets.
     */
    void Resize (uint32_t newSize)
    {

      // PrintInfo ();
      uint32_t newWidth = CalculateNewWidth ();
      DoResize (newSize, newWidth);
    }
    /**
     * Compute the new bucket size, based on up to the first 25 entries.
     *
     * \returns The new width.
     */
    uint32_t CalculateNewWidth (void)
    {

      if (m_qSize < 2)
      {
        return 1;
      }
      uint32_t nSamples;
      if (m_qSize <= 5)
      {
        nSamples = m_qSize;
      }
      else
      {
        nSamples = 5 + m_qSize / 10;
      }
      if (nSamples > 25)
      {
        nSamples = 25;
      }

      // we gather the first nSamples from the queue
      std::list<Event> samples;
      // save state
      uint32_t lastBucket = m_lastBucket;
      uint64_t bucketTop = m_bucketTop;
      uint64_t lastPrio = m_lastPrio;

      // gather requested events
      for (uint32_t i = 0; i < nSamples; i++)
      {
        samples.push_back (DoRemoveNext ());
      }
      // put them back
      for (std::list<Event>::const_iterator i = samples.begin ();
          i != samples.end (); ++i)
      {
        DoInsert (*i);
      }

      // restore state.
      m_lastBucket = lastBucket;
      m_bucketTop = bucketTop;
      m_lastPrio = lastPrio;

      // finally calculate inter-time average over samples.
      uint64_t totalSeparation = 0;
      std::list<Event>::const_iterator end = samples.end ();
      std::list<Event>::const_iterator cur = samples.begin ();
      std::list<Event>::const_iterator next = cur;
      next++;
      while (next != end)
      {
        totalSeparation += next->key.m_ts - cur->key.m_ts;
        cur++;
        next++;
      }
      uint64_t twiceAvg = totalSeparation / (nSamples - 1) * 2;
      totalSeparation = 0;
      cur = samples.begin ();
      next = cur;
      next++;
      while (next != end)
      {
        uint64_t diff = next->key.m_ts - cur->key.m_ts;
        if (diff <= twiceAvg)
        {
          totalSeparation += diff;
        }
        cur++;
        next++;
      }

      totalSeparation *= 3;
      totalSeparation = std::max (totalSeparation, (uint64_t)1);
      return totalSeparation;
    }
    /**
     * Initialize the calendar queue.
     *
     * \param [in] nBuckets The number of buckets.
     * \param [in] width The bucket size, in dimensionless time units.
     * \param [in] startPrio The starting time.
     */
    void Init (uint32_t nBuckets,
        uint64_t width,
        uint64_t startPrio)
    {
      m_buckets = new Bucket [nBuckets];
      m_nBuckets = nBuckets;
      m_width = width;
      m_lastPrio = startPrio;
      m_lastBucket = Hash (startPrio);
      m_bucketTop = (startPrio / width + 1) * width;
    }
    /**
     * Hash the dimensionless time to a bucket.
     *
     * \param [in] key The dimensionless time.
     * \returns The bucket index.
     */
    inline uint32_t Hash (uint64_t ts) const
    {

      uint32_t bucket = (ts / m_width) % m_nBuckets;
      return bucket;
    }

    /** Print the configuration and bucket size distribution. */
    void PrintInfo (void)
    {

      std::cout << "nBuckets=" << m_nBuckets << ", width=" << m_width << std::endl;
      std::cout << "Bucket Distribution ";
      for (uint32_t i = 0; i < m_nBuckets; i++)
      {
        std::cout << m_buckets[i].size () << " ";
      }
      std::cout << std::endl;
    }
    /**
     * Resize the number of buckets and width.
     *
     * \param [in] newSize The number of buckets.
     * \param [in] newWidth The size of the new buckets.
     */
    void DoResize (uint32_t newSize, uint32_t newWidth)
    {

      Bucket *oldBuckets = m_buckets;
      uint32_t oldNBuckets = m_nBuckets;
      Init (newSize, newWidth, m_lastPrio);

      for (uint32_t i = 0; i < oldNBuckets; i++)
      {
        Bucket::iterator end = oldBuckets[i].end ();
        for (Bucket::iterator j = oldBuckets[i].begin (); j != end; ++j)
        {
          DoInsert (*j);
        }
      }
      delete [] oldBuckets;
    }
    /**
     * Remove the earliest event.
     *
     * \returns The earliest event.
     */
    Event DoRemoveNext (void)
    {

      uint32_t i = m_lastBucket;
      uint64_t bucketTop = m_bucketTop;
      int32_t minBucket = -1;
      EventKey minKey;
      minKey.m_ts = uint64_t(-int64_t(1));
      minKey.m_uid = 0;
      //minKey.m_context = 0xffffffff;
      do
      {
        if (!m_buckets[i].empty ())
        {
          Event next = m_buckets[i].front ();
          if (next.key.m_ts < bucketTop)
          {
            m_lastBucket = i;
            m_lastPrio = next.key.m_ts;
            m_bucketTop = bucketTop;
            m_buckets[i].pop_front ();
            return next;
          }
          if (next.key < minKey)
          {
            minKey = next.key;
            minBucket = i;
          }
        }
        i++;
        i %= m_nBuckets;
        bucketTop += m_width;
      }
      while (i != m_lastBucket);

      m_lastPrio = minKey.m_ts;
      m_lastBucket = Hash (minKey.m_ts);
      m_bucketTop = (minKey.m_ts / m_width + 1) * m_width;
      Event next = m_buckets[minBucket].front();
      m_buckets[minBucket].pop_front ();

      return next;
    }
    /**
     * Insert a new event in to the correct bucket.
     *
     * \param [in] ev The new Event.
     */
    void DoInsert (const Event &ev)
    {
      // calculate bucket index.
      uint32_t bucket = Hash (ev.key.m_ts);

      // insert in bucket list.
      Bucket::iterator end = m_buckets[bucket].end ();
      for (Bucket::iterator i = m_buckets[bucket].begin (); i != end; ++i)
      {
        if (ev.key < i->key)
        {
          m_buckets[bucket].insert (i, ev);
          return;
        }
      }
      m_buckets[bucket].push_back (ev);
    }

    /** Calendar bucket type: a list of Events. */
    typedef std::list<Event> Bucket;

    /** Array of buckets. */
    Bucket *m_buckets;
    /** Number of buckets in the array. */
    uint32_t m_nBuckets;
    /** Duration of a bucket, in dimensionless time units. */
    uint64_t m_width;
    /** Bucket index from which the last event was dequeued. */
    uint32_t m_lastBucket;
    /** Priority at the top of the bucket from which last event was dequeued. */
    uint64_t m_bucketTop;
    /** The priority of the last event removed. */
    uint64_t m_lastPrio;
    /** Number of events in queue. */
    uint32_t m_qSize;
};


#endif /* CALENDAR_SCHEDULER_H */
