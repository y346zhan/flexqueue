#include <boost/container/flat_set.hpp>
#include <random>
#include <set>
#include "TestCase.h"
#include "benchmark-common.h"
#include "Bitmap.h"

/**
 * This test case attempts to plot the memory consumption of a flatset,
 * use with steadystate harness.
 */
class FlatsetMemoryTestCase : public TestCase {
  //boost::container::flat_set<size_t> _set;
  std::set<size_t> _set;
  std::random_device _rd;
  std::mt19937_64 _rng;
  size_t _counter;

public:
  FlatsetMemoryTestCase() : _rng(_rd()), _counter(_rng()) {
    INFO("This test ignores distribution and num_repeat override");
    INFO("Flatset initial size=" << _set.size());
    INFO("Selected=" << _counter);
  }

  virtual void setUp() { }

  virtual void run(int repeat) {
    for (int i=0; i<repeat; i++) {
      _set.insert(_counter++);
    }
  }

  virtual void tearDown() {
  }

  virtual ~FlatsetMemoryTestCase() {
    INFO("Flatset final size=" << _set.size());
    INFO("Flatset peek=" << *_set.begin());
    _set.clear();
  }
};
