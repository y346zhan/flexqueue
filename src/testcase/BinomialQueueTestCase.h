#include "TestCase.h"
#include "benchmark-common.h"
#include "BinomialQueue.h"
#include "ArrivalModel.h"

class BinomialQueueTestCase : public TestCase {

  public:
  BinomialQueueTestCase() {
    INFO("BinomialQueueTestCase: " << " queueSize=" << TP::queueSize);
    _queue = new BinomialQueue();
  }

  virtual void setUp() {
    TestCase::setUp();
    DEBUG("BinomialQueueTestCase setUp size=" << _queue->Size());
  }

  virtual void run(int repeat) {
    for (int i=0; i<repeat; i++) {
      Event e = _queue->RemoveNext();
      //assert(e.key.m_ts >= GetCurrentTime());
      SetCurrentTime(e.key.m_ts);
      size_t n = _model->next();
      //assert(e.key.m_ts >= GetCurrentTime());  // prevent underflow
      e.key.m_ts = n + GetCurrentTime(); // a random interval + now
      e.key.m_uid = _uid_counter++;
      _queue->Insert(e);
    }
  }

  virtual void tearDown() {
  }

  virtual ~BinomialQueueTestCase() {
    delete _queue;
  }
};
