#include <random>
#include "TestCase.h"
#include "benchmark-common.h"
#include "FlexQueue.h"
#include "ArrivalModel.h"

class FlexQueueTestCase : public TestCase {

  public:
  FlexQueueTestCase() {
    INFO("FlexQueueTestCase");
    _queue = new FlexQueue();
  }

  virtual void setUp() {
    TestCase::setUp();
    DEBUG("FlexQueueTestCase setUp size=" << _queue->Size());
  }

  virtual void run(int repeat) {
    for (int i=0; i<repeat; i++) {
      Event e = _queue->RemoveNext();
      SetCurrentTime(e.key.m_ts);
      size_t n = _model->next();
      e.key.m_ts = n + GetCurrentTime(); // a random interval + now
      e.key.m_uid = _uid_counter++;
      _queue->Insert(e);
    }
  }

  virtual void tearDown() {
    DEBUG("FlexQueueTestCase tearDown size=" << _queue->Size());
  }

  virtual ~FlexQueueTestCase() {
    delete _queue;
  }
};
