#include <random>
#include <vector>
#include "TestCase.h"
#include "benchmark-common.h"
#include "FlexQueue.h"
#include "ArrivalModel.h"

/**
 * Randomly sets and clears a bit in a bitvector with a given capacity and base
 * width
 *
 * BITCOUNT=X, DIST=UNIFORM, HORIZON=X
 */
class FanoutEffectTestCase : public TestCase {
  HierarchicalBitmap<TP::baseWidth, ceilinglog2(TP::bitCount)> V;
  bufptr_t buf;

  public:
  FanoutEffectTestCase() {
    buf = new buf_t[V.memsize(TP::bitCount)]();
    V.init(TP::bitCount, buf);
  }

  virtual void setUp() {
  }

  virtual void run(int repeat) {
    for (int i=0; i<repeat; i++) {
      V.set(_model->next());
      V.clr(V.find());
    }
  }

  virtual void tearDown() {
  }

  virtual ~FanoutEffectTestCase() {
    delete buf;
  }
};
