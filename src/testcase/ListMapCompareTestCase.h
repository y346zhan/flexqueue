#include <random>
#include <chrono>
#include <vector>
#include <set>
#include <list>
#include <boost/pool/pool_alloc.hpp>
#include "TestCase.h"
#include "benchmark-common.h"
#include "ArrivalModel.h"

/**
 * Performs hold on a hybrid queue that uses bitmap for events inside horizon, 
 * and STL map for events outside.
 */
class ListMapCompareTestCase : public TestCase {
  static const int        NUM_HOLD = 1000000;

  uint64_t _uid_counter_map = 0;
  uint64_t _uid_counter_list = 0;

  std::set<Event> _set;
  std::list<Event> _list;

  public:
  ListMapCompareTestCase() {
    INFO("Starting ListMap compare");
    INFO("params:"
        << " queueSize=" << TP::queueSize
        << " prob_bkt=" << TP::piecewise::percent
        << " base_width=" << TP::baseWidth
        << " horizon=" << TP::uniform::horizon
        );
  }

  virtual void setUp() {
    SetCurrentTime(0);
    for (size_t j = 0; j < TP::queueSize; j++) {
      Event e;
      e.key.m_ts = _model->next();
      e.key.m_uid = _uid_counter_map++;
      _uid_counter_list++;
      _set.insert(e);
      _list.push_back(e);
    }
    INFO("start mts=" << _set.begin()->key.m_ts);
    INFO("size begin=" << _set.size());
    auto start = std::chrono::steady_clock::now();
    mapRun();
    auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
    INFO("map: " << elapsed.count());
    INFO("size end=" << _set.size());
    INFO("end mts=" << _set.begin()->key.m_ts);

    INFO("start mts=" << _list.begin()->key.m_ts);
    INFO("size begin=" << _list.size());
    start = std::chrono::steady_clock::now();
    listRun();
    end = std::chrono::steady_clock::now();
    elapsed = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
    INFO("list: " << elapsed.count());
    INFO("size end=" << _list.size());
    INFO("end mts=" << _list.begin()->key.m_ts);
  }

  void mapRun() {
    SetCurrentTime(0);
    for (int i=0; i<NUM_HOLD; i++) {
      Event e = *_set.begin();
      _set.erase(_set.begin());
      SetCurrentTime(e.key.m_ts);
      e.key.m_ts = _model->next() + GetCurrentTime(); // a random interval + now
      e.key.m_uid = _uid_counter_map++;
      _set.insert(e);
    }
  }

  void listRun() {
    SetCurrentTime(0);
    for (int i=0; i<NUM_HOLD; i++) {
      Event e = _list.front();
      _list.pop_front();
      SetCurrentTime(e.key.m_ts);
      e.key.m_ts = _model->next() + GetCurrentTime(); // a random interval + now
      e.key.m_uid = _uid_counter_list++;
      _list.push_back(e);
    }
  }

  virtual void run(int repeat) { }
  virtual void tearDown() { }
  virtual ~ListMapCompareTestCase() { }
};
