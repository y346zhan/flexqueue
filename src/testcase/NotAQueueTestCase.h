#include <random>
#include <vector>
#include "TestCase.h"
#include "benchmark-common.h"
#include "NotAQueue.h"
#include "ArrivalModel.h"

/**
 * This test case performs hold operations as normal, but on a queue that does 
 * nothing on insert/remove. This is to get an idea of the impact of _model
 */
class NotAQueueTestCase : public TestCase {

public:
  NotAQueueTestCase() {
    INFO("Starting hold without queue operations, "
        << " queueSize=" << TP::queueSize
        );
    _queue = new NonQueue();
  }

  virtual void setUp() {
    SetCurrentTime(0);
    for (size_t j = 0; j < TP::queueSize; j++) {
      Event e{};
      e.key.m_ts = _model->next();
      e.key.m_uid = _uid_counter++;
      _queue->Insert(e);
    }
    INFO("start mts=" << _queue->Peek().key.m_ts);
    INFO("size begin=" << _queue->Size());
  }

  virtual void run(int repeat) {
    for (int i=0; i<repeat; i++) {
      Event e = _queue->RemoveNext();
      SetCurrentTime(e.key.m_ts);
      size_t n = _model->next();
      e.key.m_ts = n + GetCurrentTime(); // a random interval + now
      e.key.m_uid = _uid_counter++;
      _queue->Insert(e);
    }
  }

  virtual void tearDown() {
    INFO("size end=" << _queue->Size());
    INFO("end mts=" << _queue->Peek().key.m_ts);
  }

  virtual ~NotAQueueTestCase() {
    delete _queue;
  }
};
