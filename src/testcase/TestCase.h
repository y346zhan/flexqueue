#ifndef _TestCase_h_
#define _TestCase_h_
#include <random>
#include <map>
#include <string>
#include "benchmark-common.h"
#include "ArrivalModel.h"

class TestCase {
  std::random_device _rd;
  std::map<std::string, ArrivalModel*(*)(size_t)> modelMap = {
    {"piecewise", createArrivalModel< PiecewiseArrival<TP::piecewise::percent> >},
    {"uniform", createArrivalModel< UniformArrival >},
    {"normal", createArrivalModel< NormalArrival<TP::normal::mean, TP::normal::stddev> >},
    {"piecewise_simple", createArrivalModel< PiecewiseSimpleArrival<TP::piecewise::percent> >},
    {"triangular", createArrivalModel< TriangularArrival<TP::triangular::a, TP::triangular::b> >},
  };
protected:
  ArrivalModel *_model;
  QueueI *_queue;
  size_t _uid_counter;

public:
  TestCase() {
    _model = modelMap[XSTR(DISTRIBUTION)](_rd());
    _uid_counter = 0;
  }

  virtual void setUp() {
    SetCurrentTime(0);
    srand(_rd());

    Event last;
    while (_queue->Size() < TP::queueSize) {
      if (rand() % 100 >= 45) {
        // prbability = 0.55
        Event e;
        e.key.m_ts = _model->next() + last.key.m_ts;
        e.key.m_uid = _uid_counter++;
        _queue->Insert(e);
      } else if (_queue->Size() != 0) {
        // probability = 0.45
        last = _queue->RemoveNext();
        SetCurrentTime(last.key.m_ts);
      }
    }
  }

  virtual void run(int repeat) {}
  virtual void tearDown() {}
  virtual ~TestCase() {}
};

template<typename T> TestCase * createTestCase() { return new T(); }

#endif
