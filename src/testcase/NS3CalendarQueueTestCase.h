#include <random>
#include <vector>
#include "TestCase.h"
#include "benchmark-common.h"
#include "NS3CalendarQueue.h"
#include "ArrivalModel.h"

class NS3CalendarQueueTestCase : public TestCase {

public:

  NS3CalendarQueueTestCase() {
    INFO("CalendarQueueTestCase");
    _queue = new NS3CalendarQueue();
  }

  virtual void setUp() {
    TestCase::setUp();
    DEBUG("NS3CalendarQueueTestCase setUp size=" << _queue->Size());
  }

  virtual void run(int repeat) {
    for (int i=0; i<repeat; i++) {
      Event e = _queue->RemoveNext();
      SetCurrentTime(e.key.m_ts);
      e.key.m_ts = _model->next() + GetCurrentTime(); // a random interval + now
      e.key.m_uid = _uid_counter++;
      _queue->Insert(e);
    }
  }

  virtual void tearDown() {
    DEBUG("NS3CalendarQueueTestCase tearDown size=" << _queue->Size());
  }

  virtual ~NS3CalendarQueueTestCase() {
    delete _queue;
  }
};
