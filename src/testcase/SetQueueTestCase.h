#include <random>
#include <vector>
#include "TestCase.h"
#include "benchmark-common.h"
#include "SetQueue.h"
#include "ArrivalModel.h"

class SetQueueTestCase : public TestCase {

  public:
  SetQueueTestCase() {
    INFO("SetQueueTestCase: "
        << " queueSize=" << TP::queueSize
        );
    _queue = new SetQueue();
  }

  virtual void setUp() {
    TestCase::setUp();
    INFO("SetQueueTestCase setUp size=" << _queue->Size());
  }

  virtual void run(int repeat) {
    for (int i=0; i<repeat; i++) {
      Event e = _queue->RemoveNext();
      //assert(e.key.m_ts >= GetCurrentTime());
      SetCurrentTime(e.key.m_ts);
      size_t n = _model->next();
      //assert(e.key.m_ts >= GetCurrentTime());  // prevent underflow
      e.key.m_ts = n + GetCurrentTime(); // a random interval + now
      e.key.m_uid = _uid_counter++;
      _queue->Insert(e);
    }
  }

  virtual void tearDown() {
    INFO("size end=" << _queue->Size());
    INFO("end mts=" << _queue->Peek().key.m_ts);
  }

  virtual ~SetQueueTestCase() {
    delete _queue;
  }
};
