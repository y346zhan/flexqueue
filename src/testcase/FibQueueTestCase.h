#include "TestCase.h"
#include "benchmark-common.h"
#include "FibQueue.h"
#include "ArrivalModel.h"

class FibQueueTestCase : public TestCase {

  public:
  FibQueueTestCase() {
    INFO("FibQueueTestCase: " << " queueSize=" << TP::queueSize);
    _queue = new FibQueue();
  }

  virtual void setUp() {
    TestCase::setUp();
    DEBUG("FibQueueTestCase setUp size=" << _queue->Size());
  }

  virtual void run(int repeat) {
    for (int i=0; i<repeat; i++) {
      Event e = _queue->RemoveNext();
      //assert(e.key.m_ts >= GetCurrentTime());
      SetCurrentTime(e.key.m_ts);
      size_t n = _model->next();
      e.key.m_ts = n + GetCurrentTime(); // a random interval + now
      assert(e.key.m_ts >= GetCurrentTime());  // prevent underflow
      e.key.m_uid = _uid_counter++;
      _queue->Insert(e);
    }
  }

  virtual void tearDown() {
    DEBUG("FibQueueTestCase tearDown size=" << _queue->Size());
  }

  virtual ~FibQueueTestCase() {
    delete _queue;
  }
};
