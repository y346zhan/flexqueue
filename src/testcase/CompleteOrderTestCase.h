#include <random>
#include <vector>
#include "TestCase.h"
#include "benchmark-common.h"
#include "SetQueue.h"
#include "FlexQueue.h"
#include "ArrivalModel.h"
#include "FibQueue.h"

/**
 * Performs a check to see if the order of removal is identical to a heap 
 * implementation.
 */
class CompleteOrderTestCase : public TestCase {
  SetQueue    _mapQueue;
  FlexQueue   _testQueue;

  public:
  CompleteOrderTestCase() {
    INFO("Starting order check on \"" << typeid(_mapQueue).name()
        << "\" and \"" << typeid(_testQueue).name() << "\"");
    INFO("params: queueSize=" << TP::queueSize);
  }

  virtual void setUp() {
    SetCurrentTime(0);
    for (uint64_t j = 0; j < TP::queueSize; j++) {
      Event e;
      e.key.m_ts = _model->next();
      e.key.m_uid = _uid_counter++;
      _mapQueue.Insert(e);
      _testQueue.Insert(e);

      Event p1 = _mapQueue.Peek();
      Event p2 = _testQueue.Peek();
      assert(p1 == p2);
    }
    INFO("start mts=" << _mapQueue.Peek().key.m_ts);
    INFO("size begin=" << _mapQueue.Size());
    INFO("time now=" << GetCurrentTime());
  }

  virtual void run(int repeat) {
    for (int i=0; i<repeat; i++) {
      Event e = _mapQueue.RemoveNext();
      Event e2 = _testQueue.RemoveNext();

      assert(e.key.m_ts >= GetCurrentTime());
      if (e != e2) {
        INFO(
            "expected = {id=" << e.key.m_uid << ", ts=" << e.key.m_ts << "}, " <<
            "but got = {id=" << e2.key.m_uid << ", ts=" << e2.key.m_ts << "}"
            );
        assert(false);
      }

      SetCurrentTime(e.key.m_ts);
      e.key.m_ts = _model->next() + GetCurrentTime(); // a random interval + now
      assert(e.key.m_ts >= GetCurrentTime());  // prevent underflow
      e.key.m_uid = _uid_counter++;
      _mapQueue.Insert(e);
      _testQueue.Insert(e);
    }
  }

  virtual void tearDown() {
    DEBUG("size end=" << _mapQueue.Size());
    DEBUG("end mts=" << _mapQueue.Peek().key.m_ts);
  }

  virtual ~CompleteOrderTestCase() { }
};
