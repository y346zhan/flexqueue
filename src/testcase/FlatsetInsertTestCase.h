#include <random>
#include <vector>
#include "TestCase.h"
#include "benchmark-common.h"
#include "FlexQueue.h"
#include "ArrivalModel.h"
#include <boost/pool/pool_alloc.hpp>
#include <boost/container/flat_set.hpp>

/**
 * Performs hold on a flex queue that uses bitmap for events inside horizon, 
 * and STL map for events outside.
 */
class FlatsetInsertTestCase : public TestCase {
  boost::container::flat_set<Event> _fset;

public:
  FlatsetInsertTestCase() {
    INFO("FlatsetInsertTestCase ctor,"
        << " queueSize=" << TP::queueSize
        );
  }

  virtual void setUp() {
    SetCurrentTime(0);
    for (size_t j = 0; j < TP::queueSize; j++) {
      Event e{};
      e.key.m_ts = _model->next();
      e.key.m_uid = _uid_counter++;
      _fset.insert(e);
    }
    INFO("FlatsetInsertTestCase setUp size=" << _fset.size());
  }

  virtual void run(int repeat) {
    for (int i=0; i<repeat; i++) {
      Event e = *_fset.begin();
      _fset.erase(_fset.begin());
      //assert(e.key.m_ts >= GetCurrentTime());
      SetCurrentTime(e.key.m_ts);
      size_t n = _model->next();
      e.key.m_ts = n + GetCurrentTime(); // a random interval + now
      assert(e.key.m_ts >= GetCurrentTime());  // prevent underflow
      e.key.m_uid = _uid_counter++;
      _fset.insert(e);
    }
  }

  virtual void tearDown() {
    INFO("FlatsetInsertTestCase tearDown size=" << _fset.size());
  }

  virtual ~FlatsetInsertTestCase() { }
};
