#include <random>
#include <vector>
#include <list>
#include <set>
#include <cassert>
#include "TestCase.h"
#include "benchmark-common.h"
#include "Bitmap.h"


/**
 * Sanity checks on HierarchicalBitmap, makes sure nothing is broken
 */
class SanityCheckTestCase : public TestCase {

  public:
    SanityCheckTestCase() {
      INFO("Starting sanity checks; numbers below are meaningless");
      INFO("sizeof(Bitmap<64>) = " << sizeof(Bitmap<64>));
      INFO("sizeof(list<Event>) = " << sizeof(std::list<Event>));
      INFO("sizeof(set<Event>) = " << sizeof(std::set<Event>));
    }

    virtual void setUp() {
      // abuse this as we only need to run the test once
      testSetClr<1ull << 6>();
      testSetClr<1ull << 32>();
      testSetClr<1ull << 20>();
      testSetClr<1ull << 17>();
    }

    template <size_t w>
      void testSetClr() {
        HierarchicalBitmap<64, floorlog2(w)> map;
        bufptr_t str = new buf_t[map.memsize(w)]();
        INFO("HierarchicalBitmap using " << map.memsize(w) << " bytes, capacity = " << w);
        map.init(w, str);
        assert(map.find() == limit<size_t>());
        map.set(0);
        assert(map.findnext(w-1) == limit<size_t>());
        map.clr(0);
        map.set(w / 2);
        map.set(w / 4);
        map.set(w / 4 * 3);
        assert(map.find() == w / 4);
        map.clr(w / 4);
        assert(map.find() == w / 2);
        delete []str;
        INFO("Test passed...");
      }

    // nothing
    virtual void run(int repeat) { }

    // nothing
    virtual void tearDown() { INFO("All tests passed"); }

    virtual ~SanityCheckTestCase() { }
};
