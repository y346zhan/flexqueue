#ifndef ALL_TEST_CASES_H
#define ALL_TEST_CASES_H

#include "BitmapQueueTestCase.h"
#include "SanityCheckTestCase.h"
#include "NS3CalendarQueueTestCase.h"
#include "CompleteOrderTestCase.h"
#include "ListMapCompareTestCase.h"
#include "NotAQueueTestCase.h"
#include "FlatsetMemoryTestCase.h"
#include "FlatsetInsertTestCase.h"
#include "FanoutEffectTestCase.h"

#include "SetQueueTestCase.h"
#include "FlexQueueTestCase.h"
#include "FibQueueTestCase.h"
#include "BinomialQueueTestCase.h"

#endif
