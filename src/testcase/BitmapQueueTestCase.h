#include <random>
#include <vector>
#include "TestCase.h"
#include "benchmark-common.h"
#include "BitmapQueue.h"
#include "ArrivalModel.h"

/**
 * Performs "hold" on a hierarchical bitmap
 * Quotes because Insert may set a bit that's already set, implying that Size()
 * decreases with each "hold"
 */
class BitmapQueueTestCase : public TestCase {

  public:
  BitmapQueueTestCase() {
    INFO("Starting bitmap hold");
    INFO("params:"
        << " queueSize=" << TP::queueSize
        << " base_width=" << TP::baseWidth
        << " horizon=" << TP::uniform::horizon
        );
    _queue = new BitmapQueue();
  }

  virtual void setUp() {
    SetCurrentTime(0);
    for (uint64_t j = 0; j < TP::queueSize; j++) {
      Event e;
      e.key.m_ts = _model->next();
      e.key.m_uid = _uid_counter++;
      _queue->Insert(e);
    }
    INFO("start mts=" << _queue->Peek().key.m_ts);
    INFO("size begin=" << _queue->Size());
  }

  virtual void run(int repeat) {
    for (int i=0; i<repeat; i++) {
      Event e = _queue->RemoveNext();
      SetCurrentTime(e.key.m_ts);
      e.key.m_ts = _model->next() + GetCurrentTime();
      e.key.m_uid = _uid_counter++;
      _queue->Insert(e);
    }
  }

  virtual void tearDown() {
    INFO("size end=" << _queue->Size());
    INFO("end mts=" << _queue->Peek().key.m_ts);
  }

  virtual ~BitmapQueueTestCase() {
  }
};
