#include "wrapper.h"
#include <stdlib.h>

#ifdef _c_impl_
    #include "BitmapC.h"
    #include "HierarchicalBitmapC.h"
#else
    #include "Bitmap.h"
#endif

void hmap_alloc(HMap **self) {
#if !defined (_c_impl_)
    *self = new HMap;
#else
    *self = (HMap*)malloc(sizeof(HMap));
#endif
}

void hmap_dealloc(HMap **self) {
#if !defined (_c_impl_)
    delete *self;
#else
    free(*self);
#endif
    *self = NULL;
}

size_t hmap_allocsize(HMap *self, size_t bitcount) {
#if !defined (_c_impl_)
    return self->allocsize(bitcount);
#else
    return hm_allocsize(bitcount);
#endif
}

void hmap_init( HMap *self, size_t bitcount, bufptr_t p ) {
#if !defined (_c_impl_)
    return self->init(bitcount, p);
#else
    return hm_init(self, bitcount, p);
#endif
}

void hmap_set( HMap *self, size_t idx) {
#if !defined (_c_impl_)
    self->set(idx);
#else
    hm_set(self, idx);
#endif
}

void hmap_clr( HMap *self, size_t idx) {
#if !defined (_c_impl_)
    self->clr(idx);
#else
    hm_clr(self, idx);
#endif
}

bool hmap_test( HMap *self, size_t idx ) {
#if !defined (_c_impl_)
    return self->test(idx);
#else
    return hm_test(self, idx);
#endif
}

mword hmap_find( HMap *self ) {
#if !defined (_c_impl_)
    return self->find();
#else
    return hm_find(self);
#endif
}
