#include "PQueue.h"
#include "helper.h"

#include <iostream>
#include <fstream>
#include <chrono>
#include <cstdlib>

using namespace std;

TR PQHold(PQueue &pq) {
    const size_t trials = 1<<21;

    auto start = chrono::steady_clock::now();
    for (size_t i=0; i<trials; i++) {
        void *data = pq.dequeue();
        if (data == NULL) {
            cerr << "Should not happen" << endl;
            exit(-1);
        }
        pq.enqueue(rand(), data);
    }
    auto end = chrono::steady_clock::now();
    auto elapsed = chrono::duration_cast<chrono::microseconds>(end - start);

    if (pq.empty()) cerr << '\0';

#ifndef GNUPLOT
    cout << "PQHold :\t" << elapsed.count() << " ms" << endl;
#endif
    return TR(elapsed.count());
}


int main() {
    srand(123456);
    const size_t sample_size = 20;
    ofstream fout;

    fout.open(dirhelper("data/qhold.data").c_str());
    fout << "#i\ttime(ms)\tstddev" << endl;
    for (int i = 1; i <= 20; i+=1) {
        Samples qSample;
        PQueue pq;
        size_t size = 1<<i;

        // insert 2^i items ...
        while (size>0) {
            pq.enqueue(rand(), (void*)0xdeadbeef);
            size--;
        }

        // ...then do hold 1million trials for 20 times
        for (int j = 0; j < sample_size; j++) {
            qSample.put(PQHold(pq).r[0]);
        }

        fout << i << "\t";
        fout << (size_t)qSample.mean() << "\t" << (size_t)qSample.stddev();
        fout << endl;
    }
    fout.close();

    return 0;
}
