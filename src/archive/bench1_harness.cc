#include "wrapper.h"
#include "queue.h"
#include "hasharray.h"
#include "helper.h"

#include <cstdlib>
#include <chrono>
#include <ctime>
#include <cstdlib>
#include <memory>
#include <vector>
#include <array>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <unordered_set>
#include <string>
#include <cassert>

using namespace std;

#ifndef HOME_DIR
#error "no home directory defined"
#endif


// A dummy page structure
struct Page {
    LIST_ENTRY(Page) freelist;          // page can be on this list
    //LIST_ENTRY(Page) object_pages_list; // ..or on list of pages in the same object
};

// Head structure for freelist
LIST_HEAD(FreeListHead, Page);

// structure to refactor unnecessary code out of test cases
struct LL {
    Page *pages; // array
    FreeListHead *head;
    LL(size_t size) {
        pages = new Page[size]();
        head = new FreeListHead();
    }

    ~LL() {
        delete[] pages;
        delete head;
    }
};

struct HM {
    HMap *hmap;
    HashArray<size_t> freeItems;
    char *storage;
    size_t size;

    HM(size_t _size) : freeItems(0), size(_size) {
        hmap_alloc(&hmap);
        storage = new char[hmap_allocsize(hmap, _size)]();
        hmap_init(hmap, _size, storage);
    }

    //void initFreeItems(int percentage) {
    //    freeItems.reserve(size);
    //    for (size_t i=0; i<size; i++) {
    //        freeItems.insert(i);
    //    }
    //    //random_shuffle(order.begin(), order.end());
    //    for (size_t i=0; i<data_size / 100 * percentage; i++) {
    //        hmap->set(order[i]);
    //        freeItems.remove(order[i]);
    //    }
    //    if (hmap->test(std::rand() % data_size)) cerr << '\0'; // prevent optimization
    //}

    ~HM() {
        hmap_dealloc(&hmap);
        delete[] storage;
    }
};

void prepare_order(vector<size_t> &order) {
    for (size_t i=0; i<order.size(); i++) {
        order[i] = i;
    }
}

TR LLSequentialInsert(const size_t size, LL &ll) {
    //Page *pages = new Page[size]();
    //FreeListHead *head = new FreeListHead();

    auto start = chrono::steady_clock::now();
    for (unsigned long long i=0; i<size; i++) {
        LIST_INSERT_HEAD(ll.head, (ll.pages + i), freelist);
    }
    auto end = chrono::steady_clock::now();
    auto elapsed = chrono::duration_cast<chrono::microseconds>(end - start);

#ifndef GNUPLOT
    cout << "LLSequentialInsert :\t" << elapsed.count() << " ms" << endl;
#endif
    return TR(elapsed.count());

    //return TestResult(size / elapsed.count() * 1000000ull);
}

TR HMSequetialInsert(const size_t size, HM &hm) {
    //auto *hmap = new HMap(); // max width 2^40
    //HMap hmap;
    //char *storage = new char[hmap.allocsize(size)];
    //hmap.init(size, storage);

    auto start = chrono::steady_clock::now();
    for (unsigned long long i=0; i<size; i++) {
        hmap_set(hm.hmap, i);
    }
    auto end = chrono::steady_clock::now();
    auto elapsed = chrono::duration_cast<chrono::microseconds>(end - start);

    // access a bit to prevent compiler optimization
    if (hmap_test(hm.hmap, std::rand() % size)) cerr << '\0';

    //delete [] storage;
    //delete hmap;

    #ifndef GNUPLOT
    cout << "HMSequetialInsert :\t" << elapsed.count() << " ms" << endl;
    #endif
    return TR(elapsed.count());

}

TR LLRandomInsert(const size_t size, LL &ll, const vector<size_t> &order) {
    //assert(order.size() == size);
    //Page            *pages = new Page[size]();
    //FreeListHead    *head = new FreeListHead();
    //vector<size_t>  order(size);

    //prepare_order(order);
    //random_shuffle(order.begin(), order.end());

    auto start = chrono::steady_clock::now();
    for (unsigned long long i=0; i<size; i++) {
        Page *ele = ll.pages + order[i];
        LIST_INSERT_HEAD(ll.head, ele, freelist);
    }
    auto end = chrono::steady_clock::now();
    auto elapsed = chrono::duration_cast<chrono::microseconds>(end - start);

    //delete [] pages;
    //delete head;

#ifndef GNUPLOT
    cout << "LLRandomInsert :\t" << elapsed.count() << " ms" << endl;
#endif
    return TR(elapsed.count());
}

TR HMRandomInsert(const size_t size, HM &hm, const vector<size_t> &order) {
    //assert(order.size() == size);
    //HMap hmap;
    //har *storage = new char[hmap.allocsize(size)]; // actual width size
    //hmap.init(size, storage);
    //vector<size_t>  order(size);

    //prepare_order(order);
    //random_shuffle(order.begin(), order.end());

    auto start = chrono::steady_clock::now();
    for (unsigned long long i=0; i<size; i++) {
        hmap_set(hm.hmap, order[i]);
    }
    auto end = chrono::steady_clock::now();
    auto elapsed = chrono::duration_cast<chrono::microseconds>(end - start);
    if (hmap_test(hm.hmap, std::rand() % size)) cerr << '\0'; // access a bit to prevent compiler optimization

    //delete[] storage;

#ifndef GNUPLOT
    cout << "HMRandomInsert :\t" << elapsed.count() << " ms" << endl;
#endif
    return TR(elapsed.count());
}

TR LLRandomFillRemove(size_t percentage, size_t data_size, LL &ll, const vector<size_t> &order) {
    if (percentage > 100) return TR();

    //Page            *pages = new Page[data_size]();
    //FreeListHead    *head = new FreeListHead();
    //vector<size_t> order(data_size);

    // randomly selected a subset of pages to insert
    //for (unsigned long long i=0; i<data_size; i++) {
    //    order[i] = i;
    //}
    //random_shuffle(order.begin(), order.end());
    for (unsigned long long i=0; i<data_size / 100 * percentage; i++) {
        LIST_INSERT_HEAD(ll.head, ll.pages + order[i], freelist);
    }

    auto start = chrono::steady_clock::now();
    for (unsigned long long i=0; i<data_size / 100 * percentage; i++) {
        LIST_REMOVE(LIST_FIRST(ll.head), freelist);
    }
    auto end = chrono::steady_clock::now();
    auto elapsed = chrono::duration_cast<chrono::microseconds>(end - start);

    //delete head;
    //delete [] pages;

#ifndef GNUPLOT
    cout << "LLRandomFillRemove :\t" << elapsed.count() << " ms" << endl;
#endif
    return TR(elapsed.count());
}

TR HMRandomFillRemove(size_t percentage, size_t data_size, HM &hm, const vector<size_t> &order) {
    if (percentage > 100) return TR();

    //auto *hmap = new HMap(); // max width 2^40
    //char *storage = new char[hmap->allocsize(data_size)]; // actual width data_size
    //vector<size_t> order(data_size);
    //hmap->init(data_size, storage);

    // randomly selected a subset of bits to insert
    //for (size_t i=0; i<data_size; i++) {
    //    order[i] = i;
    //}
    //random_shuffle(order.begin(), order.end());
    for (size_t i=0; i<data_size / 100 * percentage; i++) {
        hmap_set(hm.hmap, order[i]);
        if (!hmap_test(hm.hmap, order[i])) {
            cerr << "failed to set bit " << order[i] << endl;
            exit(2);
        }
    }
    if (hmap_test(hm.hmap, std::rand() % data_size)) cerr << '\0';


    auto start = chrono::steady_clock::now();
    for (size_t i=0; i<data_size / 100 * percentage; i++) {
        hmap_clr(hm.hmap, hmap_find(hm.hmap));
    }
    auto end = chrono::steady_clock::now();
    auto elapsed = chrono::duration_cast<chrono::microseconds>(end - start);

    //delete [] storage;
    //delete hmap;

#ifndef GNUPLOT
    cout << "HMRandomFillRemove :\t" << elapsed.count() << " ms" << endl;
#endif
    return TR(elapsed.count());
}

TR LLHold(const unsigned percentage, const size_t data_size, const size_t trials, LL &ll, const vector<size_t> &order) {/*{{{*/
    if (percentage > 100) return TR();

    //auto initStart = chrono::system_clock::now();
    //Page            *pages = new Page[data_size]();
    //FreeListHead    *head = new FreeListHead();
    Page            *tail = ll.pages + order[0];
    //vector<size_t>  order(data_size);
    //HashArray<size_t>   freeItems(data_size);

    // set up initial state, `percentage`% full
    //for (size_t i=0; i<data_size; i++) {
    //    order[i] = i;
    //    freeItems.insert(i);
    //}

    //random_shuffle(order.begin(), order.end());
    for (size_t i=0; i<data_size / 100 * percentage; i++) {
        Page *p = ll.pages + order[i];
        LIST_INSERT_HEAD(ll.head, p, freelist);
        //freeItems.remove(order[i]);
    }
    //auto initEnd = chrono::system_clock::now();
    //auto initElapsed = chrono::duration_cast<chrono::microseconds>(initEnd - initStart);
    //cout << "init took " << initElapsed.count() << " ms" << endl;

    // now perform hold, for `trials` iterations
    // 1. remove head
    // 2. take a random free page, insert at the back
    auto start = chrono::steady_clock::now();
    for (unsigned long long i=0; i<trials; i++) {
        //size_t insertMe = freeItems.removeRandom();
        //freeItems.insert(LIST_FIRST(head) - pages);
        Page *p = LIST_FIRST(ll.head);

        LIST_REMOVE(p, freelist);
        LIST_INSERT_AFTER(tail, p, freelist);

        tail = p;
    }
    auto end = chrono::steady_clock::now();
    auto elapsed = chrono::duration_cast<chrono::microseconds>(end - start);

    //delete head;
    //delete [] pages;

#ifndef GNUPLOT
    cout << "LLHold :\t" << elapsed.count() << " ms" << endl;
#endif
    return TR(elapsed.count());
}

TR HMHold(const unsigned percentage, size_t data_size, const size_t trials, HM &hm, const vector<size_t> &order) {/*{{{*/
    if (percentage > 100) return TR();

    //auto *hmap = new HMap(); // max width 2^40
    //char *storage = new char[hmap->allocsize(data_size)]; // actual width data_size
    //vector<size_t> order(data_size);
    //hmap->init(data_size, storage);
    HashArray<size_t> freeItems(data_size);

    // set up initial state, `percentage`% full
    for (size_t i=0; i<data_size; i++) {
        //order[i] = i;
        freeItems.insert(i);
    }
    //random_shuffle(order.begin(), order.end());
    for (size_t i=0; i<data_size / 100 * percentage; i++) {
        hmap_set(hm.hmap, order[i]);
        freeItems.remove(order[i]);
    }
    if (hmap_test(hm.hmap, std::rand() % data_size)) cerr << '\0'; // prevent optimization
    // now do hold, for `trials` iterations
    // 1. clear head (find; clr)
    // 2. select random free bit, set it
    auto start = chrono::steady_clock::now();
    for (size_t i=0; i<trials; i++) {
        size_t k = freeItems.removeRandom();

        size_t j = hmap_find(hm.hmap);
        hmap_clr(hm.hmap, j);
        hmap_set(hm.hmap, k);

        freeItems.insert(j);
    }
    auto end = chrono::steady_clock::now();
    auto elapsed = chrono::duration_cast<chrono::microseconds>(end - start);

    //delete [] storage;
    //delete hmap;

#ifndef GNUPLOT
    cout << "HMHold :\t" << elapsed.count() << " ms" << endl;
#endif
    return TR(elapsed.count());
}/*}}}*/

// MAIN
int main() {
    srand(123456);
    const size_t data_size = 1ull << 25;
    const size_t giga = 1ull << 18; // 2^18 4k pages
    const size_t sample_size = 20;
    ofstream fout;

    fout.open(dirhelper("data/seqinsert.data").c_str());
    fout << "#i\tll\tstddev\thm\tstddev" << endl;
    for (int i = 1; i <= 64; i+=2) {
        Samples llSample, hmSample;
        for (int j = 0; j < sample_size; j++) {
            LL llstr(giga * i);
            HM hmstr(giga * i);

            llSample.put(LLSequentialInsert(giga * i, llstr).r[0]);
            hmSample.put(HMSequetialInsert(giga * i, hmstr).r[0]);
        }
        fout << i << "\t";
        fout << (size_t)llSample.mean() << "\t" << (size_t)llSample.stddev() << "\t";
        fout << (size_t)hmSample.mean() << "\t" << (size_t)hmSample.stddev();
        fout << endl;
    }
    fout.close();

    fout.open(dirhelper("data/randinsert.data"));
    fout << "#i\tll\tstddev\thm\tstddev" << endl;
    for (size_t i = 1 ; i <= 64; i+=2) {

        vector<size_t> order(giga * i); // LL and HM uses same random order
        prepare_order(order);

        Samples llSample, hmSample;
        for (int j = 0; j < sample_size; j++) { // average of 20 runs
            LL llstr(giga * i);
            HM hmstr(giga * i);

            random_shuffle(order.begin(), order.end());

            llSample.put(LLRandomInsert(giga * i, llstr, order).r[0]);
            hmSample.put(HMRandomInsert(giga * i, hmstr, order).r[0]);
        }
        fout << i << "\t";
        fout << (size_t)llSample.mean() << "\t" << (size_t)llSample.stddev() << "\t";
        fout << (size_t)hmSample.mean() << "\t" << (size_t)hmSample.stddev() << endl;
    }
    fout.close();

    fout.open(dirhelper("data/rfremove.data"));
    fout << "#i\tll\tstddev\thm\tstddev" << endl;
    for (int i=0; i<=100; i+=10) {
        Samples llSample, hmSample;
        vector<size_t> order(data_size);
        prepare_order(order);

        for (int j=0; j<sample_size; j++) {
            LL llstr(data_size);
            HM hmstr(data_size);

            random_shuffle(order.begin(), order.end());

            llSample.put(LLRandomFillRemove(i, data_size, llstr, order).r[0]);
            hmSample.put(HMRandomFillRemove(i, data_size, hmstr, order).r[0]);
        }
        fout << i << "\t";
        fout << (size_t)llSample.mean() << "\t" << (size_t)llSample.stddev() << "\t";
        fout << (size_t)hmSample.mean() << "\t" << (size_t)hmSample.stddev(); fout << endl;
    }
    fout.close();

    return 0;
}

