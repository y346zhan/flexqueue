#!/bin/zsh
for i in {1..24}; do  
    g++ -DHOME_DIR=\"/home/y346zhan\" -std=c++11 -iquote./include -Wall -g -pg -O3 -x c++ -DGNUPLOT -DINIT_CAPACITY=$i bench3_harness.cc bucketq.cc hybridq.cc mapq.cc -o ben3_i$i
    ./ben3_i$i 
    gprof ./ben3_i$i | grep cascade | head -n1
    #perf stat ./ben3_i$i 2>&1 | grep page-faults
    rm ben3_i$i
done
