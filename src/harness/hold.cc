#include <chrono>
#include <map>
#include <string>
#include <iostream>
#include "benchmark-common.h"
#include "TestCase.h"
#include "AllTestCases.h"

using namespace std;

size_t timer = 0;
size_t GetCurrentTime() { return timer; }
void SetCurrentTime(size_t _t) { timer = _t; }

const map<string, TestCase*(*)()> tcMap = {
  {"BitmapQueueTestCase", createTestCase<BitmapQueueTestCase>},
  {"NS3CalendarQueueTestCase", createTestCase<NS3CalendarQueueTestCase>},
  {"FlexQueueTestCase", createTestCase<FlexQueueTestCase>},
  {"SetQueueTestCase", createTestCase<SetQueueTestCase>},
  {"CompleteOrderTestCase", createTestCase<CompleteOrderTestCase>},
  {"SanityCheckTestCase", createTestCase<SanityCheckTestCase>},
  {"ListMapCompareTestCase", createTestCase<ListMapCompareTestCase>},
  {"NotAQueueTestCase", createTestCase<NotAQueueTestCase>},
  {"FlatsetMemoryTestCase", createTestCase<FlatsetMemoryTestCase>},
  {"FlatsetInsertTestCase", createTestCase<FlatsetInsertTestCase>},
  {"FanoutEffectTestCase", createTestCase<FanoutEffectTestCase>},
  {"FibQueueTestCase", createTestCase<FibQueueTestCase>},
  {"BinomialQueueTestCase", createTestCase<BinomialQueueTestCase>},
};

int main()
{
  const int num_trials = 20;
  const int num_repeat = 1000000;
  size_t time[num_trials] = {};

  assert(tcMap.find(XSTR(TEST_NAME)) != tcMap.end());
  TestCase *testCase = tcMap.at(XSTR(TEST_NAME))();

  testCase->setUp();
  for (int j = 0; j < num_trials + 1; j++) {
    auto start = chrono::steady_clock::now();
    testCase->run(num_repeat);
    auto end = chrono::steady_clock::now();
    auto elapsed = chrono::duration_cast<chrono::microseconds>(end - start);
    if (j != 0) { // discard the very first run only
      time[j - 1] = elapsed.count();
    }
  }
  testCase->tearDown();
  delete testCase;

  // print relevant labels, for readability
  cout << "#"; // gnuplot comment
  cout
    << X_LABEL_STRING
    << "time (us)\t"
    << "queue size\t"
    << "o_max\t"
    << "pct_bkt\t"
    << "o_max\t"
    << "mu\t"
    << "sigma\t"
    << endl;

  // print every sample for this point
  for (int i=0; i<num_trials; i++) {
    cout
      << X_VAR_STRING
      << time[i]
      << "\t" << TP::queueSize
      << "\t" << TP::uniform::horizon
      << "\t" << TP::piecewise::percent
      << "\t" << TP::piecewise::horizon
      << "\t" << TP::normal::mean
      << "\t" << TP::normal::stddev
      << endl;
  }
}
