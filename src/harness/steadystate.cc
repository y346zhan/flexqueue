#include <iostream>
#include <chrono>
#include <map>
#include <sstream>
#include <sys/resource.h>
#include "benchmark-common.h"
#include "TestCase.h"
#include "AllTestCases.h"
#if defined(__APPLE__) && defined(__MACH__)
#include <mach/mach.h>
#elif defined(__linux__) || defined(__linux) || defined(linux) || defined(__gnu_linux__)
#include <stdio.h>
#endif

using namespace std;

size_t timer = 0;
size_t GetCurrentTime() { return timer; }
void SetCurrentTime(size_t _t) { timer = _t; }

const map<string, TestCase*(*)()> tcMap = {
  {"BitmapQueueTestCase", createTestCase<BitmapQueueTestCase>},
  {"NS3CalendarQueueTestCase", createTestCase<NS3CalendarQueueTestCase>},
  {"FlexQueueTestCase", createTestCase<FlexQueueTestCase>},
  {"SetQueueTestCase", createTestCase<SetQueueTestCase>},
  {"CompleteOrderTestCase", createTestCase<CompleteOrderTestCase>},
  {"SanityCheckTestCase", createTestCase<SanityCheckTestCase>},
  {"ListMapCompareTestCase", createTestCase<ListMapCompareTestCase>},
  {"NotAQueueTestCase", createTestCase<NotAQueueTestCase>},
  {"FlatsetMemoryTestCase", createTestCase<FlatsetMemoryTestCase>},
  {"FlatsetInsertTestCase", createTestCase<FlatsetInsertTestCase>},
};

size_t getInuseMem() {
  struct rusage u;
  getrusage(RUSAGE_SELF, &u);
  return u.ru_maxrss / 1024;
}

size_t getCurrentRSS() {
#if defined(__APPLE__) && defined(__MACH__)
  /* OSX ------------------------------------------------------ */
  struct mach_task_basic_info info;
  mach_msg_type_number_t infoCount = MACH_TASK_BASIC_INFO_COUNT;
  if ( task_info( mach_task_self( ), MACH_TASK_BASIC_INFO,
        (task_info_t)&info, &infoCount ) != KERN_SUCCESS )
    return (size_t)0L;		/* Can't access? */
  return (size_t)info.resident_size;

#elif defined(__linux__) || defined(__linux) || defined(linux) || defined(__gnu_linux__)
  /* Linux ---------------------------------------------------- */
  long rss = 0L;
  FILE* fp = NULL;
  if ( (fp = fopen( "/proc/self/statm", "r" )) == NULL )
    return (size_t)0L;		/* Can't open? */
  if ( fscanf( fp, "%*s%ld", &rss ) != 1 )
  {
    fclose( fp );
    return (size_t)0L;		/* Can't read? */
  }
  fclose( fp );
  return (size_t)rss * (size_t)sysconf( _SC_PAGESIZE) / 1024 / 1024;

#else
  /* AIX, BSD, Solaris, and Unknown OS ------------------------ */
  return (size_t)0L;			/* Unsupported. */
#endif
}

// TODO this harness really should take measurement periodically using time, and
// not number of operations completed
int main()
{
  const int num_trials = 100;
  const int num_repeat = 100000;
  const int num_data_points = 50;
  assert(tcMap.find(XSTR(TEST_NAME)) != tcMap.end());

  TrialRun time[num_data_points] = {};

  for (int i = 0; i < num_trials; i++) {
    TestCase *testCase = tcMap.at(XSTR(TEST_NAME))();
    testCase->setUp();
    for (int j = 0; j < num_data_points; j++) {
      auto start = chrono::steady_clock::now();
      testCase->run(num_repeat);
      auto end = chrono::steady_clock::now();
      auto elapsed = chrono::duration_cast<chrono::microseconds>(end - start);
      time[j].addResult(elapsed.count());
    }
    testCase->tearDown();
    delete testCase;
  }

  stringstream label;
  stringstream data;

  // With this harness, x-axis is always the j-th chunk of hold, or time passed, 
  // and the y-axis is hold time.
  label << "#"; // gnuplot comment
  label
    << EXTRA_OUTPUT_VARS_LABEL
    << "Time (us)\t"
    << "Std dev\t"
    << "Chunk\t"
    << "Queue size\t";
  for (int j = 0; j < num_data_points; j++) {
    data
      << EXTRA_OUTPUT_VARS
      << (size_t)time[j].mean()
      << "\t" << (size_t)time[j].stddev()
      << "\t" << j
      << "\t" << TP::queue_size;

    string distribution (XSTR(DISTRIBUTION));
    if (distribution == "uniform") {
      label
        << "o_max\t";
      data
        << "\t" << TP::uniform::horizon;
    } else if (distribution == "piecewise" || distribution == "piecewise_simple") {
      label
        << "pct_bkt\t"
        << "o_max\t";
      data
        << "\t" << TP::piecewise::percent
        << "\t" << TP::piecewise::horizon;
    } else if (distribution == "normal") {
      label
        << "Mu\t"
        << "Sigma\t";
      data
        << "\t" << TP::normal::mean
        << "\t" << TP::normal::stddev;
    }
  }
}
