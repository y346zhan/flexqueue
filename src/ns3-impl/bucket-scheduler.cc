/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * The bitmap is 64-bit wide, divided into 4 groups of 16 bits, i.e. 4 different
 * granularities. Each granularity category has 2^16 buckets for a total of
 * 262144 buckets.
 *
 * For insertion, timestamp is converted to an index to the list of buckets, and
 * is inserted at the front of the bucket. The timestamp is assumed to be the
 * amount of time before the event should fire.
 *
 * For peek, each category needs to be searched separately, beginning with the
 * least significant category. A pointer in each category keeps track of
 * "current time" much like the hands of a clock. Search begins at this pointer
 * and wraps around. If and when this pointer wraps around, the next pointer in 
 * a higher category is search for potential candidates.  If no event is found 
 * in a category, the next significant category is searched in a similar 
 * fashion.
 *
 * Remove is like peek except "current time" advances to the time represented by
 * that event.
 */

#include "bucket-scheduler.h"
#include "event-impl.h"
#include "assert.h"
#include "log.h"
#include <string>
#include <iostream>

/**
 * \file
 * \ingroup scheduler
 * Implementation of ns3::BucketScheduler class.
 */

namespace ns3 {


NS_LOG_COMPONENT_DEFINE ("BucketScheduler");

NS_OBJECT_ENSURE_REGISTERED (BucketScheduler);

TypeId
BucketScheduler::GetTypeId (void)
{
    static TypeId tid = TypeId ("ns3::BucketScheduler")
        .SetParent<Scheduler> ()
        .SetGroupName ("Core")
        .AddConstructor<BucketScheduler> ()
        ;
    return tid;
}

BucketScheduler::BucketScheduler()
{
    LogComponentEnable ("BucketScheduler", LOG_LEVEL_INFO);

    size_t size = bitmap[0].memsize(num_buckets);
    if ( posix_memalign(&storage[0], 64, size) ) {
        NS_LOG_INFO("Error allocating cache aligned memory");
        exit(1);
    }
    if ( posix_memalign(&storage[1], 64, size) ) {
        NS_LOG_INFO("Error allocating cache aligned memory");
        exit(1);
    }

    memset(storage[0], 0, size);
    memset(storage[1], 0, size);
    bitmap[0].init(num_buckets, (char *)storage[0]);
    bitmap[1].init(num_buckets, (char *)storage[1]);
}

BucketScheduler::~BucketScheduler()
{
#ifdef NS3_BUILD_PROFILE_DEBUG
    NS_LOG_INFO(
            "num_map_insert: " << num_map_insert <<
            ", num_map_remove: " << num_map_remove <<
            ", num_bucket_insert: " << num_bucket_insert <<
            ", num_bucket_remove: " << num_bucket_remove
            );
    NS_LOG_INFO("There are " << num_item << " items remaining in the scheduler");
#endif

    free(storage[0]);
    free(storage[1]);
}

void
BucketScheduler::Insert (const Event &ev)
{
    NS_LOG_FUNCTION(this);
#ifdef NS3_BUILD_PROFILE_DEBUG
    num_item++;
    if (num_item > peak_item) peak_item = num_item;
#endif

    uint64_t now = Simulator::Now().GetTimeStep();
    ev.impl->key = ev.key;

    if (ev.key.m_ts - now < num_buckets) {
        uint64_t pos = ( (now & LOW16) + (ev.key.m_ts - now) );
        size_t bktPos = pos % num_buckets;
        size_t bmpPos = (front + pos / num_buckets) % 2;

        NS_LOG_INFO("Now=" << now << "(" << (now&LOW16) <<
                    ") InsertBKT:" <<
                    " uid=" << ev.key.m_uid <<
                    " ts=" << ev.key.m_ts <<
                    " at buckets[" << bktPos <<
                    "(" << pos <<
                    ")], setting bitmap[" << bmpPos <<
                    "].set(" << bktPos <<
                    ")");

        buckets[bktPos].insertFront(ev.impl);
        bitmap[bmpPos].set(bktPos);

#ifdef NS3_BUILD_PROFILE_DEBUG
        num_bucket_insert++;
#endif
    } else {
        mapList.insert (std::make_pair (ev.key, ev.impl));

#ifdef NS3_BUILD_PROFILE_DEBUG
        num_map_insert++;
#endif
    }
#ifdef NS3_BUILD_PROFILE_DEBUG
    goldList.insert (std::make_pair(ev.key, ev.impl));
#endif
}

bool
BucketScheduler::IsEmpty (void) const
{
    return bmEmpty() && mapList.empty();
}

Scheduler::Event
BucketScheduler::PeekNext (void) const
{
    Event ev;

    // NYI b/c never called
    NS_LOG_ERROR("PeekNext NYI");
    NS_ASSERT(false);
    return ev;
}


Scheduler::Event
BucketScheduler::RemoveNext (void)
{

    Event ev = RemoveNextCore();

    uint64_t now = Simulator::Now().GetTimeStep();

#ifdef NS3_BUILD_PROFILE_DEBUG
    EventMapI gIt = goldList.begin();
    if (gIt->first.m_ts != ev.key.m_ts) {   // Order within each bucket is not of concern
        NS_LOG_ERROR("ERROR: event removed different from gold");
        NS_LOG_INFO("Now=" << now << "(" << (now & LOW16) << ") BAD Remove: uid=" << ev.key.m_uid << " ts=" << ev.key.m_ts);
        NS_LOG_INFO("Now=" << now << "(" << (now & LOW16) << ") GOLD Remove: uid=" << gIt->first.m_uid << " ts=" << gIt->first.m_ts);
        exit(1);
    } else {
        NS_LOG_INFO("Now=" << now << "(" << (now & LOW16) << ") Remove: uid=" << ev.key.m_uid << " ts=" << ev.key.m_ts);
    }
    goldList.erase(gIt);
#endif

    if ((now & LOW16) > (ev.key.m_ts & LOW16)) {
        front = (front + 1) % 2;
    }

    return ev;
}

bool
BucketScheduler::bmEmpty() const
{
    return bitmap[0].empty() && bitmap[1].empty();
}

Scheduler::Event
BucketScheduler::RemoveNextCore (void)
{
    Event ev;
    uint64_t pos = bitmap[front].find0();
    int tmpFront = front;
    if (pos == limit<uint64_t>()) {
        tmpFront = (front + 1) % 2;
        pos = bitmap[tmpFront].find0();
    }
    EventMapI it = mapList.begin();

    // Need to check both
    if (!bmEmpty() && !mapList.empty()) {
        EventImpl *m = buckets[pos].front();
        if (m->key < it->first) {
            ev.impl = m;
            ev.key = m->key;
            buckets[pos].remove(m);
            if (buckets[pos].empty()) {
                bitmap[tmpFront].clr(pos);
            }

#ifdef NS3_BUILD_PROFILE_DEBUG
            num_bucket_remove++;
#endif
            NS_LOG_INFO("RemoveCore: pos=" << pos << " tmpFront=" << tmpFront << " front=" << front);
        } else {
            ev.impl = it->second;
            ev.key = it->first;
            mapList.erase (it);
#ifdef NS3_BUILD_PROFILE_DEBUG
            num_map_remove++;
#endif
        }

        return ev;
    }

    // In this case, heap is empty
    if (pos != limit<uint64_t>()) {
        ev.impl = buckets[pos].front();
        ev.key = ev.impl->key;
        buckets[pos].remove(ev.impl);
        if (buckets[pos].empty()) {
            bitmap[tmpFront].clr(pos);
        }

#ifdef NS3_BUILD_PROFILE_DEBUG
        num_bucket_remove++;
#endif
        NS_LOG_INFO("RemoveCore: pos=" << pos << " tmpFront=" << tmpFront << " front=" << front);
        return ev;
    }

    // bucket is empty
    ev.impl = it->second;
    ev.key = it->first;
    mapList.erase (it);
#ifdef NS3_BUILD_PROFILE_DEBUG
    num_map_remove++;
#endif

    return ev;
}

void
BucketScheduler::Remove (const Event &ev)
{
    NS_ASSERT_MSG(false, "NYI");
}

} // namespace ns3
