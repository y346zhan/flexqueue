/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

#ifndef BUCKET_SCHEDULER_H
#define BUCKET_SCHEDULER_H

#include "scheduler.h"
#include "simulator.h"
#include "assert.h"
#include "Bitmap.h"
#include <map>
#include <stdint.h>
#include <utility>

#ifdef NS3_BUILD_PROFILE_DEBUG
#include <iostream>
#endif

#ifndef BASE_WIDTH
#define BASE_WIDTH 64
#endif

/**
 * \file
 * \ingroup scheduler
 * Declaration of ns3::BucketScheduler class.
 */

namespace ns3 {

class BucketScheduler : public Scheduler
{
public:
  /**
   *  Register this type.
   *  \return The object TypeId.
   */
  static TypeId GetTypeId (void);

  /** Constructor. */
  BucketScheduler ();
  /** Destructor. */
  virtual ~BucketScheduler ();

  // Inherited
  virtual void Insert (const Scheduler::Event &ev);
  virtual bool IsEmpty (void) const;
  virtual Scheduler::Event PeekNext (void) const;
  virtual Scheduler::Event RemoveNext (void);
  virtual void Remove (const Scheduler::Event &ev);

private:

  struct SimpleBucket {
      EventImpl *_front = nullptr;
      // not directly called in benchmark
      inline void remove (EventImpl *e) {
          if (e->prev != NULL) {
              e->prev->next = e->next;
          } else {
              _front = e->next;
          }

          if (e->next != NULL) {
              e->next->prev = e->prev;
          }
          e->prev = NULL;
          e->next = NULL;
#ifdef NS3_BUILD_PROFILE_DEBUG
          if (empty()) { std::cerr << "\t Bucket now empty!" << std::endl; }
#endif
      }

      inline void removeFront() {
#ifdef NS3_BUILD_PROFILE_DEBUG
          if (empty()) { std::cerr << "ERROR: Calling min on empty bucket!" << std::endl; }
#endif
          remove(_front);
#ifdef NS3_BUILD_PROFILE_DEBUG
          if (empty()) { std::cerr << "\t Bucket now empty!" << std::endl; }
#endif
      }

      inline void insertFront (EventImpl *e) {
          if (_front != nullptr) {
              e->next = _front;
              e->prev = nullptr;
              _front->prev = e;
          }
          _front = e;
      }

      inline bool empty () const {
          return _front == nullptr;
      }

      inline EventImpl *min () const {
#ifdef NS3_BUILD_PROFILE_DEBUG
          if (empty()) {
              std::cerr << "ERROR: Calling min on empty bucket!" << std::endl;
          }
#endif
          EventImpl *m = _front, *n = m->next;
          while (n != nullptr) {
              if (n->key < m->key) {
                  m = n;
              }
              n = n->next;
          }
          return m;
      }

      inline EventImpl *front() const {
          return _front;
      }
  };

  Scheduler::Event  RemoveNextCore (void);
  bool              bmEmpty (void) const;

  typedef HierarchicalBitmap<BASE_WIDTH,16> bitmap_t;
  typedef std::map<Scheduler::EventKey, EventImpl*> EventMap;
  typedef std::map<Scheduler::EventKey, EventImpl*>::iterator EventMapI;
  typedef std::map<Scheduler::EventKey, EventImpl*>::const_iterator EventMapCI;

  static const uint64_t group_size = 16;
  static const uint64_t num_buckets = 1 << group_size;
  static const uint64_t LOW16 = 0xffff;     // = 2^18 * 8 bytes

  // The bucket queue
  SimpleBucket  buckets[num_buckets];
  bitmap_t  bitmap[2];
  void *    storage[2];
  int       front = 0;

  // The STL map queue
  EventMap  mapList;

#ifdef NS3_BUILD_PROFILE_DEBUG
  // Golden list for debugging
  EventMap goldList;

  /* Stats*/
  uint64_t num_map_insert = 0;
  uint64_t num_bucket_insert = 0;

  uint64_t num_map_remove = 0;
  uint64_t num_bucket_remove = 0;

  uint64_t num_item = 0;
  uint64_t peak_item = 0;
  /**/
#endif

};

} // namespace ns3

#endif /* MAP_SCHEDULER_H */
