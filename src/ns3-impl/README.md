# How to use this scheduler:
1. Copy all source and header files from this directory to src/core/model. In 
   addition, the following files are required and can be found in ../include:
   * Bitmap.h
   * basics.h
   * bitmanip.h
   * kostypes.h
2. Apply patch. From root of NS3 repository, run:
```
patch -p1 <ns3.patch
```

# A few useful commands
To configure ns3 with optimized profiled, static linkage, and output to a 
particular directory:
```
./waf configure -d debug --enable-examples --enable-static --out=build/hybrid
```

To build just a single target:
```
./waf build  -v --targets=<target name>
```
Note the above won't actually process the required headers. Run `./waf` first 
and stop when it starts compiling.
