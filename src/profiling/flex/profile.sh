#!/bin/zsh
rm -f flex.prof map.prof hybrid.prof
rm -f gmon.out
rm -f flex map hybrid

make -sC ~/thesis/code TEST_PARAMS="-DTEST_NAME=FlexHoldTestCase -DHORIZON=30 -DQUEUE_SIZE_LOG=20 -DBUCKET_PERCENT=100 -DBASE_WIDTH=64" clean runTest
mv ~/thesis/code/runTest ./flex

make -sC ~/thesis/code TEST_PARAMS="-DTEST_NAME=MapHoldTestCase -DHORIZON=30 -DQUEUE_SIZE_LOG=20 -DBUCKET_PERCENT=100 -DBASE_WIDTH=64" clean runTest
mv ~/thesis/code/runTest ./map

make -sC ~/thesis/code TEST_PARAMS="-DTEST_NAME=HybridHoldTestCase -DHORIZON=30 -DQUEUE_SIZE_LOG=20 -DBUCKET_PERCENT=100 -DBASE_WIDTH=64" clean runTest
mv ~/thesis/code/runTest ./hybrid

./flex
gprof flex > flex.prof
#
./map
gprof map > map.prof
#
./hybrid
gprof hybrid > hybrid.prof
#
rm gmon.out

