#ifndef _Bitmap_C_h_
#define _Bitmap_C_h_ 1

#include "kostypes.h"
#include <stdbool.h>

#define divup(n,d) ((n-1)/(d)+1)
#define fastpath(x)  (__builtin_expect(((bool)(x)),true))
#define slowpath(x)  (__builtin_expect(((bool)(x)),false))
#define align_down(x, a) ((x) & (~((a)-1)))

#define bitmask(w) ( (1 << (w)) - 1 )

// static inline constexpr T bitmask(unsigned int Width) {
//   return Width == bitsize<T>() ? limit<T>() : pow2<T>(Width) - 1;
// }

#define BASE_BITMAP_WIDTH 512
#define BM_N (divup(BASE_BITMAP_WIDTH, sizeof(mword) * 8))

#ifdef __cplusplus
extern "C" {
#endif

// extern const size_t BM_N;

typedef struct {
    mword bits [ BM_N ];
} Bitmap;

mword multiscan(int findset, const mword* data, mword idx);

inline void bitset(mword *a, mword idx) {
    *a |= (mword)1 << idx;
}

inline void bitclr(mword *a, mword idx) {
    *a &= ~((mword)1 << idx);
}

inline void bm_set(Bitmap *self, mword idx) {
    bitset(&self->bits[idx / (sizeof(mword) * 8)], idx % (sizeof(mword) * 8));
}

inline void bm_clr(Bitmap *self, mword idx) {
    bitclr(&self->bits[idx / (sizeof(mword) * 8)], idx % (sizeof(mword) * 8));
}

inline mword bm_find_2(const Bitmap *self, bool findset, mword idx) {
    return multiscan(findset, self->bits, idx);
}

inline mword bm_find(const Bitmap *self) {
    return bm_find_2(self, true, 0);
}

inline bool bm_test(const Bitmap *self, mword idx) {
        return (self->bits[idx / (sizeof(mword) * 8)]
            & ((mword)1 << (idx % (sizeof(mword) * 8)))) != 0;
}

inline mword bm_empty_1 (const Bitmap *self, int idx) {
    return self->bits[idx] | bm_empty_1(self, idx-1);
}

inline bool bm_empty(const Bitmap *self) {
    mword map = 0;
    for (size_t i=0; i < BM_N; i++) {
        map |= self->bits[i];
    }
    return map == (mword)0;
}

inline mword multiscan(int findset, const mword* data, mword idx) {
    mword result = 0;
    mword mask = ~((mword)0);
    mword newmask = mask;

    size_t i = (idx / (sizeof(mword) * 8));
    if (idx) {  // if not searching from the end:
        result = align_down(idx, (mword)(sizeof(mword)*8));
        mword scan;
        // mword datafield = (findset ? data[i] : ~data[i]) & ~bitmask<mword>(idx % bitsize<mword>());
        mword datafield = (findset ? data[i] : ~data[i]) & ~(bitmask(idx % (sizeof(mword)*8)));
        asm volatile("\
            bsfq %2, %0\n\t\
            cmovzq %3, %0\n\t\
            cmovnzq %4, %1"
        : "=&r"(scan), "+r"(newmask)
        : "rm"(datafield), "r"(sizeof(mword)*8), "r"((mword)0)
        : "cc");
        result += scan & mask;
        mask = newmask;
        i += 1;
    }

    for (; i < BM_N; i++) {
        mword scan;
        mword datafield = (findset ? data[i] : ~data[i]);
        asm volatile("\
            bsfq %2, %0\n\t\
            cmovzq %3, %0\n\t\
            cmovnzq %4, %1"
        : "=&r"(scan), "+r"(newmask)
        : "rm"(datafield), "r"(sizeof(mword)*8), "r"((mword)0)
        : "cc");
        result += scan & mask;
        mask = newmask;
    }

  return result;
}


#ifdef __cplusplus
}
#endif

#endif
