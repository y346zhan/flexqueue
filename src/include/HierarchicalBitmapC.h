#ifndef _HierarchicalBitmap_C_h_
#define _HierarchicalBitmap_C_h_ 1

#include "BitmapC.h"
#include <stdbool.h>

#define HB_LOG_MAX_WIDTH 40 // self is NOT the desired size of bitmap, but the largest possible
#define HB_LOG_CACHE_LINE 9 // assert 2^HB_LOG_CACHE_LINE == BITMAP_WIDTH

#ifdef __cplusplus
extern "C" {
#endif

#define HB_LEVELS (divup(HB_LOG_MAX_WIDTH, HB_LOG_CACHE_LINE))

struct HierarchicalBitmap {
    Bitmap *bitmaps [ HB_LEVELS ];
};

size_t hm_find_2 (const struct HierarchicalBitmap *self, size_t idx, bool findset);
size_t hm_find_4 (const struct HierarchicalBitmap *self, bool findset, size_t sidx, size_t ridx, size_t l);

inline size_t  hm_allocsize    (size_t bitcount) { // this is the desired size of bitmap
    size_t mapcount = 0;
    for (size_t l = 0; l < HB_LEVELS; l += 1) {
        bitcount = divup(bitcount, BASE_BITMAP_WIDTH);
        mapcount += bitcount;
    }
    return sizeof(Bitmap) * mapcount;
}

inline void hm_init (struct HierarchicalBitmap *self, size_t bitcount, bufptr_t p) {
    for (size_t l = 0; l < HB_LEVELS; l += 1) {
        bitcount = divup(bitcount, BASE_BITMAP_WIDTH);
        self->bitmaps[l] = (Bitmap *)p;
        p += sizeof(Bitmap) * bitcount;
    }
    //printf("DEBUG: Levels: %d\n", (int)HB_LEVELS);
}

inline void hm_set  (struct HierarchicalBitmap *self, size_t idx) {
    for (size_t l = 0; l < HB_LEVELS; l += 1) {
        size_t r = idx % BASE_BITMAP_WIDTH;
        idx = idx / BASE_BITMAP_WIDTH;
        if slowpath(bm_test(&self->bitmaps[l][idx], r)) return;
        bm_set(&self->bitmaps[l][idx], r);
    }
}

inline void hm_clr  (struct HierarchicalBitmap *self, size_t idx) {
    for (size_t l = 0; l < HB_LEVELS; l += 1) {
        size_t r = idx % BASE_BITMAP_WIDTH;
        idx = idx / BASE_BITMAP_WIDTH;
        bm_clr(&self->bitmaps[l][idx], r);
        if slowpath(!bm_empty(&self->bitmaps[l][idx])) return;
    }
}

inline size_t hm_find (const struct HierarchicalBitmap *self) {
    return hm_find_4(self, true, 0, 0, HB_LEVELS - 1);
}

inline bool hm_test (const struct HierarchicalBitmap *self, size_t idx) {
    return bm_test(&self->bitmaps[0][idx / BASE_BITMAP_WIDTH], idx % BASE_BITMAP_WIDTH);
}

// Helpers

inline size_t hm_find_2 (const struct HierarchicalBitmap *self, size_t idx, bool findset) {
    return hm_find_4(self, findset, (idx+1) / BASE_BITMAP_WIDTH, (idx+1) % BASE_BITMAP_WIDTH, 0);
}

inline size_t hm_find_4 (const struct HierarchicalBitmap *self, bool findset, size_t sidx, size_t ridx, size_t l) {
    if (l == HB_LEVELS) return ~((size_t)0);

    size_t fidx = bm_find_2(&self->bitmaps[l][sidx], findset, ridx);

    if slowpath(fidx == BASE_BITMAP_WIDTH)
        return hm_find_4(self, findset, ((sidx+1) / BASE_BITMAP_WIDTH), (sidx+1) % BASE_BITMAP_WIDTH, l+1);
    else if slowpath(l == 0)
        return sidx * BASE_BITMAP_WIDTH + fidx;
    else
        return hm_find_4(self, findset, sidx * BASE_BITMAP_WIDTH + fidx, 0, l-1);
}

#ifdef __cplusplus
}
#endif


#endif
