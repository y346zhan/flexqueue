/******************************************************************************
    Copyright � 2012-2015 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _Bitmap_h_
#define _Bitmap_h_ 1

#include "bitmanip.h"

template<size_t X> constexpr mword BitmapEmptyHelper(const mword* bits) {
  return bits[X] | BitmapEmptyHelper<X-1>(bits);
}

template<> constexpr mword BitmapEmptyHelper<0>(const mword* bits) {
  return bits[0];
}

template<size_t X> constexpr mword BitmapFullHelper(const mword* bits) {
  return bits[X] & BitmapFullHelper<X-1>(bits);
}

template<> constexpr mword BitmapFullHelper<0>(const mword* bits) {
  return bits[0];
}

template<size_t X> constexpr mword BitmapCountHelper(const mword* bits) {
  return popcount(bits[X]) + BitmapCountHelper<X-1>(bits);
}

template<> constexpr mword BitmapCountHelper<0>(const mword* bits) {
  return popcount(bits[0]);
}

template<size_t B = bitsize<mword>()>
class Bitmap {
  static const int N = divup(B,bitsize<mword>());
  mword bits[N];
public:
  explicit Bitmap() {}
  explicit Bitmap( mword b ) { for (size_t i = 0; i < N; i += 1) bits[i] = b; }
  static constexpr bool valid( mword idx ) { return idx < N * bitsize<mword>(); }
  static constexpr Bitmap filled() { return Bitmap(~mword(0)); }
  void setB() { for (size_t i = 0; i < N; i++) bits[i] = ~mword(0); }
  void clrB() { for (size_t i = 0; i < N; i++) bits[i] =  mword(0); }
  void flpB() { for (size_t i = 0; i < N; i++) bits[i] = ~bits[i]; }
  template<bool atomic=false> void set  ( mword idx ) {
    bit_set<atomic>(bits[idx / bitsize<mword>()], idx % bitsize<mword>());
  }
  template<bool atomic=false> void clr( mword idx ) {
    bit_clr<atomic>(bits[idx / bitsize<mword>()], idx % bitsize<mword>());
  }
  template<bool atomic=false> void flp ( mword idx ) {
    bit_flp<atomic>(bits[idx / bitsize<mword>()], idx % bitsize<mword>());
  }
  constexpr bool test( mword idx ) const {
    return bits[idx / bitsize<mword>()] & (mword(1) << (idx % bitsize<mword>()));
  }
  constexpr bool empty()  const { return BitmapEmptyHelper<N-1>(bits) == mword(0); }
  constexpr bool full()   const { return  BitmapFullHelper<N-1>(bits) == ~mword(0); }
  constexpr mword count() const { return BitmapCountHelper<N-1>(bits); }
  constexpr mword find(bool findset = true) const {
    return multiscan_simple<N>(bits, findset);
  }
  constexpr mword findnext(mword idx, bool findset = true) const {
    return multiscan_next<N>(bits, idx, findset);
  }
  constexpr mword findrev(bool findset = true) const {
    return multiscan_rev<N>(bits, findset);
  }
};

// B: number of bits in elementary bitmap (set to cacheline size)
// W: log2 of maximum width of bitmap
template<size_t B, size_t W>
class HierarchicalBitmap {
  static const size_t logB = floorlog2(B);
  static const size_t Levels = divup(W,logB);

  static_assert( B == pow2<size_t>(logB), "template parameter B not a power of 2" );
  static_assert( B >= bitsize<mword>(), "template parameter B less than word size" );
  static_assert( W >= logB, "template parameter W smaller than log B" );
  static_assert( W <= 100, "template parameter W really big, is this what you want?" );

  Bitmap<B>* bitmaps[Levels];
  size_t maxBucketCount;

public:
  static size_t memsize( size_t bitcount ) {
    size_t mapcount = 0;
    for (size_t l = 0; l < Levels; l += 1) {
      bitcount = divup(bitcount,B);
      mapcount += bitcount;
    }
    //GENASSERT(bitcount == 1);
    return sizeof(Bitmap<B>) * mapcount;
  }

  void init( size_t bitcount, bufptr_t p ) {
    maxBucketCount = divup(bitcount,B);
    for (size_t l = 0; l < Levels; l += 1) {
      bitcount = divup(bitcount,B);
      bitmaps[l] = new (p) Bitmap<B>[bitcount];
      p += sizeof(Bitmap<B>) * bitcount;
    }
    //GENASSERT(bitcount == 1);
  }

  bool set( size_t idx, size_t botlevel = 0 ) {
    bool didSet = false;
    for (size_t l = botlevel; l < Levels; l += 1) {
      size_t r = idx % B;
      idx = idx / B;
      if slowpath(bitmaps[l][idx].test(r)) break;
      bitmaps[l][idx].set(r);
      didSet = true;
    }
    return didSet;
    //GENASSERT(idx == 0);
  }

  void clr( size_t idx, size_t botlevel = 0 ) {
    for (size_t l = botlevel; l < Levels; l += 1) {
      size_t r = idx % B;
      idx = idx / B;
      bitmaps[l][idx].clr(r);
      if slowpath(!bitmaps[l][idx].empty()) return;
    }
    //GENASSERT(idx == 0);
  }

  void blockset( size_t idx ) {
    bitmaps[0][idx / B].setB();
    set(idx / B, 1);
  }

  void blockclr( size_t idx ) {
    bitmaps[0][idx / B].clrB();
    clr(idx / B, 1);
  }

  constexpr bool test( size_t idx ) const {
    return bitmaps[0][idx / B].test(idx % B);
  }

  constexpr bool empty() const {
    return bitmaps[Levels-1][0].empty();
  }

  constexpr bool blockempty( size_t idx ) const {
    return bitmaps[0][idx / B].empty();
  }

  constexpr bool blockfull( size_t idx ) const {
    return bitmaps[0][idx / B].full();
  }

  size_t find(bool findset = true) const {
    size_t idx = bitmaps[Levels - 1][0].find(findset);
    if (idx == B) return limit<size_t>();

    // now guaranteed to find a bit
    for (int i = (int)Levels - 2; i >= 0; i--) {
      size_t ldx = bitmaps[i][idx].find(findset);
      idx = idx * B + ldx;
    }
    return idx;
  }

  size_t findnext(size_t idx, bool findset = true) const {
    size_t bidx = idx / B;
    size_t ridx = idx % B;
    size_t mbc = maxBucketCount;
    size_t l = 0;
    for (;;) {
      size_t lidx = bitmaps[l][bidx].findnext(ridx, findset);
      if slowpath(lidx == B) {
        if slowpath(bidx + 1 >= mbc) return limit<size_t>();
        mbc = mbc / B;
        bidx = (bidx + 1) / B;
        ridx = (bidx + 1) % B;
        l = l + 1;
      } else {
        bidx = bidx * B + lidx;
        if slowpath(l == 0) return bidx;
        ridx = 0;
        l = l - 1;
      }
    }
  }

  size_t findrange(size_t& start, size_t max, bool findset = true) const {
    start = findnext(start, findset);
    if (start == limit<size_t>()) return 0;
    size_t end = findnext(start, !findset);
    if (end == limit<size_t>()) end = max;
    return end - start;
  }

  size_t findrev(bool findset = true) const {
    size_t idx = 0;
    for (size_t i = Levels - 1;; i -= 1) {
      size_t ldx = bitmaps[i][idx].findrev(findset);
      if slowpath(ldx == B) return limit<size_t>();
      idx = idx * B + ldx;
      if slowpath(i == 0) return idx;
    }
  }
};

#endif /* _Bitmap_h_ */
