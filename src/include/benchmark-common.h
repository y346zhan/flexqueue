/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

#ifndef BENCHMARK_COMMON_H
#define BENCHMARK_COMMON_H

#include "Bitmap.h"
#include <map>
#include <random>
#include <stdint.h>
#include <utility>
#include <iostream>
#include <algorithm>
#include <boost/intrusive/set.hpp>

// For convenience
#ifdef L_TRACE
#define L_DEBUG
#define TRACE(msg) std::cerr << GetCurrentTime() <<  " TRACE " << msg << std::endl;
#else
#define TRACE(msg)
#endif

#ifdef L_DEBUG
#define DEBUG(msg) std::cerr << "DEBUG " << msg << std::endl;
#else
#define DEBUG(msg)
#endif

#ifndef L_SILENT
#define INFO(msg) std::cerr << "INFO " << msg << std::endl;
#else
#define INFO(msg)
#endif
#define LOG_NO_ENDL(msg) std::cerr << msg;

// Will expand to nothing
#define NS_LOG_INFO(msg)
#define NS_LOG_ERROR(msg)
#define NS_LOG_DEBUG(msg)
#define NS_ASSERT_MSG(cond, msg)
#define NS_ASSERT(cond)
#define LogComponentEnable(name, level)
#define NS_LOG_FUNCTION(obj)

#define XSTR(s) STR(s)
#define STR(s) #s

struct EventKey {
  uint64_t m_ts;
  uint32_t m_uid;
  EventKey() : m_ts(0), m_uid(0) {}
};

struct EventImpl {
  void *data; // application specific data
  EventImpl() : data(nullptr) {}
};

struct Event {//: public boost::intrusive::set_base_hook<> {
  //EventImpl *impl;
  EventKey key;
  Event(uint64_t ts = 0, uint64_t uid = 0) {
    key.m_ts = ts;
    key.m_uid = uid;
  }
};

inline bool operator< (const EventKey &l, const EventKey &r) {
  return l.m_ts < r.m_ts || (l.m_ts == r.m_ts && l.m_uid < r.m_uid);
}

inline bool operator> (const EventKey &l, const EventKey &r) {
  return l.m_ts > r.m_ts || (l.m_ts == r.m_ts && l.m_uid > r.m_uid);
}

inline bool operator< (const Event &l, const Event &r) {
  return l.key < r.key;
}

inline bool operator> (const Event &l, const Event &r) {
  return l.key > r.key;
}

inline bool operator== (const Event &l, const Event &r) {
  return l.key.m_uid == r.key.m_uid
    && l.key.m_ts == r.key.m_ts;
}

inline bool operator!= (const Event &l, const Event &r) {
  return !(l == r);
}

#ifndef BUCKET_PERCENT
#define BUCKET_PERCENT 100
#endif

#ifndef QUEUE_SIZE
#define QUEUE_SIZE (1ull << 10)
#endif

#ifndef BASE_WIDTH
#define BASE_WIDTH 64
#endif

#ifndef HORIZON
#define HORIZON (1ull << 20)
#endif

#ifndef MEAN
#define MEAN 10000000
#endif

#ifndef STDDEV
#define STDDEV 300000
#endif

#ifndef TRI_A
#define TRI_A 1000000
#endif

#ifndef TRI_B
#define TRI_B 100000000
#endif

#ifndef DISTRIBUTION
#define DISTRIBUTION normal
#endif

#ifndef BIT_DURATION
#define BIT_DURATION 1
#endif

#ifndef BIT_COUNT
#define BIT_COUNT (1ull << 20)
#endif

#ifndef X_VAR
#define X_VAR_STRING ""
#else
#define X_VAR_STRING XSTR(X_VAR\t) // \t only if some input is provided...
#endif

#ifndef X_LABEL
#define X_LABEL_STRING ""
#else
#define X_LABEL_STRING XSTR(X_LABEL\t) // ...same here
#endif

#ifndef SPAN
#define SPAN 2.5f
#endif

// Maybe not the best way to organize configs, but works OK for now
// Test Parameters
namespace TP {
  constexpr size_t queueSize = QUEUE_SIZE;
  constexpr size_t baseWidth = BASE_WIDTH;
  constexpr size_t bitCount = BIT_COUNT;

  // params for normal distribution
  namespace normal {
    constexpr size_t mean = MEAN;
    constexpr size_t stddev = STDDEV;
  }
  // configs for uniform distribution
  namespace uniform {
    constexpr size_t horizon = HORIZON;
  }
  // configs for piecewise distribution
  namespace piecewise {
    constexpr size_t percent = BUCKET_PERCENT;
    constexpr size_t horizon = HORIZON;
  }
  // configs for triangular distribution
  namespace triangular {
    constexpr size_t a = TRI_A;
    constexpr size_t b = TRI_B;
  }
  // configs for flex queue
  namespace flexQueue {
    constexpr size_t tpbInit = BIT_DURATION;
    constexpr size_t bitCount = BIT_COUNT;

    constexpr size_t alphaN = 7;  // 7/8 is a typical value used by TCP for RTT estimation
    constexpr size_t alphaDe = 3;
    constexpr size_t alphaD = 1<<alphaDe;

    constexpr size_t betaN = 3;   // 1/4 is for RTT stddev estimation
    constexpr size_t betaDe = 2;
    constexpr size_t betaD = 1<<betaDe;

    constexpr double k = SPAN;
  }
  namespace bitmapQueue {
    constexpr size_t bitCount = BIT_COUNT;
  }
}

struct TrialRun {
  std::vector<size_t> sample;
  double avg = 0;

  void addResult(size_t ele) {
    sample.push_back(ele);
  }

  double mean() {
    double sum = std::accumulate(sample.begin(), sample.end(), 0.0);
    return sum / sample.size();
  }

  double stddev() {
    double sum = std::accumulate(sample.begin(), sample.end(), 0.0);
    double mean = sum / sample.size();

    std::vector<double> diff(sample.size());
    std::transform(sample.begin(), sample.end(), diff.begin(), [mean](double x) {
        return x - mean;
        });
    double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
    return std::sqrt(sq_sum / sample.size());
  }

};

struct QueueI {
  virtual ~QueueI() = 0;
  virtual void Insert (Event &ev) = 0;
  virtual bool IsEmpty () const = 0;
  virtual Event RemoveNext () = 0;
  virtual Event Peek () const = 0;
  virtual size_t Size () const = 0;
};

inline QueueI::~QueueI() {}

size_t GetCurrentTime();
void SetCurrentTime(size_t);

#endif /* MAP_SCHEDULER_H */
