#ifndef _PQueue_h_
#define _PQueue_h_

#include "Bitmap.h"
#include <unordered_map>
#include <list>
#include <iostream>

/*
   parameters to consider:
   1. number of priorities (number of bits)
   2. capacity of each priority
   3. when to alloc storage for each priority
*/

#define GROUP0 0xff000000
#define GROUP1 0x00ff0000
#define GROUP2 0x0000ff00
#define GROUP3 0x000000ff

#define GROUP_SIZE 8

static const size_t num_buckets = 4*(1<<8);

struct PQNode {
    PQNode *prev;
    PQNode *next;
    void *data;

    PQNode (void *d) {
        prev = NULL;
        next = NULL;
        data = d;
    }
};

class PQBucket {
    PQNode *front;

    public:
    PQBucket () {front = NULL;}
    ~PQBucket ()  {
        /*
        while (front != NULL) {
            PQNode *trash = front;
            front = front->next;
            delete trash;
        }*/
    }

    PQNode * insert (void *d) {
        PQNode *n = new PQNode(d);
        if (front != NULL) {
            n->next = front;
            n->prev = NULL;
            front->prev = n;
        }
        front = n;
        return n;
    }

    void * removeHead() {
        if (empty()) return NULL;

        void *data = front->data;
        front = front->next;
        if (front != NULL) {
            delete front->prev;
            front->prev = NULL;
        }
        return data;
    }

    void remove (PQNode *n) {
        // assume n exists in the list
        if (n->prev != NULL) {
            n->prev->next = n->next;
        } else {
            front = n->next;
        }

        if (n->next != NULL) {
            n->next->prev = n->prev;
        }
        delete n;
    }

    inline bool empty() const {
        return front == NULL;
    }
};

class PQueue {
    size_t counter;
    HierarchicalBitmap<512, 40> bitmap;
    bufptr_t str;

    PQBucket buckets[num_buckets];

    // a timestamp of 0 will also be put in bucket 0
    inline int calcPos(size_t k) {
        if (k == 0) return 0;
        int ffs = sizeof(size_t) * 8 - __builtin_clzl(k) - 1;
        // tag number
        int ignore = ffs / GROUP_SIZE * GROUP_SIZE;
        int tag = k >> ignore;
        int position = tag + ffs / GROUP_SIZE * (1 << GROUP_SIZE);
        return position;
    }

    public:
    PQueue() : counter(0), buckets() {
        size_t size = bitmap.allocsize(num_buckets);
        str = new buf_t[size]();
        bitmap.init(num_buckets, str);
    }

    ~PQueue() { delete str; }


    // adds an item with timestamp k, and data v
    void enqueue(size_t k, void * v) {
        int pos = calcPos(k);
        bitmap.set(pos);
        buckets[pos].insert(v);
    }

    // removes the next highest priority item
    // returns the data associated with it
    void * dequeue() {
        size_t pos = bitmap.find();
        if (pos >= num_buckets) return NULL;
        void * d = buckets[pos].removeHead();
        if (buckets[pos].empty()) bitmap.clr(pos);
        return d;
    }

    bool empty() const {
        return bitmap.empty();
    }
};


#endif

