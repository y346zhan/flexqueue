#ifndef _HASHARRAY_H_
#define _HASHARRAY_H_

#include <vector>
#include <unordered_map>
#include <cstdlib>
//#include <exception>
//#include <stdexcept>
#include <cassert>

// No dups allowed
template<class T>
class HashArray {
    public:
    std::vector<T> v;
    std::unordered_map<T, size_t> t;

    HashArray(size_t count) {
        reserve(count);
    }

    void reserve(size_t count) {
        v.reserve(count);
        t.reserve(count);
    }

    void insert(const T& value) {
        assert(v.size() == t.size());
        //if (t.find(value) != t.end()) throw std::invalid_argument("duplicate insert");
        t.insert({value, v.size()});
        v.push_back(value);
        assert(v.size() == t.size());
    }

    void remove(const T& value) {
        assert(v.size() == t.size());
        size_t i = t.at(value);
        if (i != v.size() - 1) {
            v[i] = v[v.size() - 1];
            v.erase(v.begin() + v.size() - 1);
            t.erase(value);
            t[v[i]] = i;
        } else {
            v.erase(v.begin() + v.size() - 1);
            t.erase(value);
        }
        assert(v.size() == t.size());
    }

    T removeRandom() {
        T tmp = v[rand() % v.size()];
        remove(tmp);
        return tmp;
    }

    size_t get(const T& value) {
        return t.at(value);
    }
};

#endif
