#ifndef WRAPPER_H
#define WRAPPER_H

#include "kostypes.h"
#include <stdbool.h>

#ifdef _c_impl_
    struct HierarchicalBitmap;
    typedef struct HierarchicalBitmap HMap;
#else
    template<size_t B, size_t W> class HierarchicalBitmap;
    typedef HierarchicalBitmap<512,40> HMap;
#endif

#ifdef __cplusplus
extern "C" {
#endif

void hmap_alloc(HMap **self);

void hmap_dealloc(HMap **self);

size_t hmap_allocsize(HMap *self, size_t bitcount);

void hmap_init( HMap *self, size_t bitcount, bufptr_t p );

void hmap_set( HMap *self, size_t idx);

void hmap_clr( HMap *self, size_t idx);

bool hmap_test( HMap *self, size_t idx );

mword hmap_find( HMap *self );

#ifdef __cplusplus
}
#endif

#endif