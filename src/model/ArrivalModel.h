#ifndef _ArrivalModel_h_
#define _ArrivalModel_h_
#include <random>
#include "benchmark-common.h"

class ArrivalModel {
public:
  virtual size_t next() = 0;
  virtual ~ArrivalModel() = 0;
};

inline ArrivalModel::~ArrivalModel () {}

template<typename T> ArrivalModel * createArrivalModel(size_t seed) { return new T(seed); }

/**
 * Uniform distribution [0, horizon) with probability p,
 * and in [horizon, inf) with probabilty 1-p.
 * Not particularly useful in practice.
 */
template <size_t p> class PiecewiseArrival : public ArrivalModel {
  std::mt19937_64 _rng;
  const std::vector<size_t> _endpoints {size_t(0), TP::piecewise::horizon, 1ull << 32};
  const std::vector<int>    _weights {p, 100 - p};
  std::piecewise_constant_distribution<>  _pcd;

public:
  PiecewiseArrival (size_t seed) :
    _rng(seed),
    _pcd(_endpoints.begin(), _endpoints.end(), _weights.begin())
    {
      DEBUG("Piecewise distribution using pcd"
          << ", p=" << p
          << ", endpoints=" << _endpoints[0] << ", " << _endpoints[1] << ", " << _endpoints[2]
          );
    }

  virtual size_t next() {
    return _pcd(_rng);
  }

  ~PiecewiseArrival() { }
};

/**
 * [0, horizon)
 */
class UniformArrival : public ArrivalModel {
  std::mt19937_64 _rng;
  const size_t _h = (TP::uniform::horizon);

public:
  UniformArrival (size_t seed) : _rng(seed) {
    DEBUG("Uniform distribution"
        << ", h=" << _h
        );
  }

  virtual size_t next() {
    return _rng() % _h; // OK because _h is a power of 2
  }

  ~UniformArrival() { }
};

/**
 * Another piecewise distribution but without STL
 * This also modifies the max event horizon so that H - h is also a power of 2,
 * so that % can be used efficiently without relying on probabilistic methods.
 */
template <size_t p> class PiecewiseSimpleArrival : public ArrivalModel {
  std::mt19937_64 _rng;
  constexpr static size_t _h = (TP::piecewise::horizon);
  constexpr static size_t _H = _h + (TP::piecewise::horizon * 3);

public:
  PiecewiseSimpleArrival (size_t seed) :
    _rng(seed)
  {
    DEBUG("Simplified piecewise distribution"
        << ", p=" << p
        << ", endpoints=" << 0 << ", " << _h << ", " << _H
        );
  }

  virtual size_t next() {
    size_t r1 = _rng();
    size_t r2 = _rng();
    if (_rng() % 100 >= p) { // p == 0 will be faster than other values because p is compile time constant
      return (r2 % (_H - _h)) + _h; //  OK because _H - _h is also a power of 2
    }
    return r1 % _h; // OK because _h divides 2^64
  }

  ~PiecewiseSimpleArrival() { }
};

/**
 * A truncated normal distribution, that is bounded below
 */
template <size_t u, size_t stddev> class NormalArrival : public ArrivalModel {
  std::mt19937_64 _rng;
  std::normal_distribution<> _d;

public:
  NormalArrival (size_t seed) : _rng(seed), _d(u, stddev) {
    DEBUG("Normal distribution" << ", u=" << u << ", stddev=" << stddev);
  }

  virtual size_t next() {
    long long r = std::round(_d(_rng));
    if (r < 0) return 0;
    return r;
  }

  ~NormalArrival() { }
};

template<size_t a, size_t b> class TriangularArrival : public ArrivalModel {
  std::mt19937_64 _rng;
  std::uniform_real_distribution<> _d;

  public:
  TriangularArrival (size_t seed) : _rng(seed), _d(0, 1) {
    DEBUG("Triangular distribution");
  }

  virtual size_t next() {
    return a + (b-a) * sqrt(_d(_rng));
  }

  ~TriangularArrival() { }
};

/**
 * Poisson distribution that produces non-negative integers.
 */
template <size_t u> class PoissonArrival : public ArrivalModel {
  std::mt19937_64 _rng;
  std::poisson_distribution<size_t> _d;

public:
  PoissonArrival (size_t seed) : _rng(seed), _d(u) {
    DEBUG("Poisson distribution" << ", u=" << u);
  }

  virtual size_t next() {
    long long r = std::round(_d(_rng));
    if (r < 0) return 0;
    return r;
  }

  ~PoissonArrival() { }
};

/**
 * A mix that alternates between two distributions
 */
template<typename D1, typename D2> class MixArrival : public ArrivalModel {
  std::mt19937_64 _rng;
  D1 _d1;
  D2 _d2;
public:
  MixArrival (size_t seed) : _rng(seed), _d1(seed), _d2(seed) {
    DEBUG("Mix distribution");
  }

  virtual size_t next() {
    if (_rng() % 2 == 0) {
      return _d1.next();
    }
    return _d2.next();
  }

  ~MixArrival() { }
};


#endif
