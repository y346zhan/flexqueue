% !TEX root = main.tex
% !TEX encoding = UTF-8 Unicode

\chapter{Design of FlexQueue}

\label{chapter:design}

The original calendar queue as proposed by Brown consists of an $N$-component
array $A$, storing pointers to $N$ sorted linked lists. Each element in $A$
represents some amount of time units $u$ such that an event with a timestamp of
$t$ belongs to a list pointed to by $A[i]$, if $iu \leq t < (i+1)u$. That is,
the bucket to which $t$ belongs can be computed as $\lfloor \frac{t}{u}
\rfloor$. The horizon of this calendar queue is therefore $h=uN$ time units, and
any event that is more than $h$ time units away is stored in the same list as if
its timestamp is the remainder of $\frac{t}{h}$.  Figure \ref{diag:simple_cq}
illustrates the structure of a calendar queue with five events, and $u=5, N=4$.
Here, event 30 is considered a distant event because $30 > 5 \cdot 4$, and it is
placed in the same bucket as if its timestamp is $30 - 5 \cdot 4 = 10$.

\begin{figure}
  \centering
  \includegraphics[scale=0.7]{diagrams/simple_cq}
    \caption{A simple calendar queue with five events}
  \label{diag:simple_cq}
\end{figure}

FlexQueue inherits the simplicity of a calendar queue, but differs in a few key
areas. First, since a calendar queue uses only a pointer array $A$ to manage
buckets, the \texttt{deleteMin} operation necessarily has to check each bucket
sequentially in order to find the next event. This behaviour is acceptable in
cases where the timer facility or operation system is already spending CPU time
to process each tick, but becomes inefficient when this per-tick bookkeeping
cost can be eliminated, such as in a tick-less runtime where the notion of
iterating through buckets is nonexistent.  FlexQueue adds a \emph{hierarchical
bit-vector} on top of $A$, which contains a summary of the state of $A$, using
one bit for each bucket. A 1-bit indicates that the bucket is non-empty and a
0-bit indicates the bucket is empty. This allows FlexQueue to query the bit
vector to find the next non-empty bucket, instead of searching in $A$ directly.
With the help of modern CPU instructions, querying the bit-vector can be much
faster than searching in $A$. Second, events outside of the finite horizon
represented by $A$ are not stored in the same array as those belonging to the
current horizon. Instead, they are stored in a separate overflow list $L$. This
ensures that any non-empty bucket as indicated by the bit-vector does indeed
contain the event which should be dequeued next. Finally, FlexQueue uses a
different strategy for adjusting the bucket width of $A$, based on the
assumption that regardless of the input distribution, the extent to which a
majority of event timestamps deviate from the mean is bounded.

To summarize, in addition to the pointer array from the original calendar queue,
FlexQueue also uses:

\begin{enumerate}

  \item A non-aggressive resizing policy for calculating bucket width.

  \item A bit-vector $V$, that allows the next non-empty bucket in $A$ to be
    located efficiently;

  \item A second pointer array $A_2$ and bit-vector $V_2$, that facilitates the
    non-aggressive resizing policy as explained in Section
    \ref{sec:pointer_array}.

  \item An collection of events $L$, or the overflow list,  for any events
    beyond the current calendar horizon.

\end{enumerate}

\section{Pointer Array and Overflow List}

\label{sec:pointer_array}

Given an uniform input distribution, list-based priority queues, such as the
timing wheel and the original calendar queue, are very efficient because the
events are uniformly partitioned and distributed into buckets, indexed by their
timestamps. In this scenario, there is no bucket that holds a large number of
events, nor is there a bucket that holds no events at all. Therefore, the
average access time to a bucket is reduced.  Furthermore, when the bucket width
$u=1$, as is the case with the original timing wheel, all events in $A[i]$ have
identical timestamps and thus the bucket itself can be implemented as an
unsorted linked list, instead of a sorted data structure. This further reduces
the complexity and average access time to each bucket. However, such assumption
is unrealistic as the FlexQueue is designed to vary the bucket width $u$
periodically, in order to prevent buckets from becoming overpopulated as well as
underpopulated.

The pointer array $A$ that FlexQueue uses is a statically allocated
$N$-component array, where $N$ is a pre-determined integer that is a power of
two. As a result, the bit-vector $V$ that FlexQueue uses, contains $N$ bits. It
is also FlexQueue's goal to attempt to balance the variation in bucket sizes and
therefore, it is expected that the majority of buckets will contain some
reasonable number of events rather than being empty. This goal further justifies
using a static array as the amount of wasted memory is expected to be low.  On
the other hand, one could easily modify $A$ to be any other randomly accessible
data structure, if dynamic memory allocation is not a concern. For example, a
hash table can be considered if the total number of events is very small
compared to $N$.

Figure \ref{diag:static_horizon} illustrates the operation of such a static
pointer array whose horizon $h=20$ does not vary. Solid dots represent events,
and are loosely distributed across $A$. Any non-empty bucket has its
corresponding bit in $V$ set to 1.  Note here the use of a second pointer array
and bit-vector $A_2$ and $V_2$. This is done for two reasons. First, using a
second pointer array ensures that periodically at least one of the pointer
arrays will be empty. If only one pointer array would be used (by treating $A_1$
as a circular buffer), then it is likely that $A_1$ is never empty. Resizing
$A_1$ while it is non-empty implies that events need to be relocated to the
correct bucket, which can result in a latency spike.  On the other hand, using a
second pointer array also ensures that the first non-zero bit of $V_1$ (or
$V_2$, whichever represents the current horizon) always points to the bucket
that contains the event with the highest priority. It is explained in Section
\ref{sec:ffs_operation} why this has the added benefit that searching for the
next non-zero bit is faster.

The next question is the type of data structure to be used for each bucket, and
whether it should be sorted or unsorted. A sorted data structure is required if
the goal is to guarantee all events be dequeued in exact temporal order. This is
similar to the behaviour of tree-based algorithms and results in a queue that
can perform sorting correctly. An unsorted bucket implies that two events with
timestamps $e_1$ and $e_2$ dequeued consecutively does not necessarily satisfy
$e_1 \leq e_2$. As the bucket width $u$ grows and shrinks, so does the potential
distance of events dequeued out-of-order. FlexQueue supports both variants, and
can be configured to use either scheme with trivial effort. 

It is the responsibility of the overflow list $L$ to hold ``outlier" events, so
that the remaining events may be stored in the pointer array without introducing
too much skew. As a result of this, those events that are not stored in the
array may become significantly more skewed compared to the original
distribution. Thus, it is logical to implement $L$ using a tree-based algorithm
which is insensitive to input distribution.

\begin{figure}
  \centering
  \includegraphics[scale=0.8]{diagrams/static_horizon_seminar}
    \caption{Static horizon}
  \label{diag:static_horizon}
\end{figure}

\section{Dynamic Horizon}

Given that FlexQueue uses no dynamic memory allocation, this also implies that
it cannot allocate memory for new events inserted. Fortunately, intrusive data
structures are common in operating system kernels and timer management systems
as previously mentioned. In these systems, metadata such as pointers are stored
in the same structure as the event itself, using memory previously allocated by
a third party such as a user-level program. Furthermore, since changing $u$
is the only dynamic adaptation that does not introduce latency spikes or require
memory allocation and expensive event copying, it is important to modify $u$ in
a way that ensures a reasonable partition of incoming events.  FlexQueue shows
that a simple strategy using a dynamic horizon is sufficient to obtain good
performance in most cases.

Existing variations of the calendar queue usually compute the number of buckets
needed dynamically, in addition to modifying the bucket width.  These changes
are combined in order to guarantee that subsequent enqueues and dequeues are
efficient.  More buckets are added when the average number of elements in each
bucket is too large. Whenever such a resize occurs, a large number of existing
elements may have to be relocated so that they are stored in the correct bucket
under the newly computed values. This results in unpredictable memory allocation
overhead and a latency spike.  FlexQueue uses a fixed-sized bit-vector and
pointer array, and varies $u$, the time spanned by each bucket or the bucket
width, instead of adding or removing buckets. This way memory is allocated
upfront, and no dynamic allocation is performed during normal queue operations.

Among other things, if the size of $L$ grows too large, this indicates that the
current horizon may be too small and that the pointer array $A$ is not being
used efficiently. This is because $L$ is implemented as a tree-based data
structure as mentioned before, and if $L$ holds the majority of events, then the
expected performance of the queue will approach $O(\lg n)$. In order to ensure
such degradation does not occur, the next event to be dequeued must be stored in
$A$ with a high probability. In such cases, a resize should be triggered by
calculating a new value for $u$. Ideally, a smooth and even distribution of
elements across all buckets is achieved, but this is impossible unless the input
distribution itself is uniform. However, an approximation can be made by
ensuring the standard deviation of events stored in $A$ does not become too
large.

\subsection{Calculating Horizon Size}
\label{sec:intro_k}

Suppose that the input distribution is normal, if all events are stored to $A$,
then a majority of events (approximately 68\%) will be concentrated within one
standard deviation from the mean. Furthermore, within one standard deviation,
the difference between the most and least populated bucket will not be as
significant as if the entire population of events are considered. This suggests
that $u$ can be adjusted using the sample mean and standard deviation as a
guideline. Effectively, $A$ will store all events that fall within a
\emph{two-sided truncated} normal distribution. If the input distribution is not
normal however, it is not possible to be precise about the degree to which
events are centred around the mean, but approximation is still possible through
the Chebyshev's inequality \cite{ross2004} which states that:

\begin{cheb}

  If $X$ is a random variable with finite expected value $\mu$ and finite
  non-zero variance $\sigma^2$, then for any real number $k>0$, there is
  $P(|X-\mu|\geq k\sigma) \leq \frac{1}{k^2}$.

\end{cheb}

A direct result of this inequality is that regardless of the input distribution,
suppose $k=1.5$, then at most 44\% of all events will be more than 1.5 standard
deviations away from the mean.  Of course, this is a more relaxed bound compared
to one that can be computed if the distribution is known in advance. For
example, if $k=2$, then $P=25\%$ by this inequality, while a normal distribution
with $\mu=0$ and $\sigma^2=1$ has approximately 95\% of all values within two
standard deviations according to the three-sigma rule \cite{leonard2009}.
Setting $k$ to a large value guarantees that $L$ cannot hold too many events,
which is critical to prevent FlexQueue from degrading to a generic $O(\lg n)$
data structure.  At the same time however, this also increases the standard
deviation of the truncated distribution that $A$ is meant to store, resulting in
an even more skewed distribution in $A$ that $L$ exists to prevent. Clearly, an
appropriate value of $k$ is somewhere between these two extremes.  If there is
prior knowledge of the input distribution, then it is possible to calculate $k$
such that the size of a bucket is equal to the size of $L$. For example,
assuming the input distribution is normal with mean $\mu$ and variance
$\sigma^2$, then the percentage of events $E_A$ that is stored in $A$ is given
by integrating the probability density function of the normal distribution as
follows:
\begin{align*}
  E_A &= \int_{\mu-k\sigma}^{\mu+k\sigma} pdf(x,\mu,\sigma^2) dx\\
      &= \int_{\mu-k\sigma}^{\mu+k\sigma} \frac{1}{\sqrt{2\pi \sigma^2}} e^{-\frac{(x-\mu)^2}{2\sigma^2}} dx
\end{align*}
Let $x=y+\mu$, $y = \sqrt{2\sigma^2} z$ and integrating by substitution:
\begin{align*}
  E_A &= \int_{-\frac{k}{\sqrt{2}}}^{\frac{k}{\sqrt{2}}} \frac{1}{\sqrt{2\pi \sigma^2}} e^{-\frac{(\sqrt{2\sigma^2}z)^2}{2\sigma^2}} \sqrt{2\sigma^2} dz\\
      &= \frac{1}{\sqrt{\pi}} \int_{-\frac{k}{\sqrt{2}}}^{\frac{k}{\sqrt{2}}} e^{-z^2} dz \\
      &= \erf\left( \frac{k}{\sqrt{2}}\right)
\end{align*}
Therefore, the percentage of events $E_H$ that is stored in $L$ is simply
$1-\erf\left( \frac{k}{\sqrt{2}}\right)$, and the value of $k$ such that
\begin{align*}
  \frac{\erf\left(\frac{k}{\sqrt{2}}\right)}{N} &= 1 -
  \erf\left(\frac{k}{\sqrt{2}}\right)\\
  \erf\left(\frac{k}{\sqrt{2}}\right) &= \frac{N}{N+1}
\end{align*}
guarantees that on average the size of the overflow list is the same as the
average bucket size. Here $$\erf(x) = \frac{1}{\sqrt{\pi}}
\int_{-x}^{x}e^{-t^2}dt$$ is a well known non-elementary function, which means
that an analytical solution is not possible. An implementation of this function
is available in the standard \texttt{C++} library that approximates its value
with high precision. However, this implementation uses expensive floating point
operations that FlexQueue intends to avoid, for example, when being used inside
an operating system kernel.  Also note that there is no analytical solution for
$k$ to this equation, and an answer can be computed only through methods such as
a numerical simulation. This makes it impractical to dynamically adjust $k$ as
part of the resizing policy, even with prior knowledge of the input
distribution. For this reason, Chapter 4 presents an experiment to determine an
appropriate value for $k$, and leaves this as a configuration parameter, similar
to the bucket count $N$.

In the meantime, both the average and the standard deviation can be approximated
efficiently after each \texttt{insert} operation, by using an exponential
weighted moving average algorithm. Similar to the method used to estimate
\ac{rtt} in TCP \cite{paxson2011}, this approach has been used successfully in
practice and can be implemented efficiently as follows. If the current mean is
$\mu$, the current standard deviation is $\sigma$ and a new event is $\Delta$
time units away, then the new estimate $\mu' = \alpha\mu + (1-\alpha)\Delta$,
and the new standard deviation $\sigma' = \beta\sigma + (1-\beta)|\Delta - \mu|$
where $\alpha$ and $\beta$ are the weights given to the current estimate. If
$\alpha$ and $\beta$ can both be written as fractions with powers of two as
denominators, then the division operations involved in computing $\mu'$ and
$\sigma'$ can be replaced with bit shift operations. For this reason, typically
$\alpha = 0.875 = \frac{7}{8}$ and $\beta = 0.25 = \frac{1}{4}$.

Once a new value is calculated for $u$, this updated estimate can be applied to
incoming events by setting \texttt{CurrentHorizonMin} to $\mu' - k \sigma'$ or
0, whichever is greater, and \texttt{CurrentHorizonMax} to $\mu' + k \sigma'$.
To avoid copying existing events and thus introducing a latency spike, a second
bit-vector $V_2$ and pointer array $A_2$ is used. $A_1$ and $A_2$ can have
different values for $u$, as it is updated for one pointer array at a time.
Effectively, the actual horizon remains equal to that of a single pointer array.
Let $u_i$ denote the bucket width for pointer array $A_i$, then the correct
bucket and pointer array for an event with absolute timestamp $t$ can be
calculated as follows in Algorithm \ref{alg:calcbucket}. This assumes that this
event belongs to $A$, in other words, $t - \textsc{CurrentTime()}$ is between
\texttt{CurrentHorizonMin} and \texttt{CurrentHorizonMax}. The first return
value indicates which one of the two bucket arrays the event belongs, and the
second return value is the index of the correct bucket.

\begin{algorithm}
  \caption{CalcBucket}
  \label{alg:calcbucket}
  \begin{algorithmic}[1]
    \Function{CalcBucket}{$t$}
      \State $interval \gets t - \Call{CurrentTime}{ } $
      %\If{$interval < \texttt{CurrentHorizonMax} \And interval \geq \texttt{CurrentHorizonMin}$}
        \State $p \gets interval / u_i$
        \If{$p \geq N$}
          \State $p \gets (t - (u_i * N)) / u_{2-i}$
          \State \Return $2-i, p$
        \Else
          \State \Return $i, p$
        \EndIf
      %\Else
      %  \State \Call{Insert}{H,}
      %\EndIf
    \EndFunction
  \end{algorithmic}
\end{algorithm}

The bucket width $u$ is updated for an $A_i$ whenever it becomes empty as a
result of \texttt{deleteMin} operations. This is because applying an update at
this point requires no event copying since all events in $A_i$ have just been
dequeued. Using the estimated mean $\mu$ and standard deviation $\sigma$, $u_i$
is updated as follows in Algorithm \ref{alg:resize}. It is possible that the new
horizon value does not divide evenly into $N$, resulting in a non-integer $u$.
In this case, $u$ is simply rounded up to the nearest integer, and
\texttt{CurrentHorizonMax} is adjusted accordingly by setting it to
$\texttt{CurrentHorizonMin} + u\cdot N$.

Note that a consequence of triggering the resize only after one of $A_i$ becomes
empty is that if the current horizon $A_i$ is already holding a large number of
events, then \textsc{Resize} is not triggered after all events in $A_i$ are
first dequeued. The amount of time this process takes depend on this arbitrary
number, as well as how often incoming events are stored to $A_i$. If many
incoming events are close to \texttt{CurrentHorizonMin}, then the number of
events in $A_i$ will decrease very slowly. The result is that FlexQueue does not
instantly react to input distribution changes. This would only have a severe
impact if the input distribution changes drastically as soon as the current
horizon comes to an end, which is unlikely.

Figure \ref{diag:dynamic_horizon} illustrates the idea of a dynamically
adjustable horizon. Whenever a horizon has fully elapsed, the empty pointer
array has its semantics updated using the estimated mean and standard deviation.
This can result in a horizon that does not necessarily begin at the current
time. Therefore, in addition to overflowing events, underflowing events (those
that are too close to the current time) are also stored in $L$. To reiterate,
the use of a second pointer array creates opportunities where one of them is
empty and can be modified without affecting any existing events. Here the
effective horizon is represented partially by $A_1$ and partially by $A_2$.

\begin{figure}
  \centering
  \includegraphics[scale=0.8]{diagrams/dynamic_horizon_seminar}
    \caption{Dynamic horizon}
  \label{diag:dynamic_horizon}
\end{figure}

\begin{algorithm}
  \caption{Resize}
  \label{alg:resize}
  \begin{algorithmic}[1]
    \Function{Resize}{$\mu, \sigma$}
      \State $\texttt{CurrentHorizonMin} \gets \Call{Min}{\mu - k \sigma, 0}$
      \State $\texttt{CurrentHorizonMax} \gets \mu + k \sigma$
      \State $u_i \gets \lceil (\texttt{CurrentHorizonMin} +
      \texttt{CurrentHorizonMax})/N \rceil$
    \EndFunction
  \end{algorithmic}
\end{algorithm}

\subsection{Insert}

The remaining parts of \texttt{Insert} and \texttt{deleteMin} are not very
different from their counterparts in the calendar queue.  When FlexQueue needs
to insert an event, it first checks if it belongs to the overflow list by
comparing the difference of its timestamp and the current time with the current
horizon minimum and maximum, \texttt{CurrentHorizonMin} and
\texttt{CurrentHorizonMax}.  If it belongs in $A_1$ (or $A_2$), then it
calculates the correct bucket in $A_1$ or $A_2$ using \emph{interval}, and
inserts the event into the corresponding bucket.  Finally, FlexQueue updates the
current estimate of mean and standard deviation using the moving average method
previously mentioned. Pseudocode for \texttt{Insert} is presented in Algorithm
\ref{alg:insert}.

As mentioned previously, the values of $\alpha$ and $\beta$ for the moving
average calculation are selected so that floating point operations and integer
divisions can be replaced with bit shift.  For example $\alpha = 0.875$ means
that $\alpha\mu + (1-\alpha)\Delta$ is equivalent to $(7\mu + \Delta) >> 3$.

\begin{algorithm}
  \caption{Insert}
  \label{alg:insert}
  \begin{algorithmic}[1]
    \Function{Insert}{$P, t$}
      \State $interval \gets \Call{CurrentTime}{ } - t$
      \If{$interval < \texttt{CurrentHorizonMax} \And interval \geq \texttt{CurrentHorizonMin}$}
        \State $p, i \gets \Call{CalcBucket}{t}$
        \State $\Call{BucketInsert}{A_i[p],t}$
        \State $\Call{SetBit}{V_i,p}$
      \Else
        \State $\Call{OverflowInsert}{t}$
      \EndIf
      \State $\mu \gets \mu \alpha + interval(1-\alpha)$
      \State $\sigma \gets \sigma \beta + (interval - \mu)(1-\beta)$
    \EndFunction
  \end{algorithmic}
\end{algorithm}

\subsection{DeleteMin}

When FlexQueue needs to dequeue the next event, it must check both pointer
arrays $A_1$ and $A_2$ as well as $L$, and return the event with the highest
priority. If the event is inside one of the pointer arrays, then it is removed
from the bucket and if no more events remain within the bucket, the
corresponding bit in $V_1$ or $V_2$ is cleared.

If the current pointer array is $A_i$ but the next event is found in $A_{2-i}$,
then this implies that the current horizon has elapsed and a resize should be
triggered. From here, \textsc{Resize} is called and the mean and
standard deviation are used to compute the next horizon and its corresponding
pointer array $A_{2-i}$. This process is very efficient because changes to the
next horizon are purely semantic: no events are moved and no new buckets are created
or existing ones destroyed. Pseudocode for \texttt{DeleteMin} is given in
Algorithm \ref{alg:deletemin}.

\begin{algorithm}
  \caption{DeleteMin}
  \label{alg:deletemin}
  \begin{algorithmic}[1]
    \Function{DeleteMin}{$P$}
        \State $pos \gets \Call{FindFirstSet}{V_{cur}}$
        \State $i \gets cur$
        \If{$pos = \texttt{INTMAX}$}
          \State $pos \gets \Call{FindFirstSet}{V_{2-cur}}$
          \State $i \gets 2-cur$
        \EndIf
        \State $v \gets \Call{FirstEvent}{A_i[pos]}$

        \State $h \gets \Call{FirstEvent}{H}$

        \If{$v < h$}
          \State $\Call{DeleteFirst}{A_i[pos]}$
          \If{$A_i[pos] \textbf{ is empty}$}
            \State $\Call{BitClear}{V_i, pos}$
          \EndIf

          \If{$i \textbf{ not equal } cur$}
            \State \Call{Resize}{ }
          \EndIf
          \State \Return $v$
        \Else
          \State \Return $h$
        \EndIf
    \EndFunction
  \end{algorithmic}
\end{algorithm}

\subsection{Delete}

Apart from \texttt{insert} and \texttt{deleteMin}, \texttt{delete} is another
operation that is commonly associated with a priority queue. The \texttt{delete}
operation differs from \texttt{deleteMin} in that \texttt{deleteMin} only
removes the event with the highest priority, while \texttt{delete} removes an
arbitrary event given its timestamp.

The \texttt{delete} operation is important in a timer system such as network
protocol timeouts because a large number of timers are created with the
assumption that a timeout indicates failure. For example, if a TCP
re-transmission timer expires, this means a previous transmission was not
successful, potentially due to network congestion causing packets to be dropped.
Therefore, most of these timers are actually cancelled during normal operations,
before they expire, and are be removed from the queue through \texttt{delete}.
FlexQueue supports \texttt{delete} in the same way \texttt{insert} is supported.
Using the timestamp, the correct bucket in $A_1$ or $A_2$ can be computed with
the same algorithm presented in Algorithm \ref{alg:insert}. Once the correct
bucket is located, the appropriate \textsc{Remove} function on the bucket is
called to remove the event. If the bucket is implemented as a sorted linked
list, then \textsc{Remove} must iterate through the list until the correct event
is found.  However, given a intrusive linkage, there is no need to search in $V$
or $A$.  Assuming the list is doubly linked, the event itself contains
sufficient pointer information to perform \textsc{Remove} in $O(1)$ time.
Because of the simplicity of this operation, this thesis does not attempt to
measure the performance of \texttt{delete}, and instead focuses only on
\texttt{insert} and \texttt{deleteMin}.

\section{Hierarchical Bit Vector}

Bit vectors are capable of storing a universe of bits in an extremely compact
fashion. With modern CPU support, finding the first set bit in a machine word is
very efficient. It is possible to take advantage of this efficiency and
construct a bit-vector composed of multiple machine words, such that scanning
the bit-vector is equivalent to scanning each machine word sequentially.
However, a bit-vector constructed in this fashion is inefficient when the number
of words becomes large, for example, to support a larger universe. One such
large universe is the timestamps in event simulation problems, which are
typically 64-bit integers. To accommodate this, a bit-vector must have capacity
for $2^{64}$ bits, or $2^{58}$ words on a 64-bit machine. Scanning these words
sequentially is impractical.

Some tree-based priority queues such as the \ac{veb} tree use summary vectors to
efficiently skip consecutive zero bits during search. Similarly in FlexQueue,
the bit-vector is organized hierarchically, with level 0 being the plain bit
vector and every level above being a summary vector of the level below. Exactly
how many lower level bits are represented by an upper level bit can vary, and
this \emph{fanout} factor determines the total number of levels required. For
example, a $2^{32}$ hierarchical bit-vector with a fanout $f=64$ bits has a
total of $L=6$ levels, 5 of which are summary vectors. In this case, every
$f=64$ bits in a lower level are represented by a single bit in the level above,
thus, the total number of levels required is $\lfloor\log_{64}2^{32}\rfloor =
\lfloor 32/6 \rfloor = 6$ levels. Searching in this structure starts from the
topmost level and proceeds downward. This scheme requires exactly one word scan
on a 64-bit machine at each level and guarantees to find the result in time
proportionally to the height of the hierarchical bit-vector. Figure
\ref{diag:16bit_hbv} shows the organization of a very simple bit-vector with a
total of 16 bits and $f=4$, assuming 4-bit words.  At Level 0, all 16 bits are
divided into 4 words. This can be stored using an array $a$ with 4 elements,
with the least significant bit of the $a[0]$ being $b_0$, and the most
significant bit of $a[3]$ being $b_{15}$. Since $f=4$, every four bits in Level
0 is represented by one bit in Level 1, using a total of four bits to represent
all 16 bits in Level 0.  In this example, two word scans are needed to reach the
first set bit, one scan in each level. Generally, searching such a structure is
an $O(\lg N)$ operation, where $N$ is the size of the bit universe. Note that
$N$ is not the same as the finite horizon of $A$, since each bit may be used to
manage a bucket spanning more than one unit of time.

\begin{figure}
  \centering
  \includegraphics[scale=0.7]{diagrams/16bit_hbv}
    \caption{A hierarchical bit-vector with capacity $C=16$ bits}
  \label{diag:16bit_hbv}
\end{figure}

% TODO label figure as a) and b); need to study gnuplot some more
The next question is whether it is possible to further reduce the latency of
searching in this bit-vector. Note that each \texttt{bsf} scan instruction can
scan at most one machine word (typically $W=64$ bits) and modern CPUs typically
have a cache line size of 64 bytes (512 bits). This implies that scanning a
level when $f=64$ loads not just the word required for \texttt{bsf}, but also an
additional 56 bytes from memory. These extra bytes are not needed for searching
in the current level, and therefore, cache loads are not being used in the most
efficient manner for any $f < 512$.  By increasing $f$, the number of extraneous
bytes loaded decreases and so does the number of levels.  Thus the height of the
bit-vector will be reduced as well, without incurring extra cache loads. A bit
vector with $2^{32}$ bits now has four levels, if $f$ is increased to 256 from
64. This improved cache utilization may result in a faster search operation.
However, as the number of cache loads remain constant per level, the downside is
that this also increases the number of scans required per level, from one $f=64$
to four at $f=256$. If a hierarchical bit-vector has $T$ bits and the fanout is
$f$, then it has $L=\lfloor \frac{\lg T}{\lg f} \rfloor$ levels.  Clearly, if
$f' = f^2$, then $L'= \frac{1}{2}L$. Since $n=\lceil f/W \rceil$ scans are
needed per level, this changes the number of searches required to $$L'n' =
\frac{1}{2} L \cdot nf = \frac{f}{2} Ln.$$ Clearly, as long as $f>2$, this
results in a larger number of searches overall.  Figure \ref{fig:fanout_effect}
shows the effect of fanout selection, with various levels. In Figure
\ref{fig:fanout_effect_c}, the horizontal axis represents the total number of
bits in the bit-vector, while the vertical axis represents the latency of one
million hold operations, in microseconds. It is clearly visible that there is a
latency increase at $2^{13}$, $2^{19}$, and $2^{25}$, while the latency stays
relatively flat for each interval in-between.  These values correspond to when
the number of levels of the bit-vectors increases by 1 and illustrates the
effect on latency when the number of levels is increased. In Figure
\ref{fig:fanout_effect_w}, the total number of bits is fixed, and the fanout $f$
is varied from 64 to 2048.  This illustrates the effect of decreasing the number
of levels, while making each level more expensive to search.  The latency of
finding the first set bit increase more drastically after $f=2^9$, since this
means that a summary vector now occupies at least two cache lines rather than
one, when $f<2^9$. Thus, for the remainder of the thesis, it is assumed that
$f=64$.

\begin{figure}
  \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/fanout_effect_c}
    \caption{as a function of $C$}
    \label{fig:fanout_effect_c}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/fanout_effect_w}
    \caption{as a function of $f$}
    \label{fig:fanout_effect_w}
  \end{subfigure}
  \caption{Impact of fanout factor $f$ on hold time}
  \label{fig:fanout_effect}
\end{figure}

\subsection{SetBit Operation}

A bit in $V$ can be uniquely identified using three subscripts: the $i$-th
level, the $k$-th word, and the $r$-th bit.  Setting a bit is performed
bottom-up: setting the bit in Level $i$, then setting the $\lfloor
\frac{i}{fanout} \rfloor$-th bit in Level $i+1$, and then updating the summary
vectors repeatedly until the top-most level is reached. This requires exactly
$L$ iterations, although it is possible to stop as soon as $V[i][k][r]=1$, since
every level thereafter must already be 1.

\begin{algorithm}
  \caption{BitSet}
  \begin{algorithmic}[1]
    \Function{BitSet}{$V, L, k$}
      \For{$i \gets 0, L - 1$}
        \State $r \gets k \bmod f$
        \State $k \gets k / B$
        \State $V[i][k][r] = 1$
      \EndFor
    \EndFunction
  \end{algorithmic}
\end{algorithm}

\subsection{ClearBit Operation}

Setting a bit in $V$ to zero is slightly more involved, because in order to
clear the corresponding bit in Level $i+1$, the current word must entirely
consist of zero bits. This requires a branch and the loop must terminate as soon
as the current word is no longer zero. Similar to \texttt{BitSet},
\texttt{BitClear} also requires at most $L$ iterations.

\begin{algorithm}
  \caption{BitClear}
  \begin{algorithmic}[1]
    \Function{BitClear}{$V, L, k$}
      \For{$i \gets 0, L - 1$}
        \State $r \gets k \bmod f$
        \State $k \gets k / B$
        \State $V[i][k][r] = 0$
        \If{$V[i][k] \textbf{ not } 0$}
          \State \Return
        \EndIf
      \EndFor
    \EndFunction
  \end{algorithmic}
\end{algorithm}

\subsection{FindFirstSet Operation}

\label{sec:ffs_operation}

Finding the first non-zero bit is also very straightforward. Starting at Level
$L$, which by definition has at most one word, find the first non-zero bit at
position $p$, then the next word to scan is simply the $p$-th word in Level
$L-1$. This is repeated until level 0 is reached, at which point the final
position can be calculated.

\begin{algorithm}
  \caption{FindFirstSet}
  \label{alg:ffs}
  \begin{algorithmic}[1]
    \Function{FindFirstSet}{$V, L$}
      \State $p \gets \Call{WordScan}{V[L-1][0]}$
      \If{$p=f$}
        \State \Return \texttt{INTMAX}
      \EndIf
      \For{$i \gets L - 2, 0$}
        \State $l \gets \Call{WordScan}{V[i][p]}$
        \State $p \gets f*p + l$
      \EndFor
      \State \Return $p$
    \EndFunction
  \end{algorithmic}
\end{algorithm}

\textsc{WordScan} is a wrapper function that calls the built-in macro
\texttt{\_\_builtin\_ffs} in GNU \texttt{gcc}, which depending on the
implementation and the architecture, uses \texttt{bsf} on x86, or an
functionally equivalent software implementation if \texttt{bsf} is not supported
in hardware. According to the GNU \texttt{gcc} documentation, if the return
value of this built-in macro is zero, it indicates that the input word is empty.
In this case, $f$ is returned as a signal that no one-bit is found.  Otherwise,
the return value $s$ is the index of the least significant one-bit of W,
starting at 0.

\begin{algorithm}
  \caption{WordScan}
  \begin{algorithmic}[1]
    \Function{WordScan}{$W$}
      \State $s \gets \Call{\_\_builtin\_ffs}{W}$
      \If{$s=0$}
        \State \Return $f$
      \Else
        \State \Return $s-1$
      \EndIf
    \EndFunction
  \end{algorithmic}
\end{algorithm}

Variations of \textsc{FindFirstSet} are possible, for example, if one wishes to
begin the search not from the least significant end, but rather from some other
non-zero position $p$. In this case, a simple top-down scan as presented in
Algorithm \ref{alg:ffs} does not work, since finding the first non-zero bit
using this algorithm relies on the principle that the first non-zero bit in
Level $i$ is represented also by the first non-zero bit on Level $i+1$.  The
same however does not apply to an arbitrary $p$. At minimum, the Level 0 word in
which bit $p$ is stored (word $\lfloor \frac{p}{f} \rfloor$) must be searched
since there is no information elsewhere on whether there are multiple 1-bits in
a word. If there is no one-bit after bit $p$ on Level 0, then the bits on Level
1 can be used to the find the next non-zero word, by searching word $\lfloor
\frac{p}{f^2} \rfloor$ on Level 1. If the next one-bit is far away from $p$,
then this process may repeat until Level $L$ is reached, after which a top-down
search can be performed in the same way as Algorithm \ref{alg:ffs}.  This
variation requires at most $2L$ iterations and is slower than when $p$ can be
assumed to be zero.

In any case, $V$ is only responsible for the meta-information, while the events
themselves are managed by the pointer array $A$. After the first non-empty
bucket is located efficiently, the correct event can be removed from that
bucket.

