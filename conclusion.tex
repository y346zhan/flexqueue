% !TEX root = main.tex

\chapter{Conclusion and Future Work}

This thesis presents FlexQueue, a priority queue design that is suitable for use
in system software such as discrete event simulators and operating system
kernels.  FlexQueue is different from other calendar queue variations in three
ways.  First, FlexQueue uses an efficient hierarchical bit vector to locate the
bucket where the highest priority item is stored. This significantly lowers the
cost of both \texttt{insert} and \texttt{deleteMin} operations. Searching
through the bit vector still takes time proportional to the logarithm of the
total number of buckets, but given that the number of buckets is fixed, the cost
is practically constant. Second, in order to ensure the bit vector can be
queried at all times, events that do not fit in the current horizon are stored
separately in an overflow list $L$. Finally, to minimize the possibility of
having too many events in $L$ as a result of a skewed input distribution,
FlexQueue uses a dynamic horizon that is able to track basic shifts in input
characteristics.

A bench-marking tool has been implemented to evaluate the effectiveness of these
changes. Since the bit vector is intended to operate on top of the pointer
array, its cost is compared with operations on a linked list. This represents
the cheapest operation possible on a given bucket, thus exaggerates the overhead
of the bit vector, if any. Results show that in the worst case, accessing the
bit vector is slightly faster than inserting elements into a linked list. In
addition, because the implementation of the bit vector uses \texttt{bsf} whose
input is at most one machine word, this limits the possibility of using a fanout
factor larger than 64 bits.  Benchmark experiments also demonstrate the effect
of dynamic horizon when both queue size and distribution changes. FlexQueue's
resizing policy is able to sense and track when the timestamps of the incoming
events are increasing, and responds by modifying the lower and upper bound of
the current horizon.  Through a combination of these techniques, FlexQueue is
able to perform competitively against popular tree-based priority queue
implementations when the queue size is sufficiently large.

This thesis compares FlexQueue only with other tree-based implementations. A
more general and comprehensive study should include selected list-based
implementations mentioned in Chapter 2. In addition, a clear weakness of
FlexQueue is that it is slightly slower when the queue size is small. In this
scenario, most of the buckets are presumably empty. As a result, attempting to
divide the already small universe of elements into a much larger number of
buckets becomes more expensive than a simpler data structure such as a binary
tree.  A possible solution is to allow the number of buckets to decrease, if it
is detected that a majority of buckets are unused.  These ideas are left for
future exploration.
