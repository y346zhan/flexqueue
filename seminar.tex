\documentclass{beamer}
\usepackage{subcaption}
\usepackage{pifont}

\newtheorem*{cheb}{Chebyshev's Inequality}

\newcommand{\vitem}{\vfill\item}
\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%

\setbeamertemplate{footline}{}
\setbeamertemplate{navigation symbols}{}

\addtobeamertemplate{navigation symbols}{}{%
  \usebeamerfont{footline}%
  \usebeamercolor[fg]{footline}%
  \hspace{1em}%
  \insertframenumber/\inserttotalframenumber
}

\title{FlexQueue: Simple and Efficient Priority Queue for System Software}
\author{Yifan Zhang}
\institute
{
  David R. Cheriton School of Computer Science\\
  University of Waterloo
}
\date{May 15, 2018}

% Show current section relative to ToC at the beginning of each section
\AtBeginSection[]
{
  \begin{frame}
    \frametitle{Table of Contents}
    \tableofcontents[currentsection]
  \end{frame}
}

\begin{document}

\frame{\titlepage}

\begin{frame}
  \frametitle{Table of Contents}
  \tableofcontents
\end{frame}

\section{Introduction}

\begin{frame}
  \frametitle{Definition}
  \begin{itemize}
    \item queue-like data structure where each element has a ``priority"
    \vitem higher priority items are served before lower priority items
    \vitem operations permitted:
      \begin{itemize}
        \item \texttt{insert} (\texttt{enqueue})
        \item \texttt{deleteMin} (\texttt{dequeue})
        \item \texttt{delete}
        \item \texttt{merge}
        \item \texttt{decreaseKey}
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Problem Statement}
  \begin{itemize}
      \item worst-case performance guarantee
      \begin{itemize}
        \item no slower than $O(\lg n)$
      \end{itemize}
      \vitem no dynamic memory allocation
      \begin{itemize}
        \item useful in resource-constrained environment
        \item \emph{limited}, \emph{fixed} amount of memory allocated upfront
      \end{itemize}
      \vitem focus on applications in OS kernels and runtime systems
      \begin{itemize}
        \item timer; memory management
        \item \texttt{insert} and \texttt{deleteMin} most relevant
      \end{itemize}
  \end{itemize}
\end{frame}

\section{Related Work}

\begin{frame}
  \frametitle{Priority Queue and Sorting}
  \begin{itemize}
    \item sorting algorithms as priority queues
      \begin{itemize}
        \item treesort (1962)
        \item conversion introduces hidden constants
      \end{itemize}
      \vitem specialized data structures: binary heap (1964)
      \vitem tree-based
      \begin{itemize}
        \item[\cmark] good theoretical bound
        \item[\xmark] hidden constants
        \item[\xmark] not adaptive to changing input
      \end{itemize}
      \vitem array-based
      \begin{itemize}
        \item[\cmark] faster and more adaptive
        \item[\xmark] unpredictable latency as a result of adaptation
      \end{itemize}
    \vitem hybrid: e.g. vEB tree
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Theoretical Results}
  \begin{itemize}
    \item \texttt{insert} + \texttt{deleteMin} $\geq O(\lg n)$
      \begin{itemize}
        \item \emph{min-heap}: $O(\lg n)$ \texttt{insert} and \texttt{deleteMin}
        \item \emph{binomial heap}: $O(1)$ \texttt{insert}
        \item \emph{splay tree}: $O(1)$ \texttt{deleteMin}
      \end{itemize}
    \vitem graph theory problems: e.g. shortest path
      \begin{itemize}
        \item \texttt{merge} and \texttt{decreaseKey} most relevant
        \item would like to break the $O(\lg n)$ bound
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Pragmatic Designs}
  \begin{itemize}
      \item timing wheel
      \begin{itemize}
        \item fixed-size array of lists
        \item sorted overflow list
        \item \texttt{insert} straightfoward; \texttt{deleteMin} scans array
        \item bad if most events in overflow!
      \end{itemize}
      \vitem calendar queue
      \begin{itemize}
        \item size of array depend on \emph{load factor}
        \item sorted vs. unsorted bucket
        \item sensitive to input changes, drastic resizing operation
      \end{itemize}
  \end{itemize}
  \begin{figure}
    \centering
    \includegraphics[scale=0.6]{diagrams/timing_wheel}
  \end{figure}
\end{frame}

\section{Design}

\begin{frame}
  \frametitle{Characteristics}
  FlexQueue differs from other distribution-aware priority queues:
  \begin{itemize}
    \vitem resizing policy
      \begin{itemize}
        \item no perodic event copying or relocation
        \item predictable latency
      \end{itemize}
    \vitem dynamic horizon
      \begin{itemize}
        \item simple and efficient to sense and compute
        \item less sensitive to small changes
      \end{itemize}
    \vitem hierarchical bit vector
      \begin{itemize}
        \item first non-empty bucket may be located efficiently
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{The Pointer Array}
  \begin{itemize}
      \item static array of size $N$, where $N$ is pre-configured
      \vitem use of second array $A_2$ eliminates resizing overhead
      \vitem \texttt{deleteMin} searches both $A$ and $L$
  \end{itemize}
  \begin{figure}
    \centering
    \includegraphics[scale=0.6]{diagrams/dynamic_horizon_seminar}
  \end{figure}

\end{frame}

\begin{frame}
  \frametitle{Dynamic Horizon}
  \begin{itemize}
      \item horizon have ``gap" at the front
      \vitem both underflowing and overflowing events stored to $L$
      \vitem goal: $L$ should not store too many/too few events
  \end{itemize}
  \begin{figure}
    \centering
    \includegraphics[scale=0.6]{diagrams/dynamic_horizon_seminar}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Effect of $k$}
  \begin{figure}
    \centering
    \includegraphics[scale=1]{figures/value_of_k}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Hierarchical Bit Vector}
  \begin{itemize}
      \item one-dimensional bit vector optimal in terms of space
      \vitem search speed limited by number of machine words
      \vitem one word per instruction on x86
      \vitem idea: use summary vectors to skip consecutive zero bits
  \end{itemize}
  \begin{figure}
    \centering
    \includegraphics[scale=0.5]{diagrams/16bit_hbv}
  \end{figure}
\end{frame}

\section{Benchmarks}

\begin{frame}
  \frametitle{Effect of Dynamic Horizon}
  \begin{itemize}
    \item piecewise uniform distribution: $[0, 2^{20})$, $[2^{20}, 2^{32})$
    \vitem favours static setup
  \end{itemize}
  \begin{figure}
    \begin{subfigure}{0.49\textwidth}
      \centering
      \includegraphics[width=\textwidth]{figures/piecewise_static_flex_p}
      \caption{Static}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
      \centering
      \includegraphics[width=\textwidth]{figures/piecewise_dynamic_flex_p}
      \caption{Dynamic}
    \end{subfigure}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Effect of Dynamic Horizon (cont.)}
  \begin{figure}
    \begin{subfigure}{0.49\textwidth}
      \centering
      \includegraphics[width=\textwidth]{figures/piecewise_static_flex_queue_size}
      \caption{Static}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
      \centering
      \includegraphics[width=\textwidth]{figures/piecewise_dynamic_flex_queue_size}
      \caption{Dynamic}
    \end{subfigure}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Versus Competitors}
  \begin{itemize}
    \item less favourable (for FlexQueue) distributions used
    \vitem more skewed vs. less skewed
  \end{itemize}
  \begin{figure}
    \centering
    \includegraphics[width=\textwidth]{figures/competitors}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{NS3 Simulation}
  \begin{itemize}
    \item two subnets connected by a fast link and a slow link
    \vitem fast link repeatedly connected and disconnected
    \vitem priority queue operations account for \textasciitilde 10\%
  \end{itemize}
  \begin{figure}
    \centering
    \includegraphics[scale=0.7]{figures/ns3_ripng}
  \end{figure}
\end{frame}

\section{Conclusion}

\begin{frame}
  \frametitle{Conclusion}
  \begin{itemize}
    \item hierarchical bit vector speeds up search
    \vitem resize at the end of every horizon
    \vitem dynamic horizon works best when $\sigma$ is larger
    \vitem scales well with larger queue size
    \vitem repository: \url{git.uwaterloo.ca/y346zhan/thesis}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{SetQueue Peculiarities}
  \begin{itemize}
    \item implemented as a red-black tree in \texttt{libstdc++}
    \vitem effective working size reduced due to large interval
  \end{itemize}
  \begin{figure}
    \centering
    \begin{subfigure}{0.49\textwidth}
      \includegraphics[width=\textwidth]{figures/piecewise_dynamic_flex_p}
      \caption{FlexQueue}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
      \includegraphics[width=\textwidth]{figures/piecewise_static_set_p}
      \caption{SetQueue}
    \end{subfigure}
  \end{figure}
\end{frame}



\end{document}
