% !TEX root = main.tex
% !TEX encoding = UTF-8 Unicode

\chapter{Background and Related Work}
\label{chapter:relatedwork}

\section{Priority Queue and Sorting}

The problems of designing an efficient priority queue and sorting integers have
received similar treatment in the early literature. In both cases, there is a
need to manage a totally ordered set of elements. As a result, solutions to
these problems are often very similar. For example, every priority queue can
also be used for sorting, by first calling a sequence of \texttt{insert}
operations followed by a sequence of \texttt{deleteMin} operations.  If the
priority queue supports both \texttt{insert} and \texttt{deleteMin} in $O(\lg
n)$ time, this method results in an $O(n\lg n)$ time sorting algorithm. More
recently, Thorup \cite{thorup2000} also proved the opposite: that a sorting
algorithm with complexity $O(f(n))$ implies that there is a priority queue with
$O(\frac{f(n)}{n})$ \texttt{insert} and \texttt{deleteMin}.  In fact, early
priority queue designs such as the \emph{binary heap} \cite{williams1964} are
originally proposed as a sorting algorithm.

However, despite the homogeneity in pure mathematical terms, there has been no
recent priority queue design that starts by building an efficient sorting
algorithm, because differences remain between the two problems.  In a sorting
problem, the data to be processed are often available offline. That is, the
problem input is known. A highly optimized sorting algorithm can therefore take
advantage of this complete knowledge, and for example apply the median of
medians method \cite{blum1973} for pivot selection used in \emph{QuickSort}. On
the other hand, the operations of a priority queue are similar to those of an
online sorting algorithm, where the data structure must be able to operate
efficiently under incomplete knowledge. Furthermore, the output of a sorting
algorithm is required to be a list of ordered elements, whereas a priority queue
does not necessarily maintain total order on every element at all times. Since
the primary operations are \texttt{insert} and \texttt{deleteMin}, only the
largest or smallest element needs to be readily available. Furthermore, many
results in Word-RAM sorting, including Thorup's, introduce hidden constants that
are not immediately obvious at a glance. These hidden constants often prevent
the theoretical superior techniques from running well in practice.

As a result of such distinctions, studies on these problems became largely
independent and research efforts began to diverge. On the one hand, there are
continued improvements to the original priority queue design, which focus on the
theoretical cost of those primary operations. On the other hand, more recent
proposals improve the efficiency by taking advantage of external information,
such as knowledge of the input workload.

\section{Improving the Theoretical Bound}

One direction of research into priority queues focuses on general algorithms.
That is, algorithms that make no assumption about the types of elements or the
system in which they are used. Floyd presents \emph{Treesort} as ``Algorithm
113" \cite{floyd1962}, that uses $O(2n)$ space to sort $n$ elements.  This
algorithm is essentially a tournament tree that is able to find the largest or
smallest elements in $N-1$ comparisons. In this tree, elements are compared
pair-wise to determine a ``winner", and winners are compared pair-wise
repeatedly until one element remains. From this idea, William proposes an
implicit version of the tournament tree, known as \emph{binary heap}
\cite{williams1964}, as the first priority queue implementation that supports
\texttt{insert} and \texttt{deleteMin}. This uses a single $n$-component array
$A$ to store $n$ elements and both operations maintain \emph{heap order}. That
is, $A[i] \leq A[2i]$ and $A[i] \leq A[2i+1]$ for all $0 \leq i \leq n-1$.  It
can be shown that both \texttt{insert} and \texttt{deleteMin} on the binary heap
are $O(\lg n)$ in the worst case, because any out-of-order element $A[j]$ needs
at most $O(\lg j)$ swaps with a parent element to restore heap order.

This worst-case bound of $O(\lg n)$ has been improved repeatedly by different
researchers. For example, Carlson \cite{carlsson1988} modifies the implicit heap
structure resulting in $O(1)$ \texttt{insert}, while \texttt{deleteMin} is still
$O(\lg n)$. The idea builds on the \emph{binomial heap} \cite{vuillemin1978} in
which elements are organized in a forest of trees that are powers of two in
size, and two trees in the forest can be merged in constant time if their height
is the same.

As mentioned before, any priority queue implementation can be used for sorting
by first inserting all elements, and then repeatedly calling \texttt{deleteMin}.
This implies that a generic priority queue has the same theoretical lower bound
as comparison-based sorting algorithms. That is, it is not possible to obtain a
better lower bound than $O(\lg n)$ for \texttt{insert} and  \texttt{deleteMin}
combined.  As such, there is an increased interest in optimizing other priority
queue operations, such as \texttt{merge} and \texttt{decreaseKey}. A basic
binary heap design has the disadvantage that a straightforward \texttt{merge}
still takes $O(n)$ time.  This is improved to $O(\lg n)$ by Crane with the
\emph{leftist heap} \cite{crane1972} by maintaining extra \emph{rank}
information about each node and its distance to the nearest leaf node. The
previously mentioned binomial heap reduces the space usage of leftist heap,
since it no longer needs to maintain the same rank information, while achieving
amortized $O(1)$ insert.  \emph{Skew heap} \cite{sleator1986} is later
introduced as a self-adjusting leftist heap, and proves that \texttt{merge} can
be executed in amortized constant time as well.  Note that in this case,
self-adjusting simply means that the heap itself does not maintain balance
information like the leftist heap.  Instead, the heap is modified each time it
is accessed to maintain balance.  Thus, the strength of the skew heap is its
amortized cost and not the worst-case cost.

Fredman notes the importance of \texttt{decreaseKey} in algorithms such as
Dijkstra's single source shortest path. The \emph{Fibonacci heap}
\cite{fredman1987} is then presented as a solution that supports
\texttt{decreaseKey} in amortized constant time, improving the solution for the
shortest path problem to $O(n\lg n + m)$, where $m$ is the number of edges.
Other standard priority queue operations such as \texttt{insert} and
\texttt{deleteMin} are also supported in amortized $O(1)$ and $O(\lg n)$ time,
respectively. 

So far, those tree-based algorithms do not inherently require dynamic memory
allocation, since the metadata of a node can be stored alongside its data using
an intrusive data structure. However, they bring increasing complexity as a
result of optimizing for the general case. In addition, at least one of
\texttt{deleteMin} and \texttt{insert} still takes $O(\lg n)$ time on average,
which is unfavourable for workloads that contain approximately an equal
proportion of \texttt{insert} and \texttt{deleteMin} operations.

More recent priority queues begin to make assumptions about the context of the
problem.  In discrete event simulation, the \emph{pending event set} contains
events that must occur at a specific time in the future. As simulation
progresses, these events are removed from the set in temporal order. In this
context, keys can be assumed to be non-negative integers that represent the
amount of simulation time elapsed since the beginning of the simulation. For
example, the \ac{veb} tree \cite{boas1976}  takes advantage of this assumption,
resulting in \texttt{insert} and \texttt{deleteMin} in $O(\lg\lg N)$ time, where
$N$ is the size of the key universe. This is accomplished by recursively
dividing the universe $N$ into children of size $\sqrt{n}$. This is a
significant improvement over previous results that do not make such assumptions.
Johnson \cite{johnson1981} builds on this using a non-recursive approach
resulting in a $O(\lg\lg D)$ bound, where $D$ is the difference between the
smallest and the largest item in the queue. The same idea is used by Anderson to
improve radix sort \cite{andersson1994}. Thorup \cite{thorup2000} modifies this
idea again achieving a priority queue that supports \texttt{deleteMin} and
\texttt{insert} in $O(\lg\lg n)$ time while using $O(n2^{\epsilon w})$ space,
where $n$ is the number for keys and $w$ is the maximum number of bits of each
key. However, a downside of the \ac{veb} tree and its derivatives is that they
also require space proportional to the size of the universe $N$. This $O(N)$
space complexity can be improved to $O(n)$ if hashing is used, allocating a
sub-tree for a child only when an element belonging to that sub-tree is first
inserted. However, this approach once again requires dynamic memory allocation.

\section{Timing Wheel and Array-Based Designs}

With the advent of data structures like the vEB tree, there has been a shift in
research efforts into more specific designs. In comparison to previous
tree-based algorithms, those designed for specific applications can make more
assumptions that are otherwise impossible. One such assumption is that time is
discrete rather than continuous. In other words, time is always represented as
an integer. The unit of the integer is typically 1 ns on a modern operating
system such as GNU/Linux.

Under these assumptions, Varghese proposes the \emph{timing wheel}
\cite{varghese1987} using a fixed-size array of list pointers. Each pointer
$A[i]$ in the array points to a linked list of timers that are due $i-i_0$ time
units in the future, where $i_0$ represents the current time. Suppose the size
of this array is $n$, and if there is an event that is more than $n$ time units
away, then this event is not stored in the array but in a separate sorted linked
list.  Note that there is no need to sort each $A[i]$ because by definition, all
events in a list are due at the same time.  In this context, \texttt{deleteMin}
is less relevant because it is also assumed that each element in the array
corresponds to a periodic timer tick, which the operating system must already
spend some CPU cycles on bookkeeping. Therefore, it is not useful to be able to
look ahead and find an event that is not due immediately.  Hence, the timing
wheel simply increments $i$, and processes the list that $A[i]$ points to, if
any.  This results in a constant time \texttt{insert} operation, without
amortization.

Brown \cite{brown1988} extends this concept with the \emph{calendar queue}.
Instead of letting $A[i]$ represent a single time unit, it can now represent an
interval, $u$, of time units. Effectively, the universe of $n$ future events is
partitioned into $m < n$ lists, each of these $m$ lists corresponds to an
element in $A$ and is known as a \emph{bucket}. This implies that within a
bucket, all events have similar but not necessarily identical timestamps. Thus,
there is a choice of implementing the bucket as either a sorted or unsorted
list. A sorted bucket means that events are dequeued in the same order as a
timing wheel. An unsorted bucket such as a FIFO list means that for each event
dequeued at time $t$, the next event can be up to $u$ time units before or after
$t$, introducing an error proportional to $u$.  Furthermore, the size of $A$ or
the number of buckets can be dynamically adjusted as well. In order to limit the
maximum number of events that can be stored in a single bucket, the following
method is used:

\begin{enumerate}

  \item Initially there are two buckets, and $u=1$.

  \item If the total number of events is more than twice the size of $A$, i.e.
    the number of buckets, then create a new $A'=A$ with twice the number of
    buckets and copy all events from $A$ to $A'$.

  \item Similarly, if the number of events is less than half of the size of $A$,
    then the $A'$ is created with half the number of buckets.

  \item Whenever a new $A'$ is created, the amount of time units each bucket
    represents is recomputed by calculating the average separation of a fixed
    number of events at the head of the queue.

\end{enumerate}

This method bounds the average number of times an existing event is copied and
prevents any $A[i]$ from eventually having to perform a costly linear search on
a large list. If the queue size grows to an exact power of two, then on average
each event is copied once. However, in the worst case the queue size grows to
one more than a power of two, then all events must be copied once more. More
recent variations of the calendar queue propose smarter heuristics on when and
how to modify $A$, but the problem remains that event copying can result in
large latency spikes as new events are inserted to ensure they remain in the
correct bucket.

\emph{Lazy queue} \cite{ronngren1991} and \emph{DSplay queue} \cite{siow2004}
are more recent variations that also use a multi-list structure. They do not
require resize operations in the same way as other dynamic calendar queues such
as the \emph{SNOOPy} queue \cite{tan2000}, in which frequent sampling of the
input is used to obtain the necessary metrics for deciding when and how to
resize. Lazy queue uses an unsorted list for far-away events, and defers sorting
to the first \texttt{deleteMin} operation. At that time, the bucket width is
computed using the minimum and maximum timestamps found in this overflow list,
and every event is moved into the appropriate bucket. Then, buckets are emptied
sequentially into a splay tree, where they become fully sorted. This eliminates
the sampling overhead present in many heuristics-based calendar queue
variations. However, as all events in the overflow list must be transferred at
the same time, a heavy latency spike is still possible. In contrast, FlexQueue
does not rely on intensively sampling the input nor does it require existing
events to be relocated each time the bucket width is adjusted. Furthermore,
these variations of the original calendar queue do not meet the design
objectives because buckets in $A$ are created and destroyed dynamically as the
total number of elements grows and shrinks.

\section{Concurrency}

\ac{pdes} emerged as an alternative way to cope with a complex system that is
far too time consuming to analyze using sequential simulation. \ac{pdes}
attempts to exploit the underlying parallelism in some system models and can
drastically speed up a simulation's execution time. However, one of the major
challenges as with any concurrency problem is synchronization between execution
units.  In particular, if simulation events are to be processed in parallel, the
implied causality in sequential simulation may not be preserved since the
processing order of events is no longer deterministic. In addition, any changes
to the global simulation state must be synchronized and this further limits the
degree to which the model is parallelizable.

To make use of array-based priority queues in a concurrent application, one
could simply apply spin locks either on the queue itself, or on the level of
individual elements in the array. Either way, controlling access to the array is
analogous to protecting the hash buckets in a hast table. It has been shown
\cite{sunxianda2015} that it is sufficient to use a spin lock at the bucket
level. This approach only occupies one addition bit per bucket, and is shown to
be simpler and faster compared to other techniques such as lock-free queue or
reader/writer lock.

\section{Evaluation Methodology}

Regardless of the design choices, it is possible to evaluate tree-based and
list-based priority queues using the same methods. For example, \emph{access
time} is an intuitive measurement to understand the performance of a priority
queue operation. That is, one could measure the time required to perform the
most basic operation of \texttt{insert} and \texttt{deleteMin}. On top of this,
the \emph{hold model} has been used for evaluation by nearly every paper that
proposes a new priority queue design \cite{ronngren1997}. In this model, the
queue is first populated using a fixed number of items generated by an
\emph{increment distribution} $\mathcal{P}_{inc}$. During this time,
self-adjusting data structures like the dynamic calendar queue may begin to
re-organize according to $\mathcal{P}_{inc}$. After this initial setup phase,
the \texttt{hold} operation is executed repeatedly, which consists of a
\texttt{deleteMin} and an \texttt{insert}.  Subsequent events inserted are
generated using the same distribution $\mathcal{P}_{inc}$. The total time for
all \texttt{hold} operations are then measured and an average access time can be
calculated.

This model is a useful representation of a discrete event simulation and can be
used to examine the relationship between access time and queue size. However, if
the queue is \emph{distribution-aware}, either in terms of tree balancing or
bucket resizing, then this model can be misleading as the internal queue
structure may have changed, even as the total number of elements remains
constant. In this situation, it is no longer meaningful to report an average
measurement as this does not capture the performance fluctuations as a result of
implementation-specific resizing policies. Jones \cite{jones1986} also points
out that in addition to the increment distribution, the initial distribution and
the resulting queue structure can both have a significant impact on the measured
time.

As a generalization of the hold model \cite{ronngren1997}, the Markov model
tries to better represent the random nature of event simulation by introducing
transition probabilities. In the hold model, a \texttt{deleteMin} operation
always comes after every \texttt{insert}. In a Markov-based model, each
\texttt{deleteMin} operation has a probability $p_1$ to transition into the
\emph{insert} state where the next operation will be \texttt{insert}. Similarly,
each \texttt{insert} operation has probability $p_2$ to transition into the
\emph{deleteMin} state, making the next operation a \texttt{deleteMin}. The
result is that there is a random sequence of \texttt{insert} and
\texttt{deleteMin} operations.  However, these models tend to mask the internal
re-organization of distribution-sensitive queues because they measure the
average of a group of temporally sequential operations.  Other models exist that
are better at exposing this characteristic, and they are often used together
with the hold model or the Markov model. The \emph{up-down model}
\cite{riboe1990} consists of a sequence of \texttt{insert} operations followed
by a sequence of \texttt{deleteMin} operations, making it easy to see the
effects of a growing/shrinking queue size and thus the effectiveness of the
resize operation.

\section{Simulator as a Testbed}

Aside from benchmark models, which are primarily simulations of real world
workloads, it is important to evaluate FlexQueue in a real-world setting.
FlexQueue is designed with many restrictions and trade-offs in mind, ones that
make it very suitable for use in a operating system kernel where memory
footprint should be minimized. However, it would be slow and difficult to assess
the effectiveness of FlexQueue's design choices in such an environment, because
integration with a kernel is a non-trivial undertaking. Therefore, given
priority queue's affinity to discrete event simulation, a network simulator is a
natural alternative test platform. A network simulator attempts to model the
internal state of a given system such as link speed, congestion, and routing
tables. In general, there are two approaches to building a simulator:
synchronous and asynchronous. In synchronous or clock-driven simulation, the
simulator checks for \emph{events}, or a modification to the current state at
each clock tick, giving an impression that time is progressing continuously. In
asynchronous or event-driven simulation, all changes to the simulation state are
processed in a first-in-first-out fashion, skipping any clock ticks that have no
events attached. These two approaches are not mutually exclusive however, as a
system being modelled can exhibit characteristics of both. A event can trigger
more events resulting in a complex dependency graph that must be followed to
ensure simulation accuracy. Asynchronous simulation represents an interesting
test case for FlexQueue because the \texttt{deleteMin} operation lends itself
naturally to finding the next event to be processed. As an example, Chapter
\ref{chapter:benchmark} presents modifications to an event-driven simulator
\texttt{ns3} to use FlexQueue.

% TODO repeat argument that asynchorous is interesting test case for FlexQueue.

