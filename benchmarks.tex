% !TEX root = main.tex

\chapter{Benchmarks}

\label{chapter:benchmark}

As previously mentioned, the primary operations on a priority queue are
\texttt{insert} and \texttt{deleteMin}, which is the focus of evaluation in this
chapter.  While other common operations, such as \texttt{decreaseKey} and
\texttt{delete}, are not directly evaluated, the methods and results presented
in this chapter allow the observation of FlexQueue's performance under the most
common workloads.

For the purpose of clarity, a preliminary set of experiments are conducted on
the individual components of FlexQueue: the bit-vector and the bucket structure.
The first part of these experiments is intended to measure the average access
time of a bit-vector and a linked-list. The linked-list is considered to be the
simplest and fastest data structure that may be used to implement buckets, thus,
this comparison shows the maximum overhead of adding a bit-vector on top of the
pointer array. The second part of these experiments focuses on the bucket
itself. Specifically, these experiments measure the difference between using an
intrusive and non-intrusive data structure.

Section \ref{sec:results} present experiments that evaluate the effectiveness of
FlexQueue's resizing policy. FlexQueue's hold time is first measured when the
horizon is not permitted to vary dynamically. The results are then contrasted
with those when the resizing policy is employed. Next, the performance  of
\texttt{insert} and \texttt{deleteMin} are compared to Fibonacci heap, red-black
tree, binomial heap, and the original calendar queue.  Implementations of these
queues are freely available in third-party libraries such as \texttt{boost}
except for the calendar queue, and they are selected for the following reasons:

\begin{enumerate}

  \item Fibonacci queue claims amortized $O(1)$ time for not just
    \texttt{insert} and \texttt{deleteMin}, but also \texttt{delete} and
    \texttt{decreaseKey}.  This implies that it has an average case complexity
    that is theoretically comparable to a calendar queue under optimal
    conditions. 

  \item Red-black tree is the implementation used by the \texttt{GNU C++}
    library for the \texttt{map} and \texttt{set} data structure. It is readily
    available for any \texttt{C++} program and thus it can be considered a
    default choice when a priority queue is needed. It is also the priority
    queue used by the popular network simulator \texttt{ns3}. For simplicity,
    this thesis refers to this as the set queue.

  \item Binomial queue is the one of the earliest designs of priority queue, and
    it claims to have favourable worst-case performance than other tree-based
    algorithms.

  \item The original calendar queue performs well if the input distribution is
    uniform and if the number of queue items does not fluctuate significantly.
    However, costly event copying and poor resizing policy are two flaws that
    are immediately obvious when compared with FlexQueue. This serves as a
    baseline for other calendar queue variations.

\end{enumerate}

\section{Experiment Design}

One of the primary objectives of FlexQueue is to achieve comparable performance
to existing priority queue implementations, while not requiring complex
heuristics and dynamic memory allocation. Other objectives are arguably less
relevant, if FlexQueue does not at least perform comparatively in an optimal
setup, such as uniform input distribution. In this distribution, the number of
events in each bucket remain consistent across the entire horizon. Thus, the
average access time is minimized.  More importantly, performance is critical
especially when the queue is intended to be useful in operating system kernels
and timer management systems. This section focuses on synthetic benchmarks and
access patterns, rather than real workloads. These benchmarks are specifically
crafted to stress FlexQueue's implementation in a particular way by allowing
experiment parameters to be easily controlled.

Each benchmark consists of three stages: model selection, initialization, and
simulation. During the model selection stage, one of the three experiment models
are selected as the harness that drives the remaining two stages.  Each model
represents a different set of criteria for performance evaluation.  For example,
the hold model measures the average latency for a pair of \texttt{insert} and
\texttt{deleteMin} operations under a fixed queue size, while the Markov model
measures the average access time of a single operation which may be
\texttt{insert} with probability $p$ and \texttt{deleteMin} with probability
$1-p$. During the initialization stage, an increment distribution
$\mathcal{P}_{inc}$ is selected that is used to populate the queue being
evaluated until the number of events in the queue reaches the specified size.
Finally, during the simulation stage, a large number of \texttt{insert} and
\texttt{deleteMin} operations are executed on the queue and the relevant
performance metrics are measured and reported.

\subsection{Hold Model}

The most commonly used model to measure a priority queue's performance is the
hold model. The objective of this model is to measure the time of
\texttt{insert} and \texttt{deleteMin} combined. This simulates a scenario where
the number of events stored in a queue converges to a steady state value. A
plausible reason that this model is widely used is that most tree-based
implementations have time complexities that are proportional to the size of the
queue. It is therefore convenient to use a model where this variable can be
easily controlled. 

\subsection{Markov Model}

In a discrete event simulator, simulation typically begins by generating a large
number of events, causing the priority queue size to steadily increase, and then
finishes by eventually deleting every event from the queue. A Markov model with
parameters $p_0$ and $p_1$ generalizes the classic hold model. By decoupling
\texttt{insert} and \texttt{deleteMin}, such a model allows \texttt{insert} to
precede a second \texttt{insert} with probability $p_0$, and \texttt{deleteMin}
to precede a second \texttt{deleteMin} with probability $p_1$. Clearly, the hold
model is a special case where $p_0=p_1=0$. This results in a slightly more
general approximation of a real-world workload, since it is not likely that each
\texttt{deleteMin} produces exactly one \texttt{insert} as is the case with the
hold model.

\subsection{Transient Model}

Distribution-sensitive priority queues, which in general include all variations
of the calendar queue, modify their internal structure as a response to shifting
input distribution. Therefore, assuming the input distribution remains stable,
it is expected that these queues reach a steady state after some initial period
of time. On the other hand, tree-based priority queues are often insensitive to
input distribution, and the latency of the \texttt{insert} and
\texttt{deleteMin} operation does not typically fluctuate as the distribution
shifts. In both the Markov and the hold model, measurements made during and
across distribution shifts are reported as a single average, which does not
reflect the transient state of the queue, but also does not reflect a meaningful
steady state. While this is not an issue for tree-based implementations, for
FlexQueue there may be a significant difference between the observed performance
at the beginning of an experiment and after this initial period of time.  For
these types of queues including FlexQueue, it is helpful to understand if and
when the data structure initiates this transient behaviour, and for how long it
persists. This can be accomplished by taking the average of a smaller set of
consecutive hold operations. For example, instead of reporting the average of
one million hold operations, these can be divided into smaller groups of 10,000,
and reporting 100 data points. In other words, by measuring over a smaller
interval, it is possible to observe, at a finer granularity, the change in
latency as a function of time.

% TODO explain that this is used in the initialization stage TODO

There is another type of transient behaviour that is best described as the
warm-up period. When a benchmark program is first loaded into memory and
execution begins, CPU cache lines are not yet populated, or contain data from
other programs running on the same CPU that may need to be evicted. Once the
benchmark has executed the \texttt{deleteMin}-\texttt{insert} loop a few times,
it is more likely that measurements will be protected from such effects.

\subsection{Increment Distributions}

The increment distribution $\mathcal{P}_{inc}$ is used to generated event
timestamps, or priorities. Given the timestamp $t$ of a previously expired
event, $\mathcal{P}_{inc}$ produces an non-negative integer $\Delta$, according
to some pre-determine distribution parameters. The timestamp of the new event is
simply $t + \Delta$. Different distribution parameters can result in vastly
distinct access patterns.

A common distribution used in bench-marking is the uniform distribution
$U(a,b)$. This distribution produces values between real numbers $a$ and $b$,
each with equal probability of $\frac{1}{b-a}$. This is a useful distribution
because in addition to its simplicity, if $a$ and $b$ are assigned the lower and
upper bound respectively of the timestamps domain, then this distribution is
effectively a random number generator. As a result, the average case latency of
a priority queue can be measured. However, such a simple distribution is
insufficient when it comes to modelling a real workload. A normal distribution
$N(\mu, \sigma^2)$ with mean $\mu$ and standard deviation $\sigma$ comes much
closer to a real workload than uniform distribution. As stated in previous
sections, the majority of events are centred around the mean in a normal
distribution, and events with larger or smaller timestamps are exponentially
less probable. This is useful in modelling workloads that exhibit temporal
locality. That is, an simulation event is more likely to trigger events that are
closer to itself than to trigger events that are more distant into the future.

While both uniform and normal distribution are useful models for workloads with
distinct characteristics, they are insufficient when the workloads becomes more
complex. A complex workload does not necessarily have a clear cluster, nor are
its events uniformly distributed. A triangular distribution Tri$(a,b)$ generates
values distributed over the interval $[a,b]$, such that the probability
increases linearly from $a$ to $b$. This distribution is useful because unlike
the normal distribution, there is clear cluster and thus can represent a
workload with mixed characteristics.

\section{Setup and Implementation}

The benchmarks are performed on the following platform:

\begin{itemize}
  \item Intel Xeon E5-4610 8-core CPU with Hyper-Threading turned on
  \item 32 KiB L1 data cache, 256 KiB L2 cache, and 16 MiB L3 cache
  \item Linux v4.13.0-38, gcc-7.2.0
  \item 256 GB memory
\end{itemize}

Regardless of the models used, the core logic of the experiment harness remains
largely identical. The harness is divided in to two major modules: harness and
test case.

\subsection{Harness}

This module is responsible for operations such as collecting input parameters,
measure run time of test cases, and printing the output to a white space
separated row suitable for use in a graphing utility such as \texttt{gnuplot}.
It contains only the most basic logic required to start a benchmark. Listing
\ref{lst:harness} shows the basic operations of the harness. Note that each data
point plotted is the average of 20 samples, as indicated by the
\texttt{num\_trials} variable in Listing \ref{lst:harness}. Results in Section
\ref{sec:results} show that majority of measurements have a relative standard
error of less than 2.3\%. That is, the true  mean is likely to be within 2.3\%
of the sample mean that is shown in subsequent figures. Occasional outliers
exist with relative standard errors up to 9\%, these are noted where
appropriate.

\begin{lstlisting}[caption={Operations of the harness},captionpos=b,label={lst:harness}]
int main() {
  const int num_trials = 20;
  const int num_repeat = 1000000;
  TrialRun time;

  // create test case from supplied name
  TestCase *testCase = tcMap.at(XSTR(TEST_NAME))();

  testCase->setUp();
  for (int j = 0; j < num_trials; j++) {
    auto start = chrono::steady_clock::now();
    testCase->run(num_repeat);
    auto end = chrono::steady_clock::now();
    auto elapsed = chrono::duration_cast<chrono::microseconds>(end - start);
    time.addResult(elapsed.count());
  }
  testCase->tearDown();
  delete testCase;

  cout << time.mean() << "\t" << time.stddev() << endl;
}

\end{lstlisting}

\subsection{Test Cases}

Test cases are collections of \texttt{C++} classes that decide the content of a
benchmark, including the model used, the queue tested and the input distribution
used. Listing \ref{lst:testcase} shows the operation of a test case that
implements the hold model using the normal distribution to test FlexQueue.

\begin{lstlisting}[caption={A FlexQueue test case},captionpos=b,label={lst:testcase}]
class FlexQueueTestCase {
  size_t _uid_counter = 0;
  FlexQueue _queue;
  Distribution *_dist;

  public:
  FlexQueueTestCase() {
    _dist = distMap[XSTR(DISTRIBUTION)]();
  }

  // populate queue to desired size
  virtual void setUp() {
    SetCurrentTime(0);
    for (size_t j = 0; j < QueueSize; j++) {
      Event e{};
      e.key.m_ts = _dist->next();
      e.key.m_uid = _uid_counter++;
      _queue.Insert(e);
    }
  }

  // begin the hold operation
  virtual void run(int repeat) {
    for (int i=0; i<repeat; i++) {
      Event e = _queue.RemoveNext();
      SetCurrentTime(e.key.m_ts);
      size_t n = _dist->next();
      e.key.m_ts = n + GetCurrentTime(); // a random interval + now
      e.key.m_uid = _uid_counter++;
      _queue.Insert(e);
    }
  }
};
\end{lstlisting}

While piece-wise and normal distribution's implementation are both available
directly through the \texttt{C++} standard library, the triangular distribution
is not. Nevertheless, it is simple to derive an equation from the definition of
triangular distribution: Tri$(a,b)$ can be generated by $a+(b-a)
\sqrt{\texttt{rand}()}$, where \texttt{rand()} generates uniformly distributed
real numbers from $[0,1]$.  Figure \ref{fig:triangular_frequency} show the
values generated by a triangular distribution with $a=10^6$ and $b=10^8$.
Assuming that event timestamps are recorded in nanoseconds, this represents a
synthetic workload where there are more events 100 ms away than there are events
1 ms away.

\begin{figure}
  \centering
  \includegraphics{figures/triangular_frequency}
  \caption{Frequency histogram of Tri$(10^6, 10^8)$}
  \label{fig:triangular_frequency}
\end{figure}


\section{Results}
\label{sec:results}

\subsection{Preliminaries}

Intuitively, setting a bit in a machine word is very fast. However, in a
hierarchical bit-vector this change must propagate to several words at higher
levels. It is not obvious if and how much overhead such an operation introduces,
compared to inserting and removing from a linked list, which is generally
considered to be $O(1)$. Thus, the objective of this experiment is to determine
the cost of operations on the hierarchical bit-vector, relative to operations on
the pointer array $A$.

In this experiment, an $N$-component array is created and its elements are
accessed both sequentially and using a randomized pointer chasing method. Such a
method effectively disables memory prefetching, and represents a maximum
difference between accessing a linked-list and accessing a hierarchical bit
vector.

For the linked-list, the array is pre-allocated to store nodes that are later
appended to the linked-list. In the sequential experiment, each array element is
accessed sequentially, and linked to the previous node using standard
linked-list operations. In the randomized pointer-chasing experiment, before
$A[i]$ is appended to the linked list, the value of $A[i]$ indicates the index
of the next element in $A$ that should be inserted. Indices are generated by
first writing sequential integers starting from 0 into $A$, then shuffled using
the \texttt{C++} standard library function \texttt{std::shuffle}.

Similarly for the bit-vector, the sequential experiment starts from the least
significant bit, change its value to 1, then moves on to the next least
significant bit and so on. In the randomized experiment however, there are no
arrays or pointers for the bit-vector to chase, therefore, instead of creating a
dedicated array, a random bit is set for each insertion.

Figure \ref{fig:overhead} shows the results of inserting sequentially into a
linked list versus a bit-vector. The total number of items inserted is
represented on the horizontal axis and the latency of insertion is represented
on the vertical axis. Multiples of $2^{18}$ are selected because FlexQueue's bit
vectors fit in to the L1 and L2 cache of the test machine's CPU. Refer to
Section \ref{sec:static_horizon} and Table \ref{table:v1v2_memory} for more
details.

\begin{table}
  \begin{center}
    \begin{tabular}{|c|l|l|}
      \hline
      Capacity (bits) & Memory Used (bytes) & Fits Into \\ \hline
      64        & 16        & L1 \\ \hline
      $2^{10}$  & 272       & L1 \\ \hline
      $2^{12}$  & 1,040       & L1 \\ \hline
      $2^{20}$  & 266,320       & L1 and L2 \\ \hline
      $2^{32}$  & 1,090,785,360       & Does not fit \\ \hline
    \end{tabular}
  \end{center}
  \caption{Total memory used by bit-vectors $V_1$ and $V_2$}
  \label{table:v1v2_memory}
\end{table}

For the bit-vector, the horizontal axis represents the capacity, rather than the
number of bits set. For sequential insertion, the cost of these two types of
data structures are very close, suggesting that operations on a bit-vector are
nearly as costly as operation on the linked list itself.  However, it is
important to note that for the linked list experiment, items are taken from a
pre-allocated array sequentially. Therefore, it benefits significantly from
cache locality and is not indicative of a real-world workload.

\begin{figure}
  \centering
  \includegraphics{figures/overhead}
  \caption{Cost of Bit Vector Operations}
  \label{fig:overhead}
\end{figure}

Indeed, the right side of Figure \ref{fig:overhead} shows the result of random
insertion, where much of this benefit for the linked list is removed.
Consequently, linked-list operations become very expensive on average due to
cache misses. In contrast, the hierarchical bit-vector only uses one bit per
element whereas the linked-list uses 64 times more memory, on a 64-bit machine.
Such space efficiency implies that the CPU cache can hold more elements from the
bit-vector compared to the linked list. As a result, the number of cache misses
are reduced significantly when accessing the bit-vector resulting in faster
average access time.

\subsection{Static Horizon}
\label{sec:static_horizon}

This is a scenario in which the bucket width $u$ is not allowed to change. To
demonstrate the consequence of not responding to changes in input distribution,
the increment distribution $\mathcal{P}_{inc}$ used is \emph{piece-wise}, such
that $x\%$ of all events are distributed evenly across a fixed horizon, while
the remaining $(100-x)\%$ are outside of the horizon and thus stored in the
overflow list. Therefore, this represents the best case performance attainable
by FlexQueue when $x=100$.  In all cases, the number of buckets is fixed at
$2^{20}$. This number is the largest power of two such that the two bit-vectors
$V_1$ and $V_2$ used by FlexQueue fit into L1 and L2 cache of the CPU in the
test environment. Table \ref{table:v1v2_memory} shows the amount of memory used
by FlexQueue's bit-vectors at various configurations.

Figure~\ref{fig:static_pct} shows the performance of FlexQueue against the set
queue under various setups. Note that on the horizontal axis in Figure
\ref{fig:static_pct_flex}, $p$ decreases from 1 to 0, whereas it increases from 0
to 1 in Figure \ref{fig:static_pct_set}. This contrast emphasizes the fact that
when $p=0$, the static setup stores all events using the overflow list, which
results in similar performance as SetQueue in Figure \ref{fig:static_pct_set}.
In addition, when the queue size is relatively small at $2^{18}$ and as the
percentage of events in $A$ approaches 0\%, the average hold time decreases
slightly. This implies that set queue is slightly more efficient at managing
small sets of events. In contrast, as the queue size increases, it becomes more
expensive to use set queue, while the cost of FlexQueue does not increase as
significantly.  It is interesting to note that the set queue also responds to
distribution shift.  Upon investigation, this is because the piece-wise
distribution used in this experiment uses the interval $[0, 2^{20}]$ and
$(2^{20}, 2^{32}-1]$ with probability $p$ and $1-p$ respectively. That is,
events outside of the horizon is distributed over a much larger interval than
those within the horizon. As the total number of events outside the horizon
grows, they occupy more and more non-leaf nodes in a balanced binary tree, such
as the set queue. As a result, the remaining events within the horizon are
pushed into a small sub-tree, while the rest of the events belong to a much
larger sub-tree, with a higher access time. Since these events are always
dequeued before the others, effectively the working size, or height, of the
whole tree is reduced.  However, even with this unintended advantage, set queue
does not outperform FlexQueue. This is especially obvious when the queue size is
large.  When $p=0$, both figures are effectively representing the same data
structure, and thus converge. Note that the measurements for set queue have
relatively large standard errors for $0.6 < p \leq 1$, this is possibly because
as $p$ approaches 0.5, it becomes more likely that both sub-trees are accessed.
Since the access time of these two sub-trees differ, measurements across these
ranges of $p$ values are no longer stable.

\begin{figure}
  \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/piecewise_static_flex_p}
    \caption{FlexQueue static}
    \label{fig:static_pct_flex}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/piecewise_static_set_p}
    \caption{SetQueue}
    \label{fig:static_pct_set}
  \end{subfigure}
  \caption{Time vs. $p$ under various $q$}
  \label{fig:static_pct}
\end{figure}

Figure~\ref{fig:static_qsize} shows that as long as most events are stored
within the pointer array, the average hold time does not noticeably increase as
queue size increases. However, when the number of events is greater than the
number of buckets, further increasing the queue size does still increase the
average access time of FlexQueue. Intuitively, since $N=20$, on average each
bucket, implemented with a set queue as well, holds a constant factor of
$2^{20}$ fewer events  compared to the single set queue. When the number of
events is less than $2^{20}$, there exists one or more empty bucket. Thus,
increasing the queue size merely fills those empty buckets, without increasing
the average latency.

\begin{figure}
  \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/piecewise_static_flex_queue_size}
    \caption{FlexQueue static}
    \label{fig:static_qsize_flex}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/piecewise_static_set_queue_size}
    \caption{SetQueue}
    \label{fig:static_qsize_set}
  \end{subfigure}
  \caption{Time vs. queue size under various $p$}
  \label{fig:static_qsize}
\end{figure}

\subsection{Dynamic Horizon}

Figure \ref{fig:dynamic_horizon} shows the result of enabling the resizing
policy. When the number of events is $2^{18}$ and $p=0$, the resizing policy
estimates the mean and standard deviation of $\mathcal{P}_{inc}$ and attempts to
resize to a smaller horizon. However, since the bucket width can be no less than
1, FlexQueue does not make any changes to the horizon size. Notice that compared
to the static scenario, the hold time no longer briefly increases up to
$p=0.8$. This is the result of the resizing policy modifying the boundaries of
the horizon. As more events are shifted from $A$ to $L$, the mean of  event
timestamps increases. FlexQueue senses such a shift and ``tracks" it by
increasing the lower bound of the horizon. As a result, hold time remains
relatively flat from $p=1$ to $p=0$.

Notice that since the resizing policy is designed to minimize the negatives of a
biased input distribution, it has no effect on nor is it impacted by the queue
size. As Figure \ref{fig:dynamic_horizon_queue_size} shows, employing a dynamic
horizon does not improve FlexQueue's efficiency when every bucket needs to store
more events on average.

\begin{figure}
  \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/piecewise_dynamic_flex_p}
    \caption{as a function of $p$}
    \label{fig:dynamic_horizon_p}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/piecewise_dynamic_flex_queue_size}
    \caption{as a function of $q$}
    \label{fig:dynamic_horizon_queue_size}
  \end{subfigure}
  \caption{Effect of dynamic horizon}
  \label{fig:dynamic_horizon}
\end{figure}

\subsection{Determining the Value of $k$}

Briefly recall $k$ from Section \ref{sec:intro_k}, the appropriate value of $k$
may differ depending on the input distribution. For example, for a highly
clustered distribution such as a normal distribution with small variance, $k$
may need to be large in order to effectively make use of the buckets in $A$. On
the other hand, a large $k$ increases the difference between the size of the
largest and smallest bucket in $A$. This causes the distribution of events in
$A$ to approach the actual workload distribution, which is presumably skewed.
Section \ref{sec:intro_k} states that the following must be satisfied in order
for $L$ and each bucket in $A$ to have the same size:

\begin{align*}
  \erf\left(\frac{k}{\sqrt{2}}\right) &= \frac{N}{N+1}.
\end{align*}

Since \texttt{erf} is an increasing function, it is possible to approximate the
value of $k$ using binary search by setting lower bound to 1 and upper bound to
a sufficiently large value such as $10^8$. When $N=2^{20}$ as is the case with
previous experiments, $k$ is estimated to be approximately 4.90096. Figure
\ref{fig:value_of_k} shows the effect of $k$ on a normal distribution benchmark
with various standard deviations. Clearly, when the majority of events are
clustered as is the case with $\sigma=10000$, varying $k$ does not help. This is
because when $k$ is small, a large percentage of events reside in $L$; when $k$
is large, a small number of buckets in $A$ hold a majority of the events. Since
bucket is also implemented using a set queue similar to $L$, regardless of $k$,
in both cases the average latency is dominated by the performance of the set
queue.

\begin{figure}
  \centering
  \includegraphics{figures/value_of_k}
  \caption{Effect of $k$}
  \label{fig:value_of_k}
\end{figure}

On the other hand, when $\sigma$ is large, i.e. the events are more scattered,
then increasing $k$ may marginally reduce the latency, by bringing more
events into every bucket in $A$ instead of just a few when then events are
tightly clustered. There is no single value of $k$ that is optimal for
all distributions, and this experiment suggests that a large default value
such as 6 is safer than a small value such as 2. While a large $k$ does not
necessarily reduce access time, it is reasonable to assume that it does not
increase access time either.

\subsection{Other Distributions}

Previous benchmarks employs a basic piece-wise uniform distribution that is
unrealistic for a real workload. This section compares FlexQueue against
competing implementations under synthetic, yet more typical of real world, input
distributions. These include normal and triangular distributions.

Notably, in Figure~\ref{fig:competitors}, set queue performs almost identical to
FlexQueue. This is because a normal distribution with a small variance is used.
As such, a majority of the events are centred around the mean, therefore, it
becomes difficult to choose a value of $k$ such that $2k$ standard deviations of
events can be distributed across all buckets evenly when the queue size is only
slightly larger than the number of buckets. This reduces the effectiveness of
the pointer array and increases the average latency of accessing $A$.  It is
therefore expected that FlexQueue should have better performance if the input
distribution has several clumps rather than just one. 

The triangular distribution in Figure \ref{fig:competitors} is similar to the
result of adding multiple normal distributions with unique means. Such a
distribution results in multiple, closely-spaced clusters of values rather than
just a single cluster, and gives FlexQueue the opportunity to make use of the
pointer array over a much larger range of events.

\begin{figure}
  \centering
  \includegraphics[scale=1]{figures/competitors}
  \caption{FlexQueue vs competitors}
  \label{fig:competitors}
\end{figure}

\section{\texttt{ns3} Application}

\texttt{ns3} \cite{zotero-2} is an open-source, discrete event network
simulator. It is particularly good at packet level simulation and creating
models that are realistic enough that simulation results are considered very
realistic.  According to its documentation, the priority queue used as the
scheduler is an STL set queue, which is implemented using a red-black tree.
FlexQueue works as a drop-in replacement for \texttt{ns3}'s scheduler with no
changes to the source code.

Included in the source code repository are workloads that can be readily
compiled and executed. One of these workloads, '\texttt{ripng-network}' creates
a small topology with 6 nodes and 7 links between them. This workload is
intended to simulate the operation of \ac{ripng} by repeatedly disconnecting and
reconnecting one of the low-cost links between two subnets. This causes the
protocol to re-route traffic through a different, high-cost link that is
otherwise unused. By increasing the number of copies $t$ of this topology
created during simulation, it is possible to observe the performance of
FlexQueue under various queue sizes and determine whether its performance
benefits carry over to a real world application.

Figure \ref{fig:ns3_ripng} shows the running time of the \texttt{ripng-network}
simulation.
\begin{figure}
  \centering
  \includegraphics{figures/ns3_ripng}
  \caption{\texttt{ripng-network} topology using different scheduler}
  \label{fig:ns3_ripng}
\end{figure}
As expected, FlexQueue does not out-perform set queue at smaller
queue sizes, and is noticeably slower at these parameters. Note that the
smallest queue sizes here are much smaller than those during benchmarks. This
shows that FlexQueue does not introduce significant overhead, even under
parameters that it is not optimized for. By increasing the number of nodes used
in the simulation,  the number of queued events increases, and approaches the
range of values that FlexQueue excels at, as suggested by previous benchmarks.
At $t=300$, FlexQueue's simulation run time is roughly 2\% faster than that of
set queue. Using the \texttt{gprof} profiler, it is discovered that priority
queue related operations such as \texttt{insert} and \texttt{deleteMin} account
for roughly 10\% of the entire \texttt{ripng-network} simulation run time. Thus,
this result translates to roughly a 20\% improvement over set queue for
priority queue operations only, since all other components of the simulation
remains identical.

