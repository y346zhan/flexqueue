set terminal epslatex color

set size 0.8,0.8
set output "figures/".filename.".tex"
plot "data/".filename.".data" using 1:2 title 'Linked List' with linespoint, '' using 1:4 title 'HBitmap' with linespoint
