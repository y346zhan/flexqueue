% !TEX root = main.tex
% !TEX encoding = UTF-8 Unicode

\chapter{Introduction}

A \emph{priority queue} is a data structure that manages a set of key-value
pairs, in a way that allows efficient retrieval of the value associated with the
smallest key.  It is used in many different fields including runtime timer
management, discrete event simulation, and memory allocation. In all cases, the
efficiency of this data structure is crucial as it is often the most accessed
structure in the system \cite{comfort1984}. The need for a high performance
priority queue has driven numerous design proposals published over the last few
decades. 

Among these proposals roughly two research directions can be identified. The
first line of research focuses on improving the theoretical bound of canonical
operations, such as \texttt{insert} and \texttt{deleteMin}, which includes
tree-based algorithms such as skew heap, binomial heap, and Fibonacci heap. The
second direction of research takes into account the characteristics of the input
workload and uses this information to dynamically adjust the internal structure.
Algorithms in this category often achieve $O(1)$ or amortized $O(1)$ cost for
\texttt{insert} and \texttt{deleteMin} in practice and are generally list-based,
such as the two-list procedure \cite{blackstone1981}, timing wheel
\cite{varghese1987}, and calendar queue \cite{brown1988}. However, while
improved performance is appreciated, it often comes with trade-offs in terms of
complexity and predictability. Novel but complicated design is typically
introduced by a heuristic that makes better estimation of workload
characteristics, but predictability is sacrificed when memory allocation and
self-adjustments, in response to a skewed input distribution, are deferred to an
unknown point of time in the future.  Some of these trade-offs may be more
favourable to one type of application than others. For example, in a scheduler
for a hard real-time system, a predictable worst-case latency is extremely
valuable as this guarantees that operations can be completed within a stringent
timeline. In contrast, applications like discrete event simulation can work with
amortized complexity and a more relaxed worst-case requirement.

This thesis presents an overview of existing priority queue implementations and
discusses their strengths and weaknesses in various applications.  Moreover, a
different trade-off of complexity and performance is explored through the
development and evaluation of a new priority queue design called
\emph{FlexQueue} that performs on par with state-of-the-art implementations,
while using only a straightforward heuristic and not requiring dynamic memory
allocation.  This is especially useful, for example, in an operating system
kernel or a runtime system.  In these systems, intrusive data structures are
common because \texttt{insert} and \texttt{deleteMin} operations do not result
in memory allocation or release.  To evaluate the performance of FlexQueue, it
is benchmarked against several popular tree-based and list-based implementations
using the classic \emph{hold} model with \emph{piece-wise} distribution,
\emph{normal} distribution, and \emph{triangular} distribution. Results show
that FlexQueue outperforms several tree-based implementations including
Fibonacci heap and red-black tree.  Finally, this thesis modifies a popular
network simulator, \texttt{ns3}, and replaces its priority queue structure with
FlexQueue. A simulator such as \texttt{ns3} represents a scenario for which
FlexQueue is not optimized because dynamic memory allocation is implied by
\texttt{ns3}'s source code interface, and amortized overhead is acceptable.
Therefore, good performance in this case indicates that FlexQueue is also
suitable for more restricted scenarios, for example, when being used inside an
operating system kernel.

\section{Problem Statement}

If $P$ is a priority queue, then at minimum the following operations are
defined:

\begin{itemize}

  \item \texttt{insert($P,k,v$)} Adds the key-value pair $(k, v)$ to $P$.

  \item \texttt{deleteMin($P$)} Returns and deletes the key-value pair with the
    smallest key.

  \item \texttt{delete($P,k$)} If $k$ exists in $P$, then remove it and its
    associated value.

\end{itemize}

In addition to the above, extra operations are defined for priority queues that
are used in solving problems such as Dikjstra's shortest path algorithm
\cite{dijkstra1959} \cite{fredman1987} and minimum spanning tree
\cite{tarjan1983}.

\begin{itemize}

  \item \texttt{decreaseKey($P,k,k'$)} If key $k$ exists in $P$, then modify it
    to $k'$. This is functionally equivalent to \texttt{delete} followed by a
    \texttt{insert} and does not change the associated value.

  \item \texttt{merge($P_1$,$P_2$)} Returns a new priority queue containing all
    elements from both $P_1$ and $P_2$. This is functionally equivalent to a
    sequence of alternating \texttt{deleteMin} and \texttt{insert}.

\end{itemize}

Note that although these compound operations can be implemented using the three
basic primitives above, certain designs are more efficient at supporting them.
That is, \texttt{decreaseKey} can be more efficient compared to a
\texttt{delete} followed by an \texttt{insert}.  For this thesis, the primary
target scenario is runtime systems in which these operations are not relevant.
Therefore, the priority queue design needs to focus only on the basic
operations.

As previously mentioned, there are two important trade-offs to consider when
designing a priority queue: complexity and predictability. Complexity can be
traded for better performance but this trade-off is often not proportional. That
is, it can be questionable whether a marginal reduction in time complexity is
worth the additional effort required to implement it correctly. There are simple
tree-based data structures that offer worst-case complexity of $O(\lg n)$, while
more sophisticated designs can achieve $O(\lg \lg n)$ or amortized $O(1)$ time
complexity.  The proposed priority queue should thus strike a balance between
the two, while ensuring worst case is at least $O(\lg n)$. On the other hand,
predictability can be traded for better memory utilization. With dynamic memory
allocation, only the minimum amount of memory is required to manage queue
objects. However, the assumption that memory allocation is $O(1)$ is not always
true or reasonable. For example, a system can always run out of memory causing
system calls such as \texttt{mmap} to fail. Therefore, it can be desirable for
the priority queue to allocate required memory ahead of time, while making use
of intrusive data structures to eliminate the need for dynamic memory allocation
during operations.

The rest of the thesis is organized as follows. Chapter 2 gives an overview of
the past research efforts on priority queues and identifies the primary
competitors of FlexQueue. Chapter 3 details the design of FlexQueue and the
rationale of the choices involved. Chapter 4 presents the experimental setup
and evaluation of design. Chapter 5 concludes the thesis.
