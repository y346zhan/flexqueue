#!/bin/zsh
cd "src"

printf "\"BinomialQueue\"\n"
for k in {18..24}; do
  make -sB TEST_PARAMS="-DTEST_NAME=BinomialQueueTestCase\
			-DDISTRIBUTION=normal\
                        -DMEAN=10000000\
                        -DSTDDEV=300000\
                        -DQUEUE_SIZE=$(( 2**k ))\
			-DX_VAR=$(( 2**k ))\
                        -DX_LABEL='queue size'\
			" hold
  ./hold
done
printf "\n\n"

printf "\"FibQueue\"\n"
for k in {18..24}; do
  make -sB TEST_PARAMS="-DTEST_NAME=FibQueueTestCase\
			-DDISTRIBUTION=normal\
                        -DMEAN=10000000\
                        -DSTDDEV=300000\
                        -DQUEUE_SIZE=$(( 2**k ))\
			-DX_VAR=$(( 2**k ))\
                        -DX_LABEL='queue size'\
			" hold
  ./hold
done
printf "\n\n"

printf "\"SetQueue\"\n"
for k in {18..24}; do
  make -sB TEST_PARAMS="-DTEST_NAME=SetQueueTestCase\
			-DDISTRIBUTION=normal\
                        -DMEAN=10000000\
                        -DSTDDEV=300000\
                        -DQUEUE_SIZE=$(( 2**k ))\
			-DX_VAR=$(( 2**k ))\
                        -DX_LABEL='queue size'\
			" hold
  ./hold
done
printf "\n\n"

printf "\"FlexQueue\"\n"
for k in {18..24}; do
  make -sB TEST_PARAMS="-DTEST_NAME=FlexQueueTestCase\
			-DDISTRIBUTION=normal\
                        -DMEAN=10000000\
                        -DSTDDEV=300000\
                        -DQUEUE_SIZE=$(( 2**k ))\
			-DX_VAR=$(( 2**k ))\
                        -DX_LABEL='queue size'\
			" hold
  ./hold
done
printf "\n\n"

