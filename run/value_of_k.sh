#!/bin/zsh
cd "src"

for power in {4..6}; do
  let "stddev = 10 ** power"
  printf "\"stddev=10^%d\"\n" $power
  for span in $(seq 1.0 0.5 6.0); do
    make -sB TEST_PARAMS="-DTEST_NAME=FlexQueueTestCase\
                          -DQUEUE_SIZE=$(( 2**24 ))\
                          -DDISTRIBUTION=normal\
                          -DMEAN=10000000\
                          -DSTDDEV=$stddev\
                          -DSPAN=$span\
                          -DX_VAR=$span\
                          -DX_VAR_LABEL=k\
                          " hold
    ./hold
  done
  printf "\n\n"
done
