#!/bin/zsh
cd "src"

for k in {10..30}; do
  make -sB TEST_PARAMS="-DTEST_NAME=FanoutEffectTestCase\
    -DBASE_WIDTH=64\
    -DDISTRIBUTION=uniform\
    -DHORIZON=$(( 2**k ))\
    -DBIT_COUNT=$(( 2**k ))\
    -DX_VAR=$(( 2**k ))\
    -DX_LABEL='C'\
    " hold
  ./hold
done

