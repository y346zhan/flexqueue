#!/bin/zsh
cd "src"

for q in {0..2}; do
  let "qSizeLog = 24 - 3 * q"
  printf "\"q=2^{%d}\"\n" $qSizeLog
  for p in {0..10}; do
    let "prob = 100 - p * 10" # 100, 90, ..., 0
    make -sB TEST_PARAMS="-DTEST_NAME=FlexQueueTestCase\
                          -DDISTRIBUTION=piecewise\
                          -DQUEUE_SIZE=$(( 2**qSizeLog ))\
                          -DBUCKET_PERCENT=$prob\
                          -DX_VAR=$prob\
                          -DX_LABEL=o_max\
                          " hold
  ./hold
  done
  printf "\n\n"
done
