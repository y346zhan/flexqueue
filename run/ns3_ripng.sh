#!/bin/zsh
cd "src/bin"

printf "\"BinomialQueue\"\n"
for i in {1..10}; do
  ./ripng-binomial -t=1 -n=50
done
for t in {1..10}; do
  for i in {1..10}; do
    ./ripng-binomial -t=$((t*10)) -n=50
  done
done
for i in {1..10}; do
  ./ripng-binomial -t=200 -n=50
done
for i in {1..10}; do
  ./ripng-binomial -t=300 -n=50
done
printf "\n\n"

printf "\"FibQueue\"\n"
for i in {1..10}; do
  ./ripng-fib -t=1 -n=50
done
for t in {1..10}; do
  for i in {1..10}; do
    ./ripng-fib -t=$((t*10)) -n=50
  done
done
for i in {1..10}; do
  ./ripng-fib -t=200 -n=50
done
for i in {1..10}; do
  ./ripng-fib -t=300 -n=50
done
printf "\n\n"

printf "\"SetQueue\"\n"
for i in {1..10}; do
  ./ripng-set -t=1 -n=50
done
for t in {1..10}; do
  for i in {1..10}; do
    ./ripng-set -t=$((t*10)) -n=50
  done
done
for i in {1..10}; do
  ./ripng-set -t=200 -n=50
done
for i in {1..10}; do
  ./ripng-set -t=300 -n=50
done
printf "\n\n"

printf "\"FlexQueue\"\n"
for i in {1..10}; do
  ./ripng-flex -t=1 -n=50
done
for t in {1..10}; do
  for i in {1..10}; do
    ./ripng-flex -t=$((t*10)) -n=50
  done
done
for i in {1..10}; do
  ./ripng-flex -t=200 -n=50
done
for i in {1..10}; do
  ./ripng-flex -t=300 -n=50
done
printf "\n\n"

