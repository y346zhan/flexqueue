#!/bin/zsh
cd "src"

for k in {6..11}; do
  make -sB TEST_PARAMS="-DTEST_NAME=FanoutEffectTestCase\
    -DBASE_WIDTH=$(( 2**k ))\
    -DDISTRIBUTION=uniform\
    -DHORIZON=$(( 2**32 ))\
    -DBIT_COUNT=$(( 2**32 ))\
    -DX_VAR=$(( 2**k ))\
    -DX_LABEL='w'\
    " hold
  ./hold
done

