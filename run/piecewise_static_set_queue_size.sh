#!/bin/zsh
cd "src"

for i in {0..2}; do
  let "prob = 100 - i * 40"
  let "p = 1.0 - i * 0.4"
  printf "\"p=%.1f\"\n#SetQueue" $p
  for j in {0..11}; do
    let "qSizeLog = 14 + j"
    make -sB TEST_PARAMS="-DTEST_NAME=SetQueueTestCase\
                          -DNO_RESIZE\
                          -DQUEUE_SIZE=$(( 2**qSizeLog ))\
                          -DDISTRIBUTION=piecewise\
                          -DBUCKET_PERCENT=$prob\
                          -DX_VAR=$((2**qSizeLog))\
                          -DX_LABEL='queue size'\
                          " hold
    ./hold
  done
  printf "\n\n"
done
