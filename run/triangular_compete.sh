#!/bin/zsh
cd "src"

printf "\"BinomialQueue\"\n"
for k in {18..24}; do
  make -sB TEST_PARAMS="-DTEST_NAME=BinomialQueueTestCase\
			-DDISTRIBUTION=triangular\
			-DTRI_A=1000000\
			-DTRI_B=100000000\
                        -DQUEUE_SIZE=$(( 2**k ))\
			-DX_VAR=$(( 2**k ))\
			-DX_LABEL='queue size'\
			" hold
  ./hold
done
printf "\n\n"

printf "\"FibQueue\"\n"
for k in {18..24}; do
  make -sB TEST_PARAMS="-DTEST_NAME=FibQueueTestCase\
			-DDISTRIBUTION=triangular\
			-DTRI_A=1000000\
			-DTRI_B=100000000\
			-DDISTRIBUTION=triangular\
                        -DQUEUE_SIZE=$(( 2**k ))\
			-DX_VAR=$(( 2**k ))\
			-DX_LABEL='queue size'\
			" hold
  ./hold
done
printf "\n\n"

printf "\"SetQueue\"\n"
for k in {18..24}; do
  make -sB TEST_PARAMS="-DTEST_NAME=SetQueueTestCase\
			-DDISTRIBUTION=triangular\
			-DTRI_A=1000000\
			-DTRI_B=100000000\
                        -DQUEUE_SIZE=$(( 2**k ))\
			-DX_VAR=$(( 2**k ))\
			-DX_LABEL='queue size'\
			" hold
  ./hold
done
printf "\n\n"

printf "\"FlexQueue\"\n"
for k in {18..24}; do
  make -sB TEST_PARAMS="-DTEST_NAME=FlexQueueTestCase\
			-DDISTRIBUTION=triangular\
			-DTRI_A=1000000\
			-DTRI_B=100000000\
                        -DQUEUE_SIZE=$(( 2**k ))\
			-DX_VAR=$(( 2**k ))\
			-DX_LABEL='queue size'\
			" hold
  ./hold
done
printf "\n\n"

