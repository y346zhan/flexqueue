#!/bin/zsh
# Each script should produce data for only one figure.
# Use of multiplot is discouraged because it is difficult to reference such
# figures in LaTeX.
HOST=$(hostname)

run/fanout_effect_c.sh | tee ~/thesis/data/$HOST/raw/fanout_effect_c.dat
run/fanout_effect_f.sh | tee ~/thesis/data/$HOST/raw/fanout_effect_f.dat

run/piecewise_static_flex_p.sh | tee ~/thesis/data/$HOST/raw/piecewise_static_flex_p.dat
run/piecewise_static_set_p.sh | tee ~/thesis/data/$HOST/raw/piecewise_static_set_p.dat

run/piecewise_static_flex_queue_size.sh | tee ~/thesis/data/$HOST/raw/piecewise_static_flex_queue_size.dat
run/piecewise_static_set_queue_size.sh | tee ~/thesis/data/$HOST/raw/piecewise_static_set_queue_size.dat

run/piecewise_dynamic_flex_p.sh | tee ~/thesis/data/$HOST/raw/piecewise_dynamic_flex_p.dat
run/piecewise_dynamic_flex_queue_size.sh | tee ~/thesis/data/$HOST/raw/piecewise_dynamic_flex_queue_size.dat

run/value_of_k.sh | tee ~/thesis/data/$HOST/raw/value_of_k.dat
run/normal_compete.sh | tee ~/thesis/data/$HOST/raw/normal_compete.dat
run/triangular_compete.sh | tee ~/thesis/data/$HOST/raw/triangular_compete.dat
