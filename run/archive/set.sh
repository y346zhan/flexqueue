#!/bin/zsh
cd "src"
test_number=0

while getopts "n:" opt; do
  case "$opt" in
    n) test_number=$OPTARG
      ;;
    \?) exit 1
      ;;
  esac
done

# to test if STL set's speed is affected by the range of keys
if [[ test_number -eq 1 ]]; then
  for i in {20..64..2}; do
    printf "\"\$h=$i\$\"\n"
    make -B TEST_PARAMS="-DTEST_NAME=SetQueueTestCase -DQUEUE_SIZE=$(( 2**20 )) -DDISTRIBUTION=uniform -DHORIZON=$(( 2**i ))" steadystate
    taskset -c 4 ./steadystate
    printf "\n\n"
  done
fi

# to test if STL set's speed is affected using simplified PCD
if [[ test_number -eq 2 ]]; then
  for i in {100..0..20}; do
    printf "\"\$p=$i\$\"\n"
    make -sB TEST_PARAMS="-DTEST_NAME=SetQueueTestCase -DQUEUE_SIZE=$(( 2**20 )) -DDISTRIBUTION=piecewise_simple -DHORIZON=$(( 2**30 )) -DBUCKET_PERCENT=$i" steadystate
    taskset -c 4 ./steadystate
    printf "\n\n"
  done
fi

# to test if STL set's speed is affected using STL PCD
if [[ test_number -eq 3 ]]; then
  for i in {100..0..20}; do
    printf "\"\$p=$i\$\"\n"
    make -sB TEST_PARAMS="-DTEST_NAME=SetQueueTestCase -DQUEUE_SIZE=$(( 2**20 )) -DDISTRIBUTION=piecewise -DHORIZON=$(( 2**30 )) -DBUCKET_PERCENT=$i" steadystate
    taskset -c 4 ./steadystate
    printf "\n\n"
  done
fi

