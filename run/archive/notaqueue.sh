#!/bin/zsh
cd "src"
test_number=0

while getopts "n:" opt; do
  case "$opt" in
    n) test_number=$OPTARG
      ;;
    \?) exit 1
      ;;
  esac
done

# to confirm that Mersenne Twister engine's speed is not affected by the range of values to 
# select from
if [[ test_number -eq 1 ]]; then
  for i in {20..44..2}; do
    printf "\"\$h=2^{$i}\$\"\n"
    make -sB TEST_PARAMS="-DTEST_NAME=NotAQueueTestCase -DQUEUE_SIZE=$(( 2**20 )) -DDISTRIBUTION=uniform -DHORIZON=$(( 2**i ))" steadystate
    taskset -c 4 ./steadystate
    printf "\n\n"
  done
fi

# to demonstrate that without queue operations, p has no effect on run time
if [[ test_number -eq 2 ]]; then
  for i in {100..0..20}; do
    printf "\"\$p=$i\$\"\n"
    make -sB TEST_PARAMS="-DTEST_NAME=NotAQueueTestCase -DQUEUE_SIZE=$(( 2**20 )) -DDISTRIBUTION=piecewise -DHORIZON=$(( 2**30 )) -DBUCKET_PERCENT=$i" steadystate
    taskset -c 4 ./steadystate
    printf "\n\n"
  done
fi

# to test if simplified pcd is still affected by p
# -> no, not affected
if [[ test_number -eq 3 ]]; then
  for i in {100..0..20}; do
    printf "\"\$p=$i\$\"\n"
    make -sB TEST_PARAMS="-DTEST_NAME=NotAQueueTestCase -DQUEUE_SIZE=$(( 2**20 )) -DDISTRIBUTION=piecewise_simple -DHORIZON=$(( 2**30 )) -DBUCKET_PERCENT=$i" steadystate
    taskset -c 4 ./steadystate
    printf "\n\n"
  done
fi
