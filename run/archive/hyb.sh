#!/bin/bash
cd "src"
#
# Before using the -S option, remember to modify hybridq.h accordingly
# ^ No longer necessary with the addition of -DNO_ARR
hybridq_hold_vs_bwidth=0
hybridq_hold_vs_dist=0
hybridq_hold_vs_qsize=0
bitmapq_hold_vs_qsize=0
hybridq_hold_vs_horizon=0
bitmapq_hold_vs_horizon=0
make_target="hold"

while getopts "wdsShHt:" opt; do
  case "$opt" in
    w) hybridq_hold_vs_bwidth=1
      ;;
    d) hybridq_hold_vs_dist=1
      ;;
    s) hybridq_hold_vs_qsize=1
      ;;
    h) hybridq_hold_vs_horizon=1
      ;;
    S) bitmapq_hold_vs_qsize=1
      ;;
    H) bitmapq_hold_vs_horizon=1
      ;;
    t) make_target=$OPTARG
      ;;
  esac
done

# vs horizon [2^24..2^12]
# density = 1/2
# width = [64..1024]
# Varies # of levels by varying horizon
if [[ bitmapq_hold_vs_horizon -eq 1 ]]; then
  for w in {0..4}; do
    let "numBits = 64 * (2 ** w)"
    echo "\"\$w=$numBits\$\""
    for hz in {27..12}; do
      let "qSizeLog = hz - 1"
      make -sB TEST_PARAMS="-DTEST_NAME=BitmapqHoldTestCase -DBASE_WIDTH=$numBits -DQUEUE_SIZE_LOG=$qSizeLog -DHORIZON=$hz" $make_target
      ./$make_target
    done
    echo "\n"
  done
fi

# VS. queue size = [2^24..2^29]
# horizon = 2^30
# width = [64,128,256,512]
# no bucket array
if [[ bitmapq_hold_vs_qsize -eq 1 ]]; then
  for w in {0..3}; do
    let "numBits = 64 * (2 ** w)"
    echo "\"\$w=$numBits\$\""
    for q in {0..7}; do
      let "qSizeLog = 27 + q"
      make -sB TEST_PARAMS="-DTEST_NAME=BitmapqHoldTestCase -DBASE_WIDTH=$numBits -DQUEUE_SIZE_LOG=$qSizeLog -DHORIZON=36" $make_target
      ./$make_target
    done
    echo "\n"
  done
fi

# vs horizon [2^27..2^12]
# density = 1/2
# width = [64..1024]
# Varies # of levels by varying horizon
if [[ hybridq_hold_vs_horizon -eq 1 ]]; then
  for w in {0..4}; do
    let "numBits = 64 * (2 ** w)"
    echo "\"\$w=$numBits\$\""
    for hz in {27..12}; do
      let "qSizeLog = hz - 1"
      make -sB TEST_PARAMS="-DTEST_NAME=HybridHoldTestCase -DBASE_WIDTH=$numBits -DQUEUE_SIZE_LOG=$qSizeLog -DHORIZON=$hz -DBUCKET_PERCENT=100" $make_target
      ./$make_target
    done
    echo "\n"
  done
fi

# VS. width [64..2048]
# horizon = 2^30
# density = [1/16..1]
# Varies # of levels by varying the base width
if [[ hybridq_hold_vs_bwidth -eq 1 ]]; then
  for q in {0..4}; do
    let "qSizeLog = 26 + q"
    echo "\"\$hz=2^{30}, q=2^{$qSizeLog}\$\""
    for w in {0..5}; do
      let "numBits = 64 * (2 ** w)"
      make -sB TEST_PARAMS="-DTEST_NAME=HybridHoldTestCase -DBASE_WIDTH=$numBits -DQUEUE_SIZE_LOG=$qSizeLog -DHORIZON=30 -DBUCKET_PERCENT=100" $make_target
      ./$make_target
    done
    echo "\n"
  done
fi


# VS. distribution
# distribution = [100..0]
# horizon = 2^30
# width = [64..256]
# queue size = [2^20..2^26]
if [[ hybridq_hold_vs_dist -eq 1 ]]; then
  for w in {0..2}; do
    let "numBits = 64 * (2 ** w)" # 64, 128, 256
    for q in {0..2}; do
      let "qSizeLog = 20 + 3 * q" # 2**18, 2**21, 2**24
      echo "\"\$q=2^{$qSizeLog}, w=$numBits\$\""
      for p in {0..10}; do
        let "prob = 100 - p * 10" # 100, 90, ..., 0
        make -sB TEST_PARAMS="-DTEST_NAME=HybridHoldTestCase -DBASE_WIDTH=$numBits -DQUEUE_SIZE_LOG=$qSizeLog -DHORIZON=30 -DBUCKET_PERCENT=$prob" $make_target
        ./$make_target
      done
      echo "\n"
    done
  done
fi

# VS. queue size
# queue size = [2^14..2^25]
# horizon = 2^30
# width = [64..256]
# distribution = [100,60,20]
if [[ hybridq_hold_vs_qsize -eq 1 ]]; then
  #print "Running Time vs. queue size..."
  for w in {0..2}; do
    let "numBits = 64 * (2 ** w)"
    for p in {0..2}; do
      let "prob = 100 - p * 40"
      echo "\"\$p=$prob, w=$numBits\$\""
      for q in {0..11}; do
        let "qSizeLog = 14 + q"
        make -sB TEST_PARAMS="-DTEST_NAME=HybridHoldTestCase -DBASE_WIDTH=$numBits -DQUEUE_SIZE_LOG=$qSizeLog -DHORIZON=30 -DBUCKET_PERCENT=$prob" $make_target
        ./$make_target
      done
      echo "\n"
    done
  done
fi

