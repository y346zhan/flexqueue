#!/bin/zsh
cd "src"
#
# Before using the -S option, remember to modify hybridq.h accordingly
# ^ No longer necessary with the addition of -DNO_ARR
hybridq_hold_vs_dist=0
hybridq_hold_vs_qsize=0

while getopts "wdsShH" opt; do
  case "$opt" in
    d) hybridq_hold_vs_dist=1
      ;;
    s) hybridq_hold_vs_qsize=1
      ;;
  esac
done

# VS. distribution
# distribution = [100..0]
# horizon = 2^30
# width = [64..256]
# queue size = [2^20..2^26]
if [[ hybridq_hold_vs_dist -eq 1 ]]; then
  for w in {0..2}; do
    let "numBits = 64 * (2 ** w)" # 64, 128, 256
    for q in {0..2}; do
      let "qSizeLog = 20 + 3 * q" # 2**18, 2**21, 2**24
      echo "\"\$q=2^{$qSizeLog}, w=$numBits\$\""
      for p in {0..10}; do
        let "prob = 100 - p * 10" # 100, 90, ..., 0
        make -sB TEST_PARAMS="-DTEST_NAME=NS3CalendarHoldTestCase -DQUEUE_SIZE_LOG=$qSizeLog -DHORIZON=30 -DBUCKET_PERCENT=$prob" hold
        ./hold
      done
      echo "\n"
    done
  done
fi

# VS. queue size
# queue size = [2^14..2^25]
# horizon = 2^30
# width = [64..256]
# distribution = [100,60,20]
if [[ hybridq_hold_vs_qsize -eq 1 ]]; then
  #print "Running Time vs. queue size..."
  for w in {0..2}; do
    let "numBits = 64 * (2 ** w)"
    for p in {0..2}; do
      let "prob = 100 - p * 40"
      echo "\"\$p=$prob, w=$numBits\$\""
      for q in {0..11}; do
        let "qSizeLog = 14 + q"
        make -sB TEST_PARAMS="-DTEST_NAME=NS3CalendarHoldTestCase -DQUEUE_SIZE_LOG=$qSizeLog -DHORIZON=30 -DBUCKET_PERCENT=$prob" hold
        ./hold
      done
      echo "\n"
    done
  done
fi

