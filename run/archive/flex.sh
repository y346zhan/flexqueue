#!/bin/zsh
cd "src"
test_number=0

while getopts "n:" opt; do
  case "$opt" in
    n) test_number=$OPTARG
      ;;
    \?) exit 1
      ;;
  esac
done

# Experiment different combination of BITCOUNT & UNITDURAITON under various 
# horizon
if [[ test_number -eq 1 ]]; then
  for i in {20..24..2}; do
    for j in {100..60..20}; do
      printf "\"\$h=2^{$i}, p=$j\$\"\n"
      for k in {10..$i}; do
        make -sB TEST_PARAMS="-DTEST_NAME=FlexQueueTestCase -DQUEUE_SIZE=$(( 2**20 )) -DDISTRIBUTION=piecewise_simple -DHORIZON=$(( 2**i )) -DBUCKET_PERCENT=$j -DBIT_COUNT=$(( 2**k )) -DBIT_DURATION=$(( 2**(i-k) )) -DEXTRA_VARS=$(( 2**k ))" hold
        taskset -c 7 ./hold
      done
      printf "\n\n"
    done
  done
fi

# same as #1 but with steadystate
# takes ~7 hours to run, be careful
if [[ test_number -eq 2 ]]; then
  for i in {20..24..2}; do
    for j in {100..60..20}; do
      for k in {10..$i}; do
        printf "\"p=$j\$\"\n"
        make -sB TEST_PARAMS="-DTEST_NAME=FlexQueueTestCase -DQUEUE_SIZE=$(( 2**20 )) -DDISTRIBUTION=piecewise_simple -DHORIZON=$(( 2**i )) -DBUCKET_PERCENT=$j -DBIT_COUNT=$(( 2**k )) -DBIT_DURATION=$(( 2**(i-k) )) -DEXTRA_VARS=$(( 2**k ))" steadystate
        taskset -c 7 ./steadystate
        printf "\n\n"
      done
    done
  done
fi

# Test using different data structure as bucket
# uses uniform distribution
if [[ test_number -eq 3 ]]; then
  for i in {18..20}; do 
    printf "\"bitcount=\$2^{$i}\$\"\n"
    for j in {16..24}; do # horizon will be 2^20
      make -sB TEST_PARAMS="-DTEST_NAME=FlexQueueTestCase -DQUEUE_SIZE=$(( 2**j )) -DDISTRIBUTION=uniform -DHORIZON=$(( 2**20 )) -DBIT_COUNT=$(( 2**i )) -DBIT_DURATION=$(( 2**(20-i) ))" hold
      taskset -c 7 ./hold
    done
    printf "\n\n"
  done
fi

