#!/bin/zsh
cd "src"
test_number=0

while getopts "n:" opt; do
  case "$opt" in
    n) test_number=$OPTARG
      ;;
    \?) exit 1
      ;;
  esac
done

# repeatedly inserts into a boost::flatset to observe memeory consumption
if [[ test_number -eq 1 ]]; then
  make -sB TEST_PARAMS="-DTEST_NAME=FlatsetMemoryTestCase " steadystate
  taskset -c 7 ./steadystate
fi

# at which point does array become slower than set?
if [[ test_number -eq 2 ]]; then
  for i in {1..20}; do
    make -sB TEST_PARAMS="-DTEST_NAME=FlatsetInsertTestCase -DDISTRIBUTION=uniform -DHORIZON=1048576 -DQUEUE_SIZE=$(( i*10 ))" hold
    taskset -c 7 ./hold
  done
fi
# at which point does array become slower than set?
if [[ test_number -eq 3 ]]; then
  for i in {1..20}; do
    make -sB TEST_PARAMS="-DTEST_NAME=SetQueueTestCase -DDISTRIBUTION=uniform -DHORIZON=1048576 -DQUEUE_SIZE=$(( i*10 ))" hold
    taskset -c 7 ./hold
  done
fi
