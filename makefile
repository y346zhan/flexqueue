ROOT=thesis.tex
SEMINAR_TEX=seminar.tex
MAIN_TEXS=$(filter-out $(SEMINAR_TEX), $(wildcard *.tex))
PLOTS=$(wildcard scripts/*.gpi)
FIGURES_EXT=$(PLOTS:.gpi=.eps)
FIGURES=$(subst scripts, figures, $(FIGURES_EXT))
DIAGRAMS=$(wildcard diagrams/*.eps)
BIB=mylib.bib

all: thesis.pdf seminar.pdf

thesis.pdf: $(MAIN_TEXS) $(FIGURES) $(PLOTS) $(DIAGRAMS) $(BIB)
	latexmk -bibtex -pdf $(ROOT)

seminar.pdf: $(SEMINAR_TEX) $(FIGURES) $(PLOTS) $(DIAGRAMS)
	latexmk -pdf $(SEMINAR_TEX)

figures/%.eps : scripts/%.gpi
	gnuplot $<

clean:
	latexmk -C
	rm -rf figures/* diagrams/*.pdf
